<?php

namespace app\widgets\callback\actions;

use app\widgets\callback\models\CallbackForm;
use Yii;
use yii\base\Action;

class CallbackAction extends Action
{
    /**
     * Форма обратного звонка
     * @return string
     */
    public function run()
    {
        $model = new CallbackForm();

        $request = Yii::$app->request;

        if ($model->load($request->post())) {
            $model->save();
        }
        // Показываем уведомление
        Yii::$app->getSession()->setFlash('callback', 'Наши специалисты оперативно свяжутся и проконсультируют Вас по видам и характеристикам продукции.');

        // Редирект на предыдущую страницу
        return $this->controller->redirect(Yii::$app->request->referrer);
    }
}