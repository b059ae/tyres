<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 03.06.2017
 * Time: 16:55
 */

use app\models\Region;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/** @var $popupId string */
/** @var $url string */
/** @var $model \app\widgets\callback\models\CallbackForm */
/** @var $metrika string */
/** @var $title string */
/** @var $buttonTitle string */

?>
<?php Modal::begin([
    'header' => '<strong>Бесплатная консультация</strong>',
    'id' => $popupId,
    'clientOptions'=>[
            'fade'=>false,
    ]
]); ?>
    <p>Перезвоним за 8 секунд.</p>
<?php $form = ActiveForm::begin([
    'action' => '/callback',

]);
?>
<?= $form->field($model, 'name')->textInput(); ?>
<?= $form->field($model, 'phone')->textInput()->widget(\yii\widgets\MaskedInput::className(), [
    'mask' => '+7 (999) 999 99 99',
    'options' => [
        'id' => $popupId . '-phone',
        'autocomplete' => 'off',
        'class' => 'form-control',
    ],
]);
?>
    <div class="form-group text-center">
        <?= Html::submitButton('Позвоните мне', ['class' => 'btn btn-primary']) ?>
    </div>
<?php ActiveForm::end(); ?>
<?php Modal::end(); ?>