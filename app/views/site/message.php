<?php
/* @var $title string */
/* @var $message string */
/* @var $download string */

use yii\bootstrap\Html;
$this->title = $title;
$this->params['no_map'] = true;

if(!empty($download)){
    Yii::$app->session->setFlash('download', $download);
    $this->registerJs(
<<<JS
    document.location = '/download';
JS
    );
}

?>
<div id="main" role="main">
    <div class="container">
        <div class="row">
            <div class="main_content col-sm-12 text-center">
                <h1 class="page_heading"><?= Html::encode($title) ?></h1>
                <hr>
                <h2>
                    <?= nl2br(Html::encode($message)) ?>
                </h2>
                <div class="text-center">
                    <?=Html::a('На главную', ['/'], ['class' => 'btn btn-primary btn-lg']);?>
                </div>
            </div>
        </div>
    </div>
</div>