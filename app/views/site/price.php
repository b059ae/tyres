<?php 
/* @var $page \yii\cms\modules\page\api\PageObject */
/* @var $map_url_1 string */
/* @var $map_url_2 string */
/* @var $email string */
/* @var $phone string */
/* @var $address string */
/* @var $workingHours string */

use yii\cms\models\Setting;
use yii\helpers\Html;

$this->registerMetaTag([
    'name' => 'description',
    'content' => $page->seo('description')
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $page->seo('keywords')
]);
$this->title = $page->seo('title', $page->title);
$this->params['breadcrumbs'][] = $page->title;
?>
<div id="main" role="main">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="page_heading"><?=Html::encode($this->title);?></h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <h4>Адрес</h4>
                <ul>
                    <li><?=str_replace(';', '<br>', $address);?></li>
                </ul>
            </div>
            <div class="col-sm-3">
                <h4>Время работы</h4>
                <ul>
                    <li><?=str_replace(';', '<br>', $workingHours);?></li>
                </ul>
            </div>
            <div class="col-sm-3">
                <h4>Схемы маршрута</h4>
                <ul>
                    <li><a href="<?=$map_url_1;?>" target="_blank">Схема проезда по М4 на Москву</a></li>
                    <li><a href="<?=$map_url_2;?>" target="_blank">Схема проезда по М4 на Краснодар</a></li>
                </ul>
            </div>
            <div class="col-sm-3">
                <h4>Контакты</h4>
                <ul>
                    <li>тел.: <a href="tel:<?=preg_replace('/[^\d+]/', '', $phone);?>"><?=$phone;?></a></li>
                    <li>e-mail: <a href="mailto:<?=$email?>"><?=$email?></a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="main_content col-sm-12">
                <?=$page->getText() ?>
            </div>
        </div>
    </div>
</div>
             