<?php
use app\helpers\CatalogHelper;
?>
<table cellpadding="10">
    <tr>
        <td>Номер заказа: </td>
        <td><b><?=$order->id;?></b></td>
    </tr>

<?php foreach($order->goods as $good): ?>
    <tr>
        <td colspan="2">
            Наименование: <i><?=CatalogHelper::getItemTitle($good->item);?></i><br>
    
            За единицу: <b><?=$good->item->price;?> руб.</b><br>
            
            Кол-во: <b><?=$good->count;?></b>
        </td>
    </tr>
<?php endforeach; ?>   
    
    <tr>
        <td>Сумма заказа: </td>
        <td><b><?=$order->cost;?> руб.</b></td>
    </tr>
</table>

<br><br>

<table cellpadding="10">
<?php foreach($data as $key => $value): ?>
    <tr>
        <td><?=$key;?>:</td>
        <td><b><?=$value;?></b></td>
    </tr>
<?php endforeach; ?>
</table>
