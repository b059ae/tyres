<?php
/* @var $popular \yii\cms\modules\catalog\api\ItemObject[] */
?>
<div id="main" role="main">
    <div class="container">
        <div class="featured_products">
            <h2 class="page_heading">Популярные товары</h2>
            <div class="product_listing_main">
                <div class="row auto-clear">
                    <?php foreach($popular as $item): ?>
                        <?=$this->render('//catalog/_carousel_item', ['item' => $item]); ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>
