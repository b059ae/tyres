<!--Шины для газели-->
<?php /*<div class="col-xs-3 showcase_block showcase_block__5 wow">
    <a href="<?=Url::to(['catalog/cat', 'slug' => $catalogs['gazelle-tyres']->slug]);?>">
        <?=Html::img($asset->baseUrl . '/img/showcase5.png');?>
        <div class="wrap_1">
            <div class="wrap_2">
                <div class="wrap_3">
                    <h4 class="wow"><?=$catalogs['gazelle-tyres']->title;?></h4>
                    <span class="btn wow">Подобрать</span>
                </div>
            </div>
        </div>
    </a>
</div>*/ ?>

<?php $form = ActiveForm::begin([
    'action' => Url::to(['catalog/cat', 'slug' => 'truck-tyres']),
    'method' => 'GET',
    'options' => ['class' => 'main-filter hidden-sm'],
]); ?>

    <div class="main-filter-row">
        <label for="width">Ширина</label>
        <?=Html::activeDropDownList($filterForm, 'width', getFieldVariants('width', $catalogs['truck-tyres']->id), [
            'class' => 'main-filter-select',
        ]);?>
    </div>

    <div class="main-filter-row">
        <label for="height">Высота</label>
        <?=Html::activeDropDownList($filterForm, 'height', getFieldVariants('height', $catalogs['truck-tyres']->id), [
            'class' => 'main-filter-select',
        ]);?>
    </div>

    <div class="main-filter-row">
        <label for="diameter">Диаметр</label>
        <?=Html::activeDropDownList($filterForm, 'diameter', getFieldVariants('diameter', $catalogs['truck-tyres']->id), [
            'class' => 'main-filter-select',
        ]);?>
    </div>

    <div class="main-filter-row">
        <label for="axle">Ось</label>
        <?=Html::activeDropDownList($filterForm, 'axle', getFieldVariants('axle', $catalogs['truck-tyres']->id), [
            'class' => 'main-filter-select',
        ]);?>
    </div>

    <button type="submit" class="btn">Подобрать</button>
<?php ActiveForm::end(); ?>