<?php
/* @var $filterForm app\models\CatalogFilterForm */
/* @var $catalogs yii\cms\modules\catalog\api\CategoryObject[] */
/* @var $address string */
/* @var $workingHours string */

use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use app\helpers\CatalogHelper;
use yii\cms\modules\article\models\Category;
use yii\cms\modules\article\api\ArticleObject;
use app\assets\AppAsset;

$asset = AppAsset::register($this);
$slides_category = Category::find()->where(['slug' => 'main-slider'])->one();

function getFieldVariants($name, $id){
    $keys = CatalogHelper::getFieldVariants($name, $id);
    $values = array_map(function($value){
        return str_replace('.', ',', $value);
    }, $keys);
    return ['' => 'Все'] + array_combine($keys, $values);
}
?>
<div id="showcase" class="page_container">
    <div class="showcase_row__1">

        <!--ГРУЗОВЫЕ ШИНЫ-->
        <div class="table_cell table_cell__1 main-slider-wrapper" style="background-image: none;">
            <div class="main-slider">
            <?php foreach($slides_category->items as $slide): ?>
                <?php
                    $link_data = explode('|', $slide->short);
                ?>
                <div class="showcase_block__1"
                     style="
                     background-image: url(<?=(new ArticleObject($slide))->thumb(950);?>);
                     background-size: cover;
                     ">
                    <a href="<?=isset($link_data[1]) ? $link_data[1] : '#';?>"
                       <?=isset($link_data[2]) ? 'data-toggle="modal"' : '';?>>
                        <div>
                            <h2 class="wow"><?=$slide->title;?></h2>
                            <p class="wow showcase_block_description">
                                <?=str_replace(
                                    ['<p>', '</p>', '<div>', '</div>'],
                                    ['', '</br>', '', '</br>'],
                                    $slide->text
                                );?>
                            </p>
                            <p class="wow showcase_button_p">
                                <span class="btn wow animated">
                                    <?=isset($link_data[0]) ? $link_data[0] : 'Открыть';?>
                                </span>
                            </p>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
            </div>
            <?php $form = ActiveForm::begin([
                'action' => Url::to(['catalog/all-tyres']),
                'method' => 'GET',
                'options' => ['class' => 'main-filter hidden-sm hidden-xs'],
            ]); ?>

                <div class="main-filter-row">
                    <label for="width" class="main-filter-label">Ширина</label>
                    <?=Html::activeDropDownList($filterForm, 'width', getFieldVariants('width', $catalogs['truck-tyres']->id), [
                        'class' => 'main-filter-select',
                    ]);?>
                </div>

                <div class="main-filter-row">
                    <label for="height" class="main-filter-label">Высота</label>
                    <?=Html::activeDropDownList($filterForm, 'height', getFieldVariants('height', $catalogs['truck-tyres']->id), [
                        'class' => 'main-filter-select',
                    ]);?>
                </div>

                <div class="main-filter-row">
                    <label for="diameter" class="main-filter-label">Диаметр</label>
                    <?=Html::activeDropDownList($filterForm, 'diameter', getFieldVariants('diameter', $catalogs['truck-tyres']->id), [
                        'class' => 'main-filter-select',
                    ]);?>
                </div>

                <div class="main-filter-row">
                    <label for="axle" class="main-filter-label">Ось</label>
                    <?=Html::activeDropDownList($filterForm, 'axle', getFieldVariants('axle', $catalogs['truck-tyres']->id), [
                        'class' => 'main-filter-select',
                    ]);?>
                </div>
                
                <div class="main-filter-row text-center">
                    <?=Html::checkbox('used', false, ['id' => 'mf-used']);?>
                    <label for="mf-used"><?=$filterForm->getAttributeLabel('used');?></label>
                    &nbsp;&nbsp;
                    <?=Html::checkbox('restored', false, ['id' => 'mf-restored']);?>
                    <label for="mf-restored">Восст.</label>
                    <?=Html::checkbox('new', false, ['id' => 'mf-new']);?>
                    <label for="mf-new"><?=$filterForm->getAttributeLabel('new');?></label>
                </div>

                <button type="submit" class="btn">Подобрать</button>
            <?php ActiveForm::end(); ?>
        </div>
        <!--/ГРУЗОВЫЕ ШИНЫ-->
        
        <!--ШИНОМОНТАЖ-->
        <div class="table_cell table_cell__2">
            <div class="showcase_block__1 ">
                <a href="#" onclick="return false;">
                    <div>
                        <h2 class="wow" onclick="document.location='/uslugi-gruzovogo-shinomontazha';">
                            Шиномонтаж
                        </h2>
                        <p class="wow hidden-xs">
                            <?=str_replace(';', ',', $address);?>
                            <br>
                            <?=str_replace(';', '<br>', $workingHours);?>
                        </p>
                        <p>
                            <button class="btn animated" onclick="window.open('https://yandex.ru/maps/?um=constructor%3A195011201c958c24a156c14cb8cd43787734fe273589797b16e288f59bc27f6f&source=constructorLink', '_blank');">
                                Проложить маршрут
                            </button>
                            &nbsp;&nbsp;
                            <button class="btn animated" onclick="jQuery('#meeting-modal').modal('show')">
                                Записаться
                            </button>
                        </p>
                    </div>
                </a>
            </div>
        </div>
        <!--/ШИНОМОНТАЖ-->
    </div>

    <div class="clearfix"></div>

    <div class="showcase_row__2">
        <div class="row">


        <!--ГРУЗОВЫЕ ШИНЫ-->
        <div class="col-xs-3 showcase_block showcase_block__3 wow">
            <a href="<?=Url::to(['catalog/cat', 'slug' => $catalogs['truck-tyres']->slug]);?>">
                <?=Html::img($asset->baseUrl . '/img/showcase2.png');?>
                <div class="wrap_1">
                    <div class="wrap_2">
                        <div class="wrap_3">
                            <h4 class="wow"><?=$catalogs['truck-tyres']->title;?></h4>
                            <span class="btn wow">Подобрать</span>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <!--/ГРУЗОВЫЕ ШИНЫ-->
        
        <!--ДИСКИ-->
        <div class="col-xs-3 showcase_block showcase_block__2 wow">
            <a href="<?=Url::to(['catalog/cat', 'slug' => $catalogs['wheels']->slug]);?>">
                <?=Html::img($asset->baseUrl . '/img/showcase4.png');?>
                <div class="wrap_1">
                    <div class="wrap_2">
                        <div class="wrap_3">
                            <h4 class="wow"><?=$catalogs['wheels']->title;?></h4>
                            <span class="btn wow">Подобрать</span>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <!--/ДИСКИ-->

        <!--ЛЕГКОВЫЕ ШИНЫ-->
        <div class="col-xs-3 showcase_block showcase_block__4 wow">
            <a href="<?=Url::to(['catalog/cat', 'slug' => $catalogs['auto-tyres']->slug]);?>">
                <?=Html::img($asset->baseUrl . '/img/showcase3.png');?>
                <div class="wrap_1">
                    <div class="wrap_2">
                        <div class="wrap_3">
                            <h4 class="wow"><?=$catalogs['auto-tyres']->title;?></h4>
                            <span class="btn wow">Подобрать</span>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <!--/ЛЕГКОВЫЕ ШИНЫ-->

        
        
        <div class="col-xs-3 showcase_block showcase_block__5 wow">
            <a href="<?=Url::to(['catalog/cat', 'slug' => $catalogs['truck-tyres']->slug, 'restored' => '1']);?>">
                <?=Html::img($asset->baseUrl . '/img/showcase5.png');?>
                <div class="wrap_1">
                    <div class="wrap_2">
                        <div class="wrap_3">
                            <h4 class="wow">Восстановленные шины</h4>
                            <span class="btn wow">Подобрать</span>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <!--/Шины для газели-->

        </div>
    </div>
</div>

<?php
$this->registerJs(<<<JS
    jQuery('.main-slider').bxSlider({
        oneToOneTouch : false,
        pager : false,
        auto : true,
        pause : 5000,
        speed : 1000,
        useCSS : false,
        stopAutoOnClick : true
    });
JS
);
?>
<?=$this->render('//layouts/modals/_meeting-modal', [
    'model' => $meeting_model,
]);?>
<?=$this->render('//layouts/modals/_feedback-modal', [
    'model' => $feedback_model,
]);?>