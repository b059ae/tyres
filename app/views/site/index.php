<?php
/* @var $catalogs yii\cms\modules\catalog\api\CategoryObject[] */
/* @var $page \yii\cms\modules\page\api\PageObject */
/* @var $popular \yii\cms\modules\catalog\api\ItemObject[] */
/* @var $filterForm app\models\CatalogFilterForm */
/* @var $address string */
/* @var $workingHours string */
/* @var $services_cat \yii\cms\modules\article\api\CategoryObject */

use yii\helpers\Url;

$this->registerMetaTag([
    'name' => 'description',
    'content' => $page->seo('description')
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $page->seo('keywords')
]);
$this->title = $page->seo('title', $page->title);
$this->params['breadcrumbs'][] = $page->title;
$this->params['no_breadcrumbs'] = true;
?>
<!-- Первый слайд -->
<?= $this->render('slides/_main', [
    'meeting_model' => $meeting_model,
    'feedback_model' => $feedback_model,
    'filterForm' => $filterForm,
    'catalogs' => $catalogs,
    'address' => $address,
    'workingHours' => $workingHours,
]); ?>

<!-- Популярные товары -->
<?= $this->render('slides/_popular', ['popular' => $popular]); ?>

<!-- HOMEPAGE INFO BLOCK -->
<div id="parallax_block" class="homepage_info page_container">
    <div class="container">
        <h2 id="homepage_info__title">
            <span class="wrap-1 wow">Наши услуги</span>
        </h2>

        <div class="row auto-clear margin-t-40">
            <?php foreach($services_cat->items as $service):  ?>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 homepage_info_item wow services-item">
                <h3 class="wow">
                    <a href="<?=Url::toRoute(['services/view', 'slug' => $service->slug]);?>">
                        <?=$service->title;?>
                    </a>
                </h3>
                <p class="wow"><?=strip_tags($service->short);?></p>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<!-- Карта -->
<div class="container"> 
    <div class="row"> 
        <div class="col-sm-12 map-container"> 
            <?= $this->render('slides/_map', ['address' => $page->address]); ?>
        </div> 
    </div> 
</div>