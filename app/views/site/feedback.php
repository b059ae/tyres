<?php
/** @var $page \yii\cms\modules\page\api\PageObject */
/** @var $login_form app\modules\customer\models\LoginForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = $page->seo('title', $page->title);
$this->params['breadcrumbs'][] = $page->title;

?>
<div id="main" role="main">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="page_heading"><?=Html::encode($this->title);?></h2>
                <hr>
            </div>
        </div>

        <?php if($result): ?>
            <div class="row">
                <div class="main_content col-sm-12">
                    <h4>Спасибо что оставили заявку!</h4>
                    <p> Наш менеджер свяжится с вами в кротчайшие сроки.</p>
                </div>
            </div>
        <?php else: ?>
            <div class="row">
                <div class="main_content col-sm-12">
                    <?php $form = ActiveForm::begin([
                        'id' => 'feedback-form',
                        'options' => ['class' => 'center-block'],
                    ]); ?>

                    <?php if(!empty($feedbackForm->getErrors())): ?>
                        <div class="has-error">
                            <div class="help-block"><?= $form->errorSummary($feedbackForm);?></div>
                        </div>
                    <?php endif;?>

                    <?=$form->field($feedbackForm, 'name');?>
                    <?=$form->field($feedbackForm, 'email');?>
                    <?=$form->field($feedbackForm, 'phone')?>
                    <?=$form->field($feedbackForm, 'text') ->textarea(['rows' => 6])?>

                    <div class="form-group text-center">
                        <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        <?php endif;?>
    </div>
</div>