<?php
/* @var $page \yii\cms\modules\page\api\PageObject */

use yii\bootstrap\Html;
use yii\cms\models\Setting;

$this->registerMetaTag([
    'name' => 'description',
    'content' => $page->seo('description')
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $page->seo('keywords')
]);
$this->title = $page->seo('title', $page->title);
$this->params['breadcrumbs'][] = $page->title;

?>
<div id="main" role="main">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="page_heading"><?=Html::encode($this->title);?></h2>
                <hr>
            </div>
        </div>
	<div class="row">
            <div class="col-sm-6">
                <?=$this->render('slides/_map', ['address' => $page->address]); ?>
            </div>
            <div class="main_content col-sm-6">
		<div class="contact-scope">
                    <div class="contact-socials">
                        <p><strong>Мы в соц-сетях:</strong></p>
                        <p>
                            <?=Html::a('', Setting::get('social_vk'), [
                                'class' => 'fa fa-vk',
                                'target' => '_blank',
                            ]);?>
                            <?=Html::a('', Setting::get('social_facebook'), [
                                'class' => 'fa fa-facebook',
                                'target' => '_blank',
                            ]);?>
                            <?=Html::a('', Setting::get('social_instagram'), [
                                'class' => 'fa fa-instagram',
                                'target' => '_blank',
                            ]);?>
                        </p>
                    </div>
                    <?=$page->getText() ?>
                </div>
            </div>
	</div>
    </div>
</div>