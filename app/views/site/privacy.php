<?php
/** @var $page \yii\cms\modules\page\api\PageObject */

use yii\helpers\Html;

$this->registerMetaTag([
    'name' => 'description',
    'content' => $page->seo('description')
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $page->seo('keywords')
]);
$this->title = $page->seo('title', $page->title);
$this->params['breadcrumbs'][] = $page->title;
?>
<div id="main" role="main">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="page_heading"><?=Html::encode($this->title);?></h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="main_content col-sm-12">
                <?=$page->getText() ?>
            </div>
        </div>
    </div>
</div>
