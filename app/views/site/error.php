<?php
use yii\helpers\Html;

$title = Yii::$app->session->getFlash('title');

$this->title = !empty($title) ? $title : $name;
$this->params['no_map'] = true;

?>
<div id="main" role="main">
    <div class="container">
        <div class="row">
            <div class="main_content col-sm-12 text-center">
                <h1 class="page_heading"><?= Html::encode($this->title) ?></h1>
                <hr>
                <h2>
                    <?= nl2br(Html::encode($message)) ?>
                </h2>
            </div>
        </div>
    </div>
</div>