<?php
use yii\helpers\Html;

$this->title = "Оплата заказа №{$order_id} через платёжный шлюз Сбербанка";
$this->registerJs(<<<JS
    var iframe = document.getElementsByTagName('iframe')[0];
    iframe.onload = function(){
            $([document.documentElement, document.body]).animate({
            scrollTop: $("body").offset().top + 120
        }, 300);
    };
JS
);
?>
<div id="main" role="main">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="page_heading"><?=Html::encode($this->title);?></h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="main_content col-sm-8">
                <?php if (empty($formUrl)) : ?>
                    <p><?=$message;?></p>
                <?php else: ?>
                    <iframe src="<?=$formUrl;?>" width="600" height="1000"></iframe>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
