<?php
use app\helpers\CatalogHelper;
?>
Номер заказа: <?=$order->id;?>

<?php foreach($order->goods as $good): ?>
    Наименование: <?=CatalogHelper::getItemTitle($good->item);?>

    За единицу: <?=$good->item->price;?> руб.

    Кол-во: <?=$good->count;?>

<?php endforeach; ?>   

Сумма заказа: <?=$order->cost;?> руб.

    
<?php foreach($data as $key => $value): ?>
    <?=$key;?> : <?=$value;?>

<?php endforeach; ?>

