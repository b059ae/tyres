<?php
/* @var $model \app\models\CatalogFilterForm */
/* @var $items \yii\cms\modules\catalog\api\ItemObject[] */
/* @var $filter_params array */

use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\cms\models\Setting;
use yii\widgets\ActiveForm;
use yii\bootstrap\Html;
use app\helpers\CatalogHelper;

$page_title = 'Все шины';
$this->title = $page_title;
$this->params['breadcrumbs'][] = $page_title;
$max_price = 50000;


?>


<!-- MAIN CONTENT -->
<div id="main" role="main">
    <div class="container">
        <div class="row">
            <div class="main_content col-sm-12">
				
					

<h2 class="page_heading"><?=$page_title;?></h2>

        



<!-- ФИЛЬТР-->
<hr>
<h3 class="text-left">Фильтр по параметрам</h3>
<?php $form = ActiveForm::begin([
    'id' => 'filter-form',
    'method' => 'get',
    'action' => '/catalog/all-tyres',
]);?>
<div class="row">
    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
        <div class="checkbox">
            <?=Html::activeCheckbox($model, 'restored', [
                'onclick' => 'document.getElementById(\'filter-form\').submit();',
                'label' => '<b>'.$model->getAttributeLabel('restored').'</b>',
                'uncheck' => null,
            ]);?>
        </div>
        <div class="checkbox">
            <?=Html::activeCheckbox($model, 'used', [
                'onclick' => 'document.getElementById(\'filter-form\').submit();',
                'label' => '<b>'.$model->getAttributeLabel('used').'</b>',
                'uncheck' => null,
            ]);?>
        </div>
        <div class="checkbox">
            <?=Html::activeCheckbox($model, 'new', [
                'onclick' => 'document.getElementById(\'filter-form\').submit();',
                'label' => '<b>'.$model->getAttributeLabel('new').'</b>',
                'uncheck' => null,
            ]);?>
        </div>
    </div>
    <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12 filter-container">
        <div class="filter-header text-center">
            <h4>Бренд</h4>
        </div>
        <div class="filter-body text-center">
            <?=Html::activeDropDownList($model, 'brand', CatalogHelper::getFieldVariantsInFormat('brand', $cat_to_exclude_id, '!', true), [
                'onchange' => 'document.getElementById(\'filter-form\').submit();',
                'class' => 'filter-body-select',
            ]);?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-4 col-xs-12 filter-container">
        <div class="filter-header text-center">
            <h4>Цена</h4>
        </div>
        <div class="filter-body text-center">
            <p>
                <div id="slider" class="center-block" style="width:90%;"></div>
            </p>
            <p>
                от <?=Html::activeInput('text', $model, 'price_min', ['id' => 'min-price']);?>
                до <?=Html::activeInput('text', $model, 'price_max', ['id' => 'max-price']);?>
            </p>
            <p><button type="submit" class="btn">Показать</button>
                <a href="<?=Url::to(['catalog/all-tyres']);?>">Очистить</a>
            </p>
        </div>
    </div>
    

    <div class="col-sm-8 col-xs-12">
        <div class="row">
            <div class="col-sm-6 col-xs-6 filter-container">
                <div class="filter-header text-center">
                    <h4>Ширина профиля</h4>
                </div>
                <div class="filter-body text-center">
                    <?=Html::activeDropDownList($model, 'width', CatalogHelper::getFieldVariantsInFormat('width'), [
                        'onchange' => 'document.getElementById(\'filter-form\').submit();',
                        'class' => 'filter-body-select',
                    ]);?>
                </div>
            </div>
            <div class="col-sm-6 col-xs-6 filter-container">
                <div class="filter-header text-center">
                    <h4>Высота профиля</h4>
                </div>
                <div class="filter-body text-center">
                    <?=Html::activeDropDownList($model, 'height', CatalogHelper::getFieldVariantsInFormat('height'), [
                        'onchange' => 'document.getElementById(\'filter-form\').submit();',
                        'class' => 'filter-body-select',
                    ]);?>
                </div>
            </div>
            <div class="col-sm-6 col-xs-6 filter-container">
                <div class="filter-header text-center">
                    <h4>Диаметр</h4>
                </div>
                <div class="filter-body text-center">
                    <?=Html::activeDropDownList($model, 'diameter', CatalogHelper::getFieldVariantsInFormat('diameter'), [
                        'onchange' => 'document.getElementById(\'filter-form\').submit();',
                        'class' => 'filter-body-select',
                    ]);?>
                </div>
            </div>
            <div class="col-sm-6 col-xs-6 filter-container">
                <div class="filter-header text-center">
                    <h4>Ось</h4>
                </div>
                <div class="filter-body text-center">
                    <?=Html::activeDropDownList($model, 'axle', CatalogHelper::getFieldVariantsInFormat('axle'), [
                        'onchange' => 'document.getElementById(\'filter-form\').submit();',
                        'class' => 'filter-body-select',
                    ]);?>
                </div>
            </div>
        </div>
    </div>
    
</div>
<?php ActiveForm::end(); ?>


<!-- pagination -->
<div class="product_listing_controls">
    <p class="products_count">
        <?=count($items);?> товаров из <?=$pagination->totalCount;?>
    </p>
    <?php 
        echo LinkPager::widget([
            'pagination' => $pagination,
            'options' => [
                'class' => 'pag'
            ]
        ]);
    ?>
</div>


<div>
    <!-- products listing -->
    <div class="product_listing_main row auto-clear">
    <?php foreach ($items as $item) : ?>
        <?= $this->render('_item', ['item' => $item]) ?>
    <?php endforeach; ?>
    </div>

    <!-- pagination -->
    <div class="product_listing_controls">
        <p class="products_count">
            <?=count($items);?> товаров из <?=$pagination->totalCount;?>
        </p>
        <?php 
            echo LinkPager::widget([
                'pagination' => $pagination,
                'options' => [
                    'class' => 'pag'
                ]
            ]);
        ?>
    </div>
</div>
				
            </div>
        </div>
    </div>
</div>

<?php $this->registerJs(<<<JS
    //ФИЛЬТР + ПОЛЗУНОК ФИЛЬТРА ЦЕНЫ
    var max_price = $max_price;
    var get_params = $filter_params;
    
    $('.filter-body-select').select2({
        minimumResultsForSearch: Infinity
    });

    var slider = document.getElementById('slider');
    noUiSlider.create(slider, {
        start : [10, max_price],
        connect : true,
        range : {
            min : 10,
            max : max_price
        },
        format : wNumb({
            decimals : 0
        })
    });
    var min_price_input = document.getElementById('min-price');
    var max_price_input = document.getElementById('max-price');
    slider.noUiSlider.on('update', function(values, handle){
        if(handle === 0) min_price_input.value = values[0];
        else if(handle === 1) max_price_input.value = values[1];
    });
    
    if(get_params['price_min'] && get_params['price_max']){
        slider.noUiSlider.set([
            get_params['price_min'],
            get_params['price_max']
        ]);
    }
JS
); ?>