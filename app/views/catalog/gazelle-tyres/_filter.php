<?php
/* @var $model \app\models\CatalogFilterForm */
/* @var $cat \yii\cms\modules\catalog\api\CategoryObject */

use app\helpers\CatalogHelper;
use yii\bootstrap\Html;

function getFieldVariants($name, $id){
    $keys = CatalogHelper::getFieldVariants($name, $id);
    $values = array_map(function($value){
        return str_replace('.', ',', $value);
    }, $keys);
    return ['' => 'Все'] + array_combine($keys, $values);
}

?>
<div class="col-sm-6 col-xs-6 filter-container">
    <div class="filter-header text-center">
        <h4>Ширина профиля</h4>
    </div>
    <div class="filter-body text-center">
        <?=Html::activeDropDownList($model, 'width', getFieldVariants('width', $cat->id), [
            'onchange' => 'document.getElementById(\'filter-form\').submit();',
            'class' => 'filter-body-select',
        ]);?>
    </div>
</div>
<div class="col-sm-6 col-xs-6 filter-container">
    <div class="filter-header text-center">
        <h4>Высота профиля</h4>
    </div>
    <div class="filter-body text-center">
        <?=Html::activeDropDownList($model, 'height', getFieldVariants('height', $cat->id), [
            'onchange' => 'document.getElementById(\'filter-form\').submit();',
            'class' => 'filter-body-select',
        ]);?>
    </div>
</div>
<div class="col-sm-6 col-xs-6 filter-container">
    <div class="filter-header text-center">
        <h4>Диаметр</h4>
    </div>
    <div class="filter-body text-center">
        <?=Html::activeDropDownList($model, 'diameter', getFieldVariants('diameter', $cat->id), [
            'onchange' => 'document.getElementById(\'filter-form\').submit();',
            'class' => 'filter-body-select',
        ]);?>
    </div>
</div>
<div class="col-sm-6 col-xs-6 filter-container">
    <div class="filter-header text-center">
        <h4>Ось</h4>
    </div>
    <div class="filter-body text-center">
        <?=Html::activeDropDownList($model, 'axle', getFieldVariants('axle', $cat->id), [
            'onchange' => 'document.getElementById(\'filter-form\').submit();',
            'class' => 'filter-body-select',
        ]);?>
    </div>
</div>