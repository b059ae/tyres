<?php
use app\helpers\CatalogHelper;

/** @var $item \yii\cms\modules\catalog\api\ItemObject */

$available = ($item->available == '0' || !$item->available)
    ? 'на заказ' : $item->available;

?>
<div>Доступно, шт: <span><?=$available;?></span></div>
<div>Диаметр: <span><?=$item->data->diameter;?></span></div>
<div>Ширина: <span><?=$item->data->width;?></span></div>
<div>Количество крепежных отверстий: <span><?=$item->data->fixture;?></span></div>
<div>PCD: <span><?=$item->data->pcd;?></span></div>
<div>DIA: <span><?=$item->data->dia;?></span></div>
<div>Вылет(ET): <span><?=$item->data->et;?></span></div>

<?php if(!empty($item->data->type)): ?>
<div>Тип диска: <span><?=implode('<br>', $item->data->type);?></span></div>
<?php endif; ?>