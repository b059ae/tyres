<?php
use app\helpers\CatalogHelper;

/** @var $item \yii\cms\modules\catalog\api\ItemObject */

?>

<?php if(!empty($item->data->type)): ?>
<div>
    Тип диска: <?=implode(', ', $item->data->type);?>
</div>
<?php endif ?>