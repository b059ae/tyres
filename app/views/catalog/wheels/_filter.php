<?php
/* @var $model \app\models\CatalogFilterForm */
/* @var $cat \yii\cms\modules\catalog\api\CategoryObject */

use app\helpers\CatalogHelper;
use yii\bootstrap\Html;

function getFieldVariants($name, $id){
    $keys = CatalogHelper::getFieldVariants($name, $id);
    $values = array_map(function($value){
        return str_replace('.', ',', $value);
    }, $keys);
    return ['' => 'Все'] + array_combine($keys, $values);
}

?>
<div class="col-sm-4 col-xs-4 filter-container">
    <div class="filter-header text-center">
        <h4>Диаметр</h4>
    </div>
    <div class="filter-body text-center">
        <?=Html::activeDropDownList($model, 'diameter', getFieldVariants('diameter', $cat->id), [
            'onchange' => 'document.getElementById(\'filter-form\').submit();',
            'class' => 'filter-body-select',
        ]);?>
    </div>
</div>
<div class="col-sm-4 col-xs-4 filter-container">
    <div class="filter-header text-center">
        <h4>Ширина</h4>
    </div>
    <div class="filter-body text-center">
        <?=Html::activeDropDownList($model, 'width', getFieldVariants('width', $cat->id), [
            'onchange' => 'document.getElementById(\'filter-form\').submit();',
            'class' => 'filter-body-select',
        ]);?>
    </div>
</div>
<div class="col-sm-4 col-xs-4 filter-container">
    <div class="filter-header text-center">
        <h4>Кол-во крепежных отверстий</h4>
    </div>
    <div class="filter-body text-center">
        <?=Html::activeDropDownList($model, 'fixture', getFieldVariants('fixture', $cat->id), [
            'onchange' => 'document.getElementById(\'filter-form\').submit();',
            'class' => 'filter-body-select',
        ]);?>
    </div>
</div>

<div class="col-sm-4 col-xs-4 filter-container">
    <div class="filter-header text-center">
        <h4>PCD</h4>
    </div>
    <div class="filter-body text-center">
        <?=Html::activeDropDownList($model, 'pcd', getFieldVariants('pcd', $cat->id), [
            'onchange' => 'document.getElementById(\'filter-form\').submit();',
            'class' => 'filter-body-select',
        ]);?>
    </div>
</div>

<div class="col-sm-4 col-xs-4 filter-container">
    <div class="filter-header text-center">
        <h4>DIA</h4>
    </div>
    <div class="filter-body text-center">
        <?=Html::activeDropDownList($model, 'dia', getFieldVariants('dia', $cat->id), [
            'onchange' => 'document.getElementById(\'filter-form\').submit();',
            'class' => 'filter-body-select',
        ]);?>
    </div>
</div>

<div class="col-sm-4 col-xs-4 filter-container">
    <div class="filter-header text-center">
        <h4>Вылет (ET)</h4>
    </div>
    <div class="filter-body text-center">
        <?=Html::activeDropDownList($model, 'et', getFieldVariants('et', $cat->id), [
            'onchange' => 'document.getElementById(\'filter-form\').submit();',
            'class' => 'filter-body-select',
        ]);?>
    </div>
</div>
