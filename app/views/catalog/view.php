<?php
/* @var $popular \yii\cms\modules\catalog\api\ItemObject[] */
/* @var $item \yii\cms\modules\catalog\api\ItemObject */

use app\helpers\Html;
use app\helpers\CatalogHelper;
use app\assets\AppAsset;

$asset = AppAsset::register($this);

$this->title = $item->seo('title', CatalogHelper::getItemTitle($item));
$this->params['breadcrumbs'][] = ['label' => $item->cat->title, 'url' => ['catalog/' . $item->cat->slug]];
$this->params['breadcrumbs'][] = $this->title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $item->seo('description', Html::firstParagraph($item->description))
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $item->seo('keywords')
]);

$price = $item->getPrice();
$price_string = empty($price) ? '' : $price . ' руб.';

?>
<div class="container">
          
                
<div class="product-scope">
    <div class="product_wrap">
        <div class="row">
            
            
<div class="col-sm-6 col-lg-5 product-view">
    <div class="zoomWrapper">
        <?=Html::img(CatalogHelper::getItemThumb($item, $asset, 800, 800), ['id'=>'elevatezoom_big']); ?>
    </div>
    <?php if (count($item->photos)) : ?>
        <div id="elevatezoom_gallery">
            <a href="#" data-image="<?=$item->thumb(800, 800);?>" data-zoom-image="<?=$item->thumb(800, 800);?>">
                <?=Html::img(CatalogHelper::getItemThumb($item, $asset, 100, 100)); ?>
            </a>
            <?php foreach ($item->photos as $photo) : ?>
                <a href="#" data-image="<?=$photo->thumb(800, 800);?>" data-zoom-image="<?=$photo->thumb(800, 800);?>">
                    <?=Html::img($photo->thumb(100, 100)); ?>
                </a>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>

<div class="col-sm-6 col-lg-7">
    
    <div itemprop="name" class="product_name">
        <?=CatalogHelper::getItemTitle($item)?>
    </div>
    <?=Html::beginForm(['/shopcart/add']);?>
        <div class="options clearfix">
            <div id="product_price">
                <p class="price product-price">
                    <span class="money"><?=$price_string;?></span>
                    <?=CatalogHelper::badge($item); ?>
                </p>
            </div>

            <div id="purchase">
                <label for="quantity">Кол-во: </label>
                <input min="1" type="number" id="quantity" name="count" value="1" class="form-control input-small" />
                <input type="hidden" name="id" value="<?=$item->id;?>" />
                <button class="btn btn-cart" type="submit" id="add-to-cart">Купить</button>
            </div>
        </div>
    <?=Html::endForm();?>
    <div class="product_details">
        <?=$this->render($item->getCat()->slug . '/_view-params.php', ['item'=>$item]);?>
    </div>
    
    <?php if(!empty($item->description)): ?>
    <div id="product_description" class="rte" itemprop="description">
        <h4>Описание:</h4>
        <div><?=$item->description;?></div>
    </div>
    <?php endif; ?>
</div>

            
			
        </div>
    </div>
</div>


<?php if(count($popular)): ?>
<hr>
<div class="widget_related_products">
    <h3>Рекомендуем</h3>
    <div class="widget_content">
        <div class="row product_listing_main product_listing_related auto-clear">
        <?php foreach($popular as $item): ?>
            <?= $this->render('//catalog/_carousel_item', ['item' => $item]); ?>
        <?php endforeach; ?>
        </div>
    </div>
</div>
<?php endif; ?>


</div>
<?php

$this->registerJs(<<<JS
    /* product image zoom */
    $("#elevatezoom_big").elevateZoom({
        gallery : "elevatezoom_gallery"
    });

    /* thumbs slider */
    $('#elevatezoom_gallery').bxSlider({
        responsive: true,
        infiniteLoop: false,
        pager: false,
        minSlides: 1,
        maxSlides: 3,
        moveSlides: 1,
        slideWidth: 87,
        slideMargin: 12
    });
JS
);