<?php
/* @var $item \yii\cms\modules\catalog\api\ItemObject */

use app\helpers\CatalogHelper;
use yii\helpers\Html;
use yii\helpers\Url;

use app\assets\AppAsset;
$asset = AppAsset::register($this);

$url = Url::to(['catalog/view','category'=>$item->getCat()->slug, 'slug'=>$item->slug]);

$price = $item->getPrice();
$price_string = empty($price) ? '' : $price . ' руб.';

?>
<div class="wow product col-sm-4 col-md-3 col-lg-3 product_homepage">
    <div class="product_wrapper">
        <div class="product_img">
            <a href="<?=\yii\helpers\Url::to(['catalog/view','category'=>$item->getCat()->slug, 'slug'=>$item->slug])?>">
                <?=Html::img(CatalogHelper::getItemThumb($item, $asset)); ?>
                <?=CatalogHelper::badge($item);?>
            </a>

            <div class="product_img_hover" onclick="redirect('<?=$url;?>', event.target)">
                <?= Html::beginForm('/shopcart/add', 'post', [
                    'id'=>'cart_form_'.$item->id,
                ])?>
                    <input type="hidden" name="id" value="<?=$item->id;?>"/>
                    <input type="hidden" name="count" value="1"/>
                    <button class="btn btn-cart product-cart-btn-1" type="submit">
                        <i class="fa fa-shopping-cart"></i>
                        <span>Купить</span>
                    </button>
                <?= Html::endForm();?>
            </div>
        </div>
        <div class="product_info">
            <div class="product_name">
                <div class="product_title">
                    <a href="<?=$url;?>">
                        <?=CatalogHelper::getItemTitle($item, '')?>
                    </a>
                </div>
                <?=$this->render($item->getCat()->slug . '/_item-params.php', ['item' => $item]);?>
            </div>

            <div class="product_price ">
                <span class="money"><?=$price_string;?></span>
                <button class="btn visible-xs margin-t-5" onclick="jQuery('#cart_form_<?=$item->id;?>').trigger('submit');">
                    В корзину
                </button>
            </div>
        </div>
    </div>
</div>
