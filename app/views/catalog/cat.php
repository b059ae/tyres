<?php
/* @var $model \app\models\CatalogFilterForm */
/* @var $items \yii\cms\modules\catalog\api\ItemObject[] */
/* @var $cat \yii\cms\modules\catalog\api\CategoryObject */
/* @var $filter_params array */

use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\cms\models\Setting;
use yii\widgets\ActiveForm;
use yii\bootstrap\Html;
use app\helpers\CatalogHelper;

$this->title = $cat->seo('title', $cat->model->title);
$this->params['breadcrumbs'][] = $cat->model->title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $cat->seo('description', $cat->title)
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $cat->seo('keywords')
]);

$pagination = $cat->getPagination();
$max_price = Setting::findOne(['name' => $cat->slug . '_max_price'])->value;

?>


<!-- MAIN CONTENT -->
<div id="main" role="main">
    <div class="container">
        <div class="row">
            <div class="main_content col-sm-12">
				
					

<h2 class="page_heading"><?= $cat->seo('h1', $cat->title) ?></h2>

        



<!-- ФИЛЬТР-->
<hr>
<h3 class="text-left">Фильтр по параметрам</h3>

<?php $form = ActiveForm::begin([
    'id' => 'filter-form',
    'method' => 'get',
    'action' => Url::to(['catalog/cat', 'slug' => $cat->slug]),
]);?>
<div class="row">
    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
        <?php if($cat->slug === 'truck-tyres'): ?>
            <div class="checkbox">
                <?=Html::activeCheckbox($model, 'restored', [
                    'onclick' => 'document.getElementById(\'filter-form\').submit();',
                    'label' => '<b>'.$model->getAttributeLabel('restored').'</b>'
                ]);?>
            </div>
            <div class="checkbox">
                <?=Html::activeCheckbox($model, 'new', [
                    'onclick' => 'document.getElementById(\'filter-form\').submit();',
                    'label' => '<b>'.$model->getAttributeLabel('new').'</b>'
                ]);?>
            </div>
        <?php endif; ?>
        <?php if(in_array($cat->slug, ['spec-tyres'])): ?>
            <div class="checkbox">
                <?=Html::activeCheckbox($model, 'used', [
                    'onclick' => 'document.getElementById(\'filter-form\').submit();',
                    'label' => '<b>'.$model->getAttributeLabel('used').'</b>'
                ]);?>
            </div>
            <div class="checkbox">
                <?=Html::activeCheckbox($model, 'new', [
                    'onclick' => 'document.getElementById(\'filter-form\').submit();',
                    'label' => '<b>'.$model->getAttributeLabel('new').'</b>'
                ]);?>
            </div>
        <?php endif; ?>
    </div>
    <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12 filter-container">
        <div class="filter-header text-center">
            <h4>Бренд</h4>
        </div>
        <div class="filter-body text-center">
            <?=Html::activeDropDownList($model, 'brand', CatalogHelper::getFieldVariantsInFormat('brand', $cat->id, '', true), [
                'onchange' => 'document.getElementById(\'filter-form\').submit();',
                'class' => 'filter-body-select',
            ]);?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-4 col-xs-12 filter-container">
        <div class="filter-header text-center">
            <h4>Цена</h4>
        </div>
        <div class="filter-body text-center">
            <p>
                <div id="slider" class="center-block" style="width:90%;"></div>
            </p>
            <p>
                от <?=Html::activeInput('text', $model, 'price_min', ['id' => 'min-price']);?>
                до <?=Html::activeInput('text', $model, 'price_max', ['id' => 'max-price']);?>
            </p>
            <p><button type="submit" class="btn">Показать</button>
                <a href="<?=Url::to(['catalog/cat', 'slug' => $cat->slug]);?>">Очистить</a>
            </p>
        </div>
    </div>
    

    <div class="col-sm-8 col-xs-12">
        <div class="row">
            <?=$this->render($cat->slug . '/_filter', ['model' => $model, 'cat' => $cat]) ?>
        </div>
    </div>
    
</div>
<?php ActiveForm::end(); ?>


<!-- pagination -->
<div class="product_listing_controls">
    <p class="products_count">
        <?=count($items);?> товаров из <?=$pagination->totalCount;?>
    </p>
    <?php 
        echo LinkPager::widget([
            'pagination' => $pagination,
            'options' => [
                'class' => 'pag'
            ]
        ]);
    ?>
</div>


<div>
    <!-- products listing -->
    <div class="product_listing_main row auto-clear">
    <?php foreach ($items as $item) : ?>
        <?= $this->render('_item', ['item' => $item]) ?>
    <?php endforeach; ?>
    </div>

    <!-- pagination -->
    <div class="product_listing_controls">
        <p class="products_count">
            <?=count($items);?> товаров из <?=$pagination->totalCount;?>
        </p>
        <?php 
            echo LinkPager::widget([
                'pagination' => $pagination,
                'options' => [
                    'class' => 'pag'
                ]
            ]);
        ?>
    </div>
</div>
				
            </div>
        </div>
    </div>
</div>

<?php $this->registerJs(<<<JS
    //ФИЛЬТР + ПОЛЗУНОК ФИЛЬТРА ЦЕНЫ
    var max_price = $max_price;
    var get_params = $filter_params;
    
    $('.filter-body-select').select2({
        minimumResultsForSearch: Infinity
    });

    var slider = document.getElementById('slider');
    noUiSlider.create(slider, {
        start : [10, max_price],
        connect : true,
        range : {
            min : 10,
            max : max_price
        },
        format : wNumb({
            decimals : 0
        })
    });
    var min_price_input = document.getElementById('min-price');
    var max_price_input = document.getElementById('max-price');
    slider.noUiSlider.on('update', function(values, handle){
        if(handle === 0) min_price_input.value = values[0];
        else if(handle === 1) max_price_input.value = values[1];
    });
    
    if(get_params['price_min'] && get_params['price_max']){
        slider.noUiSlider.set([
            get_params['price_min'],
            get_params['price_max']
        ]);
    }
JS
); ?>