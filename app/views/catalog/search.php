<?php
/** @var $items \yii\cms\modules\catalog\api\ItemObject[] */

use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\cms\modules\catalog\api\Catalog;

$pagination = Catalog::pagination();

$search_query = Yii::$app->getRequest()->getQueryParam('q');

$this->title = $page->seo('title', $page->title . ': ' . $search_query);
$this->params['breadcrumbs'][] = $page->title;

?>

<!-- MAIN CONTENT -->
<div id="main" role="main">
    <div class="container">
        <div class="row">
            <div class="main_content col-sm-12">
				
					

<h2 class="page_heading"><?=Html::encode($this->title);?></h2>
<hr>


<?php if(count($items) > 0): ?>

<!-- products sorting -->
<div class="product_listing_controls">
    <p class="products_count">
        <?=count($items);?> товаров из <?=$pagination->totalCount;?>
    </p>
    <?php 
        echo LinkPager::widget([
            'pagination' => $pagination,
            'options' => [
                'class' => 'pag'
            ]
        ]);
    ?>
</div>


<div>
    <!-- products listing -->
    <div class="product_listing_main row">
    
    <?php foreach ($items as $item) : ?>
        <?= $this->render('../catalog/_item', ['item' => $item]) ?>
    <?php endforeach; ?>
    
    </div>

    <!-- pagination -->
    <div class="product_listing_controls">
        <p class="products_count">
            <?=count($items);?> товаров из <?=$pagination->totalCount;?>
        </p>
        <?php 
            echo LinkPager::widget([
                'pagination' => $pagination,
                'options' => [
                    'class' => 'pag'
                ]
            ]);
        ?>
    </div>
</div>

<?php else: ?>

<hr>
<h3 class="text-center">По данному запросу ничего не найдено</h3>
    
<?php endif ?>


				
            </div>
        </div>
    </div>
</div>