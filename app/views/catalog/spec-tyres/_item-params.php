<?php
use app\helpers\CatalogHelper;

/** @var $item \yii\cms\modules\catalog\api\ItemObject */

?>
<?php if(!empty($item->data->axle)): ?>
<div>
    Ось: <?=implode(', ', $item->data->axle);?>
</div>
<?php endif ?>

<?php if(!empty($item->data->type)): ?>
<div>
    Протектор: <?=implode(', ', $item->data->type);?>
</div>
<?php endif ?>