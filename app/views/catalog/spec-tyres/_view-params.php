<?php
use app\helpers\CatalogHelper;

/** @var $item \yii\cms\modules\catalog\api\ItemObject */

$available = ($item->available == '0' || !$item->available)
    ? 'на заказ' : $item->available;

?>
<div>Доступно, шт: <span><?=$available;?></span></div>
<div>Диаметр: <span><?=$item->data->diameter;?></span></div>
<div>Ширина профиля, мм: <span><?=$item->data->width;?></span></div>
<div>Высота профиля, мм: <span><?=$item->data->height;?></span></div>

<?php if(!empty($item->data->type)): ?>
<div>Тип протектора: <span><?=implode(',', $item->data->type);?></span></div>
<?php endif; ?>

<?php if(!empty($item->data->axle)): ?>
<div>Ось: <span><?=implode('<br>', $item->data->axle);?></span></div>
<?php endif; ?>