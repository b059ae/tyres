<?php
/* @var $page \yii\cms\modules\page\api\PageObject */

/* @var $cats \yii\cms\modules\catalog\api\CategoryObject[] */

use yii\helpers\Html;
use yii\helpers\Url;

//$this->title = $page->seo('title');
$this->title = 'Каталог';
$this->params['breadcrumbs'][] = 'Каталог';
/*$this->registerMetaTag([
    'name' => 'description',
    'content' => $page->seo('description')
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $page->seo('keywords')
]);*/
?>

<div id="main" role="main">
    <div class="container">
        <div class="row">
            <div class="main_content col-sm-12 ">
                <h2 class="page_heading">Каталог</h2>
                <div class="collection_listing_main">
                    <div class="row auto-clear">
                        
                        <?php foreach($cats as $cat): ?>
                        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 collection_listing_item wow">
                            <div class="collection_img">
                                <a href="<?= Url::to(['catalog/' . $cat->slug]) ?>">
                                    <?=Html::img($cat->thumb(480, 480)); ?>
                                </a>
                            </div>
                            <div class="collection_info">
                                <h4 class="collection_name">
                                    <a href="<?= Url::to(['catalog/' . $cat->slug]) ?>">
                                        <?= $cat->getTitle(); ?>
                                    </a>
                                </h4>
                                <p class="collection_products"></p>
                                <div class="collection_desc"><?= $cat->description;?></div>
                                <a class="btn" href="<?= Url::to(['catalog/' . $cat->slug]) ?>">Просмотр</a>
                            </div>
                        </div>
                        <?php endforeach; ?>
                        
                        
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>