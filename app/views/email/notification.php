<?php
use app\helpers\CatalogHelper;
use yii\cms\models\Setting;

$site_url = \Yii::$app->request->hostInfo;
$logo_file = $site_url . '/invoice-img/logo.png';
?>

<table cellpadding="10">
    <tr>
        <td colspan="2"><b>Спасибо, что выбрали нашу компанию!</b></td>
    </tr>
    <tr>
        <td>Номер вашего заказа: </td>
        <td><b><?=$order->id;?></b></td>
    </tr>

<?php foreach($order->goods as $good): ?>
    <tr style="background:#eee;">
        <td>Наименование:</td>
        <td><i><?=CatalogHelper::getItemTitle($good->item);?></i></td>
    </tr>
    <tr style="background:#eee;">
        <td>Стоимость:</td>
        <td><b><?=($good->item->price);?> руб.</b></td>
    </tr>
    <tr style="background:#eee;">
        <td>Кол-во:</td>
        <td><b><?=$good->count;?></b></td>
    </tr>
<?php endforeach; ?>   
    
    <tr>
        <td>Сумма заказа: </td>
        <td><b><?=$order->cost;?> руб.</b></td>
    </tr>
</table>

<br><br>

<table cellpadding="10">    
    <tr>
        <td colspan="2">
            <img src="<?=$logo_file;?>" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <?=Setting::get('company');?>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            г. Аксай, ул. Западная, 33
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <?=Setting::get('phone');?>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <a href="<?=$site_url;?>">www.<?=$_SERVER['HTTP_HOST'];?></a>
        </td>
    </tr>
</table>

