<?php
use app\helpers\CatalogHelper;
use yii\cms\models\Setting;

$site_url = \Yii::$app->request->hostInfo;
$logo_file = $site_url . '/invoice-img/logo.png';
?>

<table cellpadding="10" style="border-collapse:collapse;">
    <tr>
        <td colspan="2"><h3>Здравствуйте!</h3></td>
    </tr>
    <tr>
        <td colspan="2">
            Спасибо за Ваш заказ.<br>
            Как только мы получим ответ от платежной системы об оплате заказа,
            товар будет зарезервирован.
        </td>
    </tr>
    <tr>
        <td colspan="2">
            Для получения заказа необходимо предъявить паспорт,
            соответствующий данным, которые были указаны при оформлении заказа.
        </td>
    </tr>
    
    <tr style="background:#eee; border-bottom:1px solid #000;">
        <td>Номер вашего заказа:</td>
        <td><b><?=$order->id;?></b></td>
    </tr>

<?php foreach($order->goods as $good): ?>
    <tr style="background:#eee;">
        <td>Наименование:</td>
        <td><i><?=CatalogHelper::getItemTitle($good->item);?></i></td>
    </tr>
    <tr style="background:#eee;">
        <td>Стоимость:</td>
        <td><b><?=$good->item->price;?> руб.</b></td>
    </tr>
    <tr style="background:#eee;">
        <td>Кол-во:</td>
        <td><b><?=$good->count;?></b></td>
    </tr>
<?php endforeach; ?>   
    
    <tr height="100">
        <td>Общая сумма заказа:</td>
        <td><b><?=$order->cost;?> руб.</b></td>
    </tr>
    
    <tr>
        <td colspan="2">
            <img src="<?=$logo_file;?>" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <?=Setting::get('company');?>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            г. Аксай, ул. Западная, 33
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <?=Setting::get('phone');?>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <a href="<?=$site_url;?>">www.<?=$_SERVER['HTTP_HOST'];?></a>
        </td>
    </tr>
</table>
