<?php
/* @var $good \yii\cms\modules\shopcart\api\GoodObject */
/* @var $model \app\models\shopcart\ShopcartUpdate */

use app\helpers\CatalogHelper;
use yii\bootstrap\Html;

use app\assets\AppAsset;
$asset = AppAsset::register($this);

?>
<div class="row cart-item">
    <div class="col-lg-3 col-md-3 col-sm-3">
        <div class="item_image">
            <?=Html::a(
                Html::img(CatalogHelper::getItemThumb($good->item, $asset, 200, 200)),
                [
                    'catalog/view',
                    'category'=>$good->item->cat->slug,
                    'slug'=>$good->item->slug
                ]
            );?>
            
        </div>
    </div>

    <div class="col-lg-9 col-md-9 col-sm-9">
        <div class="item_remove pull-right">
            <?=Html::a('<i class="fa fa-times"></i>', ['shopcart/remove', 'id'=>$good->id]);?>
        </div>
        <div class="product_name">
            <?=Html::a(
                CatalogHelper::getItemTitle($good->item),
                [
                    'catalog/view',
                    'category'=>$good->item->cat->slug,
                    'slug'=>$good->item->slug
                ]
            );?>
        </div>
        <div class="item_price">
            <div class="row">
                <div class="col-sm-4">
                    <div class="price"><span class="money"><?=$good->price;?> руб.</span></div>
                </div>
                <div class="col-sm-8">
                    <div class="qty">
                        <label>
                            Кол-во:
                            <?=Html::activeTextInput($model, 'count['.$good->id.']', [
                                'value' => $good->count,
                                'class' => 'input-small form-control form-count',
                                'type' => 'number',
                                'min' => '1',
                                'data-price' => $good->item->price,
                            ]);?>
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="total col-sm-12">
                    <h3>
                        Всего: 
                        <span class="money">
                            <span class="good-cost">
                                <?=($good->price*$good->count);?>
                            </span> руб.
                        </span>
                    </h3>
                </div>
            </div>
        </div>
    </div>
</div>