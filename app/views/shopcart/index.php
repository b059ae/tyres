<?php
/* @var $model_update \app\models\shopcart\ShopcartUpdate */
/* @var $model_payment \app\models\shopcart\ShopcartPayment */
/* @var $model_invoice \app\models\shopcart\ShopcartInvoice */
/* @var $model_reserve \app\models\shopcart\ShopcartReserve */
/* @var $goods \yii\cms\modules\shopcart\api\GoodObject[] */

use yii\cms\modules\shopcart\api\Shopcart;
use yii\widgets\Menu;
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use yii\cms\models\Setting;

$this->title = 'Корзина';
$this->params['breadcrumbs'][] = $this->title;

$goods_count = count($goods);
$payment_online = (int) Setting::findOne(['name' => 'payment_online'])->value;

?>
<div id="main" role="main">
    <div class="container">
        <div class="row">
            <div class="main_content col-sm-9 col-sm-push-3">
                <div class="cart-scope">

                    <div class="page_header">
                        <h2 class="page_heading">Ваша корзина</h2>
                    </div>

                    <?php $add_message = Yii::$app->session->getFlash('add_message'); ?>
                    <?php if (!empty($add_message)): ?>
                        <p class="alert alert-success">
                            <?=$add_message; ?>
                        </p>
                    <?php endif; ?>
                    
                    <?php if($goods_count): ?>
                        <hr>
                        <div class="cart_buttons margin-0">
                            <?php if($payment_online === 1): ?>
                            <?=Html::button("Оплатить заказ VISA | MasterCard", [
                                'class' => 'btn btn-primary btn-lg margin-r-10 margin-b-10',
                                'data-toggle' => 'modal',
                                'data-target' => '#modal-payment',
                            ]);?>
                            <?php endif; ?>

                            <?=Html::button("Выставить счет", [
                                'class' => 'btn btn-primary btn-lg margin-r-10 margin-b-10',
                                'data-toggle' => 'modal',
                                'data-target' => '#modal-invoice',
                            ]);?>

                            <?=Html::button("Зарезервировать товар", [
                                'class' => 'btn btn-primary btn-lg margin-r-10 margin-b-10',
                                'data-toggle' => 'modal',
                                'data-target' => '#modal-reserve',
                            ]);?>
                        </div>
                        <hr class="margin-t-10">
                    <?php endif; ?>
                    
                    <div class="page_content">
                        <?php if ($goods_count): ?>
                            <?php $form = ActiveForm::begin([
                                'action' => ['shopcart/index', 'action' => 'update']
                            ]); ?>
                                <div class="cart-list">
                                    <?php foreach($goods as $good): ?>
                                        <?=$this->render('_cart-item', [
                                            'good' => $good,
                                            'model' => $model_update,
                                        ]); ?>
                                    <?php endforeach; ?>
                                </div>
                                <div class="cart_subtotal">
                                    <h3>
                                        <?=Html::submitButton('Обновить корзину', [
                                            'class' => 'btn btn-alt btn-lg'
                                        ]);?>
                                        <span class="right">
                                            <span>Итого:</span>
                                            &nbsp;
                                            <span class="money">
                                                <span class="order-cost"><?=Shopcart::cost();?></span> руб.
                                            </span>
                                        </span>
                                    </h3>
                                </div>
                                <div class="cart_buttons margin-t-20 margin-b-10">
                                    
                                    <?php if($payment_online === 1): ?>
                                    <?=Html::button("Оплатить заказ VISA | MasterCard", [
                                        'class' => 'btn btn-primary btn-lg margin-r-10 margin-b-10',
                                        'data-toggle' => 'modal',
                                        'data-target' => '#modal-payment',
                                    ]);?>
                                    <?php endif; ?>

                                    <?=Html::button("Выставить счет", [
                                        'class' => 'btn btn-primary btn-lg margin-r-10 margin-b-10',
                                        'data-toggle' => 'modal',
                                        'data-target' => '#modal-invoice',
                                    ]);?>

                                    <?=Html::button("Зарезервировать товар", [
                                        'class' => 'btn btn-primary btn-lg margin-r-10 margin-b-10',
                                        'data-toggle' => 'modal',
                                        'data-target' => '#modal-reserve',
                                    ]);?>
                                    
                                </div>
                            <?php ActiveForm::end(); ?>
                        <?php else: ?>
                            <h3>На данный момент корзина пуста</h3>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <div class="sidebar col-sm-3 sidebar_left col-sm-pull-9">
                <div class="sidebar_widget sidebar_widget__collections">
                    <div class="widget_content">
                        <?=Menu::widget([
                            'items' => yii\cms\modules\menu\api\Menu::items('shopcart'),
                        ]);?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if($payment_online === 1): ?>
<?=$this->render('_shopcart-modal', [
    'title' => 'Оплатить заказ VISA | MasterCard',
    'id' => 'payment',
    'model' => $model_payment,
]);?>
<?php endif; ?>

<?=$this->render('_shopcart-modal', [
    'title' => 'Выставить счет',
    'id' => 'invoice',
    'model' => $model_invoice,
]);?>
<?=$this->render('_shopcart-modal', [
    'title' => 'Зарезервировать товар',
    'id' => 'reserve',
    'model' => $model_reserve,
]);?>
<?php

$payment_fn = $model_payment->formName();
$invoice_fn = $model_invoice->formName();
$reserve_fn = $model_reserve->formName();

$this->registerJs(<<<JS
        
    jQuery.fn.getGoodCost = function(){
        var \$this = jQuery(this);
        return parseInt(\$this.val()) * parseInt(\$this.data('price'));
    };
    
    jQuery('.form-count').on('input', function(){
        var \$this = jQuery(this),
            item = \$this.closest('.cart-item'),
            good_price_field = item.find('.good-cost'),
            order_cost_field = jQuery('.order-cost');
        
        // Выводим стоимость позиции
        good_price_field.html(\$this.getGoodCost());
        // Выводим стоимость заказа
        order_cost_field.html(countTotalCost());
        
        // Добавляем hidden-поле count во все три формы, и меняем его значение
        \$this
            .synchWith('#form-payment', '$payment_fn')
            .synchWith('#form-invoice', '$invoice_fn')
            .synchWith('#form-reserve', '$reserve_fn');
        
    }).trigger('input');
    
    function countTotalCost(){
        var cost = 0,
            inputs = jQuery('.form-count');
        
        inputs.each(function(){
            cost += jQuery(this).getGoodCost();
        });
        return cost;
    }
JS
);
?>