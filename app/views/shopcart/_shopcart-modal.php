<?php
/* @var $model \app\models\shopcart\Shopcart */
/* @var $title string */
/* @var $id string */

use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\widgets\MaskedInput;
use yii\bootstrap\Html;

Modal::begin([
    'header' => '<h2 class="text-center margin-0">' . $title . '</h2>',
    'id' => 'modal-' . $id
]);

$form = ActiveForm::begin([
    'id' => 'form-' . $id,
    'action' => ['shopcart/index', 'action' => $id],
    'class' => 'shopcart-form',
]);
?>
    <?=$form->field($model, 'name')->textInput();?>
    <?=$form->field($model, 'phone')
        ->textInput()
        ->widget(MaskedInput::className(), ['mask' => '+7(999)999-99-99']);?>
    <?=$form->field($model, 'email')->textInput();?>
    <?php /*=$form->field($model, 'delivery')->dropDownList($model->getAttributeValueLabels('delivery'));?>
    <?=$form->field($model, 'city')->textInput();?>
    <?=$form->field($model, 'address')->textInput();*/?>
    <?php if($id === 'invoice'): ?>
        <?=$form->field($model, 'company')->textInput();?>
        <?=$form->field($model, 'inn')->textInput();?>
    <?php endif; ?>
    
    <?php if($id === 'payment'): ?>
        <?=$form->field($model, 'courier_name')->textInput();?>
    <?php endif; ?>

    <div class="form-group text-center">
        <?=Html::submitButton('Оформить заказ', [
            'class' => 'btn btn-primary btn-lg'
        ]);?>
    </div>
<?php
ActiveForm::end();
Modal::end();

$form_name = strtolower($model->formName());
$delivery_input_id = implode('-', [$form_name, 'delivery']);
$city_field_class = implode('-', ['field', $form_name, 'city']);
$address_field_class = implode('-', ['field', $form_name, 'address']);

$this->registerJs(<<<JS
    jQuery('#form-$id #$delivery_input_id').on('change', function(){
        var city_and_address_fields = jQuery('#form-$id .$city_field_class, #form-$id .$address_field_class');
        if(jQuery(this).val() == '1'){
            city_and_address_fields.slideDown();
        }
        else{
            city_and_address_fields.slideUp();
        }
    }).trigger('change');
JS
);
?>