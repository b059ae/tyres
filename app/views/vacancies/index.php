<?php
/* @var $cat \yii\cms\modules\article\api\CategoryObject */
/* @var $articles \yii\cms\modules\article\api\CategoryObject[] */

use yii\bootstrap\Html;
use yii\helpers\Url;

$this->registerMetaTag([
    'name' => 'description',
    'content' => $cat->seo('description')
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $cat->seo('keywords')
]);
$title = Html::encode($cat->title);
$this->title = $cat->seo('title', $title);
$this->params['breadcrumbs'][] = $title;
?>
<div id="main" role="main">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="page_heading"><?=$title;?></h2>
                <hr>
            </div>
        </div>

        <div class="row auto-clear">
            <?php if(count($cat->items)): ?>
            
                <?php foreach($cat->items as $article):  ?>
                <div class="col-xs-12 col-sm-12 col-md-10 col-lg-8 wow">
                    <h3 class="wow">
                        <a href="<?=Url::toRoute(['vacancies/view', 'slug' => $article->slug]);?>">
                            <?=$article->title;?>
                        </a>
                    </h3>
                    <p class="wow"><?=strip_tags($article->short);?></p>
                </div>
                <?php endforeach; ?>
            
            <?php else: ?>
                
                <div class="col-xs-12">
                    <h3>На данный момент открытых вакансий нет</h3>
                </div>
                
            <?php endif; ?>
        </div>
    </div>
</div>