<?php
/* @var $article \yii\cms\modules\article\api\ArticleObject */
/* @var $cat \yii\cms\modules\article\api\CategoryObject */

use yii\bootstrap\Html;

$this->registerMetaTag([
    'name' => 'description',
    'content' => $article->seo('description')
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $article->seo('keywords')
]);
$title = Html::encode($article->title);
$cat_title = Html::encode($cat->title);
$this->title = $article->seo('title', $title);
$this->params['breadcrumbs'][] = ['label' => $cat_title, 'url' => ['/vacancies']];
$this->params['breadcrumbs'][] = $title;
?>
<div id="main" role="main">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="page_heading"><?=Html::encode($title);?></h2>
                <hr>
            </div>
        </div>
        <div class="row margin-b-40">
            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-8 wow">
                <?=$article->text;?>
            </div>
        </div>
    </div>
</div>