<?php
/* @var $article \yii\cms\modules\article\api\ArticleObject */
/* @var $cat \yii\cms\modules\article\api\CategoryObject */

use yii\bootstrap\Html;

$this->registerMetaTag([
    'name' => 'description',
    'content' => $article->seo('description')
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $article->seo('keywords')
]);
$this->title = $article->seo('title', $article->title);
$this->params['breadcrumbs'][] = ['label' => $cat->title, 'url' => ['/services']];
$this->params['breadcrumbs'][] = $article->title;
?>
<div id="main" role="main">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="page_heading"><?=Html::encode($article->title);?></h2>
            </div>
        </div>
        <div class="row margin-b-40">
            <div class="col-sm-12">
                <div class="article">
                    <div class="article_header">
                        <div class="product_name"><?=Html::encode($article->seo('h1'));?></div>
                    </div>
                    <div class="rte">
                        <?=$article->text;?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row margin-b-40">
            <div class="col-sm-12 text-center">
                <?=Html::button('Узнать стоимость', [
                    'class' => 'btn btn-info btn-lg',
                    'data-toggle' => 'modal',
                    'data-target' => '#services-modal',
                ]);?>
            </div>
        </div>
        <div class="row margin-b-40">
            <div class="col-sm-12 text-center">
                <h2 class="page_heading">Примеры наших работ</h2>
                <hr>
                <div class="row auto-clear">
                <?php foreach($article->photos as $photo): ?>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 margin-b-20">
                        <?=Html::img($photo->thumb(300, 300), [
                            'class' => 'img-responsive img-thumbnail center-block margin-b-5',
                        ]);?>
                        <span class="services-case-text">
                            <?=str_replace("\n", '<br>', $photo->description);?>
                        </span>
                    </div>
                <?php endforeach;?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 text-center">
                <?=Html::button('Получить прайс', [
                    'class' => 'btn btn-info btn-sm',
                    'data-toggle' => 'modal',
                    'data-target' => '#services-price-modal',
                ]);?>
            </div>
        </div>
    </div>
</div>
<?=$this->render('_services-modal', ['model' => $model]);?>
<?=$this->render('_services-price-modal', ['model' => $price_model]);?>