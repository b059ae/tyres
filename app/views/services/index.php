<?php
/* @var $cat \yii\cms\modules\article\api\CategoryObject */
/* @var $articles \yii\cms\modules\article\api\CategoryObject[] */

use yii\bootstrap\Html;
use yii\helpers\Url;

$this->registerMetaTag([
    'name' => 'description',
    'content' => $cat->seo('description')
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $cat->seo('keywords')
]);
$this->title = $cat->seo('title', $cat->title);
$this->params['breadcrumbs'][] = $cat->title;
?>
<div id="parallax_block" class="homepage_info page_container">
    <div class="container">
        <h2 id="homepage_info__title">
            <span class="wrap-1 wow">Наши услуги</span>
        </h2>

        <div class="row auto-clear margin-t-40">
            <?php foreach($cat->items as $service):  ?>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 homepage_info_item wow services-item">
                <h3 class="wow">
                    <a href="<?=Url::toRoute(['services/view', 'slug' => $service->slug]);?>">
                        <?=$service->title;?>
                    </a>
                </h3>
                <p class="wow"><?=strip_tags($service->short);?></p>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<style>
    footer{margin: 0;}
    .homepage_info{margin-top: 0;}
    .breadcrumb_wrap{margin: 0;}
</style>