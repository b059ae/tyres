<?php
/* @var $articles \yii\cms\modules\article\api\ArticleObject[] */

use yii\helpers\Url;
?>
<?php if (count($cat->items) > 0): ?>
    <?php foreach ($cat->items as $item): ?>
        <div class="wow blog-article blog-article__1 animated" style="visibility: visible;">
            <div class="article_header">
                <div class="product_name">
                    <a href="<?=Url::to(['/services/'.$item->slug]) ?>">
                        <?=$item->title;?>
                    </a>
                </div>
            </div>
            <div class="rte">
                <p><?=$item->short;?></p>
            </div>
            <a href="<?=Url::to(['/services/'. $item->slug]) ?>"
               class="blog-article_read-more btn btn-info">
                Читать далее
            </a>
        </div>
    <?php endforeach; ?>
<?php endif; ?>