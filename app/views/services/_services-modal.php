<?php
/* @var $model \app\models\ServicesForm */

use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\widgets\MaskedInput;

Modal::begin([
    'header' => '<h2 class="text-center margin-0">Узнать стоимость</h2>',
    'id' => 'services-modal',
]);
$form = ActiveForm::begin([
    'id' => 'services-form',
]);
?>
    <?=$form->field($model, 'name')->textInput();?>
    <?=$form->field($model, 'phone')
        ->textInput()
        ->widget(MaskedInput::className(), ['mask' => '+7 (999) 999-99-99']);?>

    <div class="form-group text-center">
        <?=Html::submitButton('Отправить', [
            'class' => 'btn btn-primary btn-lg'
        ]);?>
    </div>
<?php
ActiveForm::end();
Modal::end();
?>