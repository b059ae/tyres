<?php
/* @var $model \app\models\ServicesPriceForm */

use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

Modal::begin([
    'header' => '<h2 class="text-center margin-0">Получить прайс</h2>',
    'id' => 'services-price-modal',
]);
$form = ActiveForm::begin([
    'id' => 'services-price-form',
]);
?>
    <?=$form->field($model, 'email')->textInput()->label('Введите ваш E-mail');?>

    <div class="form-group text-center">
        <?=Html::submitButton('Отправить', [
            'class' => 'btn btn-primary btn-lg'
        ]);?>
    </div>
<?php
ActiveForm::end();
Modal::end();
$this->registerJs(<<<JS
    jQuery('#services-price-form').on('beforeSubmit.yii', function() {
        jQuery('#services-price-modal').modal('hide');
        return true;
    });
JS
);
?>