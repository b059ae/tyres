<?php 
/** @var $page \yii\cms\modules\page\api\PageObject */
/** @var $checkout_form app\modules\customer\models\CheckoutForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\cms\modules\shopcart\api\Shopcart;
use yii\widgets\MaskedInput;

$this->title = $page->seo('title', $page->title);
$this->params['breadcrumbs'][] = $page->title;

$goods = Shopcart::goods();

?>
<div id="main" role="main">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="page_heading"><?=Html::encode($this->title);?></h2>
                <hr>
            </div>
        </div>
        <div class="row">
            
            <div class="main_content col-sm-6">
                <?php $form = ActiveForm::begin([
                    'id' => 'checkout-form',
                    'options' => [],
                ]); ?>
                
                <?=$form->field($checkout_form, 'name');?>
                <?=$form->field($checkout_form, 'email');?>
                <?=$form->field($checkout_form, 'phone')->widget(MaskedInput::className(), [
                    'mask' => '+7(999)999-99-99',
                ]);?>
                <?=$form->field($checkout_form, 'city');?>
                <?=$form->field($checkout_form, 'address');?>
                <?=$form->field($checkout_form, 'delivery')->dropDownList([
                    'self' => 'Самовывоз',
                    'shipping' => 'Транспортная компания',
                ]);?>
                
                <div class="form-group text-center">
                    <?= Html::submitButton('Оформить заказ', ['class' => 'btn btn-primary']) ?>
                </div>
                
                <?php ActiveForm::end(); ?>
            </div>
            
            <script type="text/javascript">
                window.onload = function(){
                    $('#checkoutform-delivery').on('change', function(){
                        var blocks = $('.field-checkoutform-city, .field-checkoutform-address');
                        blocks.css('display', (this.value == 'self' ? 'none' : 'block'));
                        if(this.value == 'self') blocks.removeClass('required');
                        else blocks.addClass('required');
                    }).trigger('change');
                }
            </script>
            
            <div class="col-sm-6">
                <?php if(count($goods)): ?>
                <?php foreach($goods as $good): ?>
                
                <div class="cart-list">
                <?=$this->render('_cart-item', ['good' => $good]);?>
                </div>
                
                <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>