<?php
/* @var $good \yii\cms\modules\shopcart\api\GoodObject */

use app\helpers\CatalogHelper;
use yii\helpers\Url;

use app\assets\AppAsset;
$asset = AppAsset::register($this);

?>
<div class="row cart-item">
    <div class="col-sm-5">
        <div class="item_image">
            <a href="<?=Url::to(['catalog/view',
                'category'=>$good->item->getCat()->slug,
                'slug'=>$good->item->slug]);?>">  
                <img src="<?=CatalogHelper::getItemThumb($good->item, $asset, 200, 200);?>" />
            </a>
        </div>
    </div>

    <div class="col-sm-7">
        <div class="product_name">
            <a href="<?=Url::to(['catalog/view', 'category'=>$good->item->getCat()->slug, 'slug'=>$good->item->slug]);?>">
               <?=CatalogHelper::getItemTitle($good->item);?>
            </a>
        </div>
        <div class="item_price">
            <div class="row">
                <div class="col-sm-4">
                    <div class="price"><span class="money"><?=($good->price);?> руб.</span></div>
                </div>
                <div class="col-sm-8">
                    <div class="qty">
                        Кол-во: <?=$good->count;?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="total col-sm-12">
                    <h3>Всего: <span class="money"><?=($good->price*$good->count);?> руб.</span></h3>
                </div>
            </div>
        </div>
    </div>
</div>