<?php 
/** @var $page \yii\cms\modules\page\api\PageObject */

use yii\helpers\Html;

//$this->title = $page->seo('title', $page->title);
//$this->params['breadcrumbs'][] = $page->title;
$this->title = "Успех! Ваш заказ отправлен на обработку";

?>
<div id="main" role="main">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="page_heading"><?=Html::encode($this->title);?></h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="main_content col-sm-8">
                Наш менеджер свяжится с вами в кротчайшие сроки.
                <?=Html::a('Посмотреть историю заказов', '/account');?>
            </div>
        </div>
    </div>
</div>
             