<?php 
/** @var $page \yii\cms\modules\page\api\PageObject */
/** @var $signup_form app\modules\customer\models\SignupForm */
/** @var $orders yii\cms\modules\shopcart\api\OrderObject[] */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Menu;
use app\helpers\CatalogHelper;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;

$this->title = $page->seo('title', $page->title);
$this->params['breadcrumbs'][] = $page->title;


?>
<div id="main" role="main">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="page_heading"><?=Html::encode($this->title);?></h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-9 mb-50 col-sm-push-3">
                
                <div class="row">
                    <div class="col-sm-12">
                        
                        <?php $message = Yii::$app->session->getFlash('message'); ?>
                        <?php if(!empty($message)): ?>      
                        <p class="alert alert-success">
                            <?=$message; ?>
                        </p>
                        <?php endif; ?>
                        
                        <?php $error = Yii::$app->session->getFlash('error'); ?>
                        <?php if(!empty($error)): ?>      
                        <p class="alert alert-danger">
                            <?=$error; ?>
                        </p>
                        <?php endif; ?>
                        
                        <h3>Мои заказы</h3>
                        <?php if(count($orders)): ?>
                        <table id="account_orders" class="table">
                            <thead>
                                <tr>
                                    <th>Дата</th>
                                    <th>Номер</th>
                                    <th>Статус</th>
                                    <th>Кол-во наименований</strong></th>
                                    <th>Стоимость, руб.</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach($orders as $order): ?>
                                <tr>
                                    <td><?=date('d.m.Y', $order->model->time);?></td>
                                    <td><?=$order->id;?></td>
                                    <td><?=$order->status;?></td>
                                    <td><?=count($order->goods);?></td>
                                    <td><?=$order->cost;?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        
                        
                        <?php foreach($orders as $order): ?>
                        <div class="blank-space"></div>
                        <h3>Заказ №<?=$order->id;?> от <?=date('d.m.Y', $order->model->time);?></h3>
                        <div class="cart-list">
                            <?php foreach($order->goods as $good): ?>
                            <?=$this->render('_cart-item', ['good' => $good]);?>
                            <?php endforeach; ?>
                        </div>
                        <div class="order-total">
                            <div class="left">
                                Статус заказа: <?=CatalogHelper::statusBadge($order);?>
                                <button type="button"
                                    <?php /*=Url::to(['/account/download', 'order_id' => $order->id]);*/ ?>
                                    data-toggle="modal"
                                    data-target="#invoice-popup"
                                    data-id="<?=$order->id;?>"
                                   class="btn btn-primary ml-20">
                                    Скачать счет
                                </button>
                            </div>
                            <div class="right">
                                Итого: <strong><?=$order->cost;?> руб.</strong>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <?php endforeach; ?>
                        
                        <?php else: ?>
                        <p>
                            На данный момент заказов не было
                        </p>
                        <?php endif; ?>
                        
                    </div>
                </div>
                
                
                <?php if(count($orders)): ?>
                
                <?php endif; ?>
            </div>
            
            
            <div class="sidebar col-sm-3 sidebar_left col-sm-pull-9">				
                <div class="sidebar_widget sidebar_widget__collections">
                    <div class="widget_content">
                        <?= Menu::widget([
                            'items' => yii\cms\modules\menu\api\Menu::items('shopcart'),
                        ]); ?>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
<?php Modal::begin([
    'header' => '<strong>Скачать счет №</strong><strong id="invoice-number"></strong>',
    'id' => 'invoice-popup'
]); ?>
    <?php $form = ActiveForm::begin(['action' => '/account/download']); ?>
        <?=$form->field($form_model, 'company');?>
        <?=$form->field($form_model, 'inn');?>
        <?=Html::activeHiddenInput($form_model, 'id', ['id' => 'invoice-id']);?>
        <div class="form-group text-center">
            <?= Html::submitButton('Скачать счет', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>
<?php Modal::end(); ?>
<script type="text/javascript">
    window.onload = function(){
        jQuery('#invoice-popup').appendTo('body');
        jQuery('button[data-target="#invoice-popup"]').on('click', function(){
            var $this = jQuery(this);
            document.getElementById('invoice-number').innerHTML = $this.data('id');
            document.getElementById('invoice-id').value = $this.data('id');
        });
    };
</script>