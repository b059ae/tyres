<?php
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\widgets\MaskedInput;

Modal::begin([
    'id' => 'feedback-modal',
    'header' =>
        '<h4 class="text-center">
            Оставьте заявку, и мы предоставим
            <br>вам необходимую информацию
        </h4>'
]);?>
<?php $form = ActiveForm::begin([]);?>

<?=$form->field($model, 'name')->textInput();?>
<?=$form->field($model, 'phone')->textInput()->widget(MaskedInput::className(), [
    'mask' => '+7 (999) 999-99-99',
]);?>

<p class="text-center"><?=Html::submitButton('Отправить', ['class' => 'btn btn-lg']);?></p>

<?php ActiveForm::end(); ?>
<?php Modal::end(); ?>
