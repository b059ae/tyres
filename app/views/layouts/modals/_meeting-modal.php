<?php
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use kartik\datetime\DateTimePicker;
use yii\widgets\MaskedInput;

Modal::begin([
    'id' => 'meeting-modal',
    'header' => '<h2 class="text-center">Записаться</h2>'
]);?>
<?php $form = ActiveForm::begin([]);?>

<?=$form->field($model, 'name')->textInput();?>
<?=$form->field($model, 'phone')->textInput()->widget(MaskedInput::className(), [
    'mask' => '+7 (999) 999-99-99',
]);?>
<?=$form->field($model, 'datetime')->widget(DateTimePicker::className(), [
    'options' => [
        'value' => Yii::$app->formatter->asDatetime($model->datetime),
        'style' => 'background-color:#fff; cursor:pointer;',
    ],
    'readonly' => true,
    'pluginOptions' => [
        'autoclose' => true,
        'ignoreReadonly' => true,
        'daysOfWeekDisabled' => [0],
        'format' => 'd-mm-yyyy HH:ii',
        'startDate' => date('d-m-Y h:i', time() + (30 * 60)),
        'hoursDisabled' => '0,1,2,3,4,5,6,7,8,18,19,20,21,22,23',
        'minView' => 1,
    ],
    'pluginEvents' => [
        'changeDate' => 'function(e){
            e.date.setMinutes(0);
            var toStr = function(n){
                    n = n.toString();
                    return n.length == 1 ? "0" + n : n;
                },
                day = toStr(e.date.getDate()),
                month = toStr(parseInt(e.date.getMonth()) + 1),
                year = e.date.getFullYear(),
                minutes = toStr(e.date.getMinutes()),
                hours = toStr(e.date.getHours());
            window.formDatetime = day + "-" + month + "-" + year + " " + hours + ":" + minutes;
        }',
        'hide' => 'function(e){
            jQuery("#meetingform-datetime").val(window.formDatetime);
        }',
    ]
]);?>
<p class="text-center"><?=Html::submitButton('Записаться', ['class' => 'btn btn-lg']);?></p>

<?php ActiveForm::end(); ?>
<?php Modal::end(); ?>
