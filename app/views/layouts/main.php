<?php

use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\Menu;
use yii\widgets\Breadcrumbs;
use yii\cms\models\Setting;
use app\helpers\CatalogHelper;
use yii\cms\modules\shopcart\api\Shopcart;
use yii\cms\modules\text\api\Text;



$asset = \app\assets\AppAsset::register($this);

$email = Setting::findOne(['name' => 'email'])->value;
$phone = Setting::findOne(['name' => 'phone'])->value;
$address = Setting::findOne(['name' => 'address'])->value;

?>
<?php $this->beginPage(); ?>
    <!DOCTYPE html>
    <!--[if lt IE 7]>
    <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
    <!--[if IE 7]>
    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
    <!--[if IE 8]>
    <html class="no-js lt-ie9" lang="en"> <![endif]-->
    <!--[if IE 9 ]>
    <html class="ie9 no-js"> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <html class="no-js"> <!--<![endif]-->
    <head>
        <?=Setting::get('ga_code');?>
        <meta charset="utf-8">
        <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

        <!-- PAGE TITLE -->
        <title><?= Html::encode($this->title); ?></title>

        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/favicon.ico" type="image/x-icon">

        <?= \yii\cms\widgets\Metrika::widget(); ?>
        <?php $this->head(); ?>
    </head>


    <body id="tyres" class="<?=CatalogHelper::getBodyCssClass();?>">
    <?php $this->beginBody(); ?>


    <!--[if lt IE 9]>
    <div class="old_browsers">
        <a href="//windows.microsoft.com/en-us/internet-explorer/download-ie">
            <i class="fa fa-warning"></i>
            You are using an outdated version of Internet Explorer.
            Upgrade today for a faster, safer browsing experience.
        </a>
    </div>
    <![endif]-->



    <div id="wrapper1">
        <div id="wrapper2">

            <!-- HEADER -->
            <header id="header">
                <div class="container">
                    <!-- LOGO -->
                    
                    <div class="logo_main logo_header">
                        <a href="/">
                            <?=Html::img($asset->baseUrl . '/img/logo.jpg');?>
                        </a>
                        <span>График работы:<br> &nbsp;<?=Setting::get('workingHours');?></span>
                    </div>
                    
                    
                    <div class="header_right">
                        <div class="header_panel">
                            <!-- USER MENU -->
                            <ul class="header_user">
                                <li class="top-phone">
                                    <a href="tel:<?=preg_replace('/[^\d+]/', '', $phone);?>" class="mgo-number-12791">
                                        <?=$phone;?>
                                    </a>
                                </li>
                                <li class="top-callback">
                                    <a href="#callback-modal" data-toggle="modal">
                                        <span class="hidden-xs">Заказать звонок</span>
                                        <span class="visible-xs glyphicon glyphicon-earphone"></span>
                                    </a>
                                </li>
                            </ul>
                            <div class="cart_currency">
                                
                                <!-- HEADER CART -->
                                <div class="header_cart">
                                    <a href="<?=Url::to('/shopcart');?>">
                                        <i class="fa fa-shopping-cart"></i>
                                        <b>Корзина:</b>
                                        <span class="cart-total-items">
                                            <span class="count">
                                                <?=count(Shopcart::goods());?>
                                            </span>
                                        </span>
                                    </a>
                                </div>
                                 
                            </div>
                        </div>
                    </div>
                </div>
            </header>


            <!-- MEGAMENU -->
            <div id="megamenu">
                <div class="container">
                    <ul class="sf-menu megamenu_desktop visible-md visible-lg">

                        <li class="megamenu_item_1">
                            <a href="/catalog">Каталог</a>
                            <ul>
                                <li>
                                    <div class="submenu submenu_1">
                                        <div class="row">

                                            <div class="col-sm-12">
                                                <?= Menu::widget([
                                                    'items' => yii\cms\modules\menu\api\Menu::items('catalog'),
                                                ]); ?>
                                            </div>


                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>

                        <?php foreach (yii\cms\modules\menu\api\Menu::items('main') as $i => $item): ?> 
                            <li class="megamenu_item_<?=$i+3;?>"> 
                                <a href="<?=$item['url']?>"><?=$item['label']?></a> 
                            </li> 
                        <?php endforeach; ?> 

                    </ul>

                    <div class="megamenu_mobile visible-xs visible-sm">
                        <h2>Каталог<i></i></h2>
                        <ul class="level_1">
                            <li>
                                <a href="/catalog">Каталог<i class="level_1_trigger"></i></a>

                                <?= Menu::widget([
                                    'items' => yii\cms\modules\menu\api\Menu::items('catalog'),
                                    'options' => [
                                        'class' => 'level_2',
                                    ]
                                ]); ?>
                            </li>

                            <?php foreach (yii\cms\modules\menu\api\Menu::items('main') as $i => $item): ?>
                                <li class="megamenu_item_<?=$i+3    ?>">
                                    <a href="<?=$item['url']?>"><?=$item['label']?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>                    
                </div>
            </div>
            

            <?php if(!isset($this->params['no_breadcrumbs'])): ?>
            <!-- BREADCRUMBS -->
            <div class="breadcrumb_wrap">
                <div class="container">
                    <?=Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]);?>
                </div>
            </div>
            <?php endif; ?>

            <!-- FLASH CALLBACK MESSAGE -->
            <?php if (Yii::$app->session->hasFlash('callback')) : ?>
                <div class="container">
                    <div class="alert alert-success" role="alert"><?=Yii::$app->session->getFlash('callback')?></div>
                </div>
            <?php endif; ?>

            <!-- MAIN CONTENT -->
            <?=$content; ?>

            <!-- FOOTER -->
            <footer>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-6 col-lg-3 col-md-3 footer_block__1 wow margin-b-20">
                            <?=Menu::widget([
                                'items' => yii\cms\modules\menu\api\Menu::items('footer'),
                                'options' => [
                                    'class' => 'footer_links',
                                ]
                            ]); ?>
                        </div>
                        
                        <div class="col-xs-6 col-lg-3 col-lg-push-6 col-md-3 col-md-push-6 footer_block__4 wow margin-b-20">
                            <ul class="footer_links right">
                                <li><?=str_replace(';', '<br>', $address);?></li>
                                <li>тел.: <span class="mgo-number-12791"><?=$phone;?></span></li>
                                <li>e-mail: <?=$email;?></li>
                                <li class="margin-t-10">
                                    <?=Html::a('', Setting::get('social_vk'), [
                                        'class' => 'fa fa-vk',
                                        'target' => '_blank',
                                    ]);?>
                                    <?=Html::a('', Setting::get('social_facebook'), [
                                        'class' => 'fa fa-facebook',
                                        'target' => '_blank',
                                    ]);?>
                                    <?=Html::a('', Setting::get('social_instagram'), [
                                        'class' => 'fa fa-instagram',
                                        'target' => '_blank',
                                    ]);?>
                                </li>
                            </ul>
                        </div>
                        
                        <div class="col-xs-12 col-lg-6 col-lg-pull-3 col-md-6 col-md-pull-3 margin-t-20">
                            <?=Text::get('footer-text-1');?>
                        </div>

                        
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-t-20">
                            <?=Text::get('footer-text-2');?>
                        </div>
                    </div>
                </div>
            </footer>

        </div><!-- / #wrapper2 -->
    </div><!-- / #wrapper1 -->

    <a id="back_top" title="Наверх" href="#"><i class="fa fa-angle-up"></i></a>

    <?=$this->render('_mango-new'); ?>
    <?php if(!YII_DEBUG): ?>
        <?=$this->render('_jivosite'); ?>
    <?php endif; ?>
    <?=$this->render('_callback-modal'); ?>
    <?php $this->endBody(); ?>
    </body>
</html>
<?php $this->endPage(); ?>