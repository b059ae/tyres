<?php

use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\widgets\MaskedInput;
use yii\bootstrap\Html;
use app\models\CallbackForm;

$model = new CallbackForm();

Modal::begin([
    'header' => '<h2 class="text-center margin-0">Заказать звонок</h2>',
    'id' => 'callback-modal'
]);
$form = ActiveForm::begin([
    'id' => 'callback-form',
    'action' => ['/callback'],
]);
?>
    <?=$form->field($model, 'phone')
        ->textInput()
        ->widget(MaskedInput::className(), ['mask' => '+7 (999) 999-99-99']);?>

    <div class="form-group text-center">
        <?=Html::submitButton('Отправить', [
            'class' => 'btn btn-primary btn-lg'
        ]);?>
    </div>
<?php
ActiveForm::end();
Modal::end();
?>