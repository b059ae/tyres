<?php

use yii\db\Migration;

/**
 * Handles the creation of table `payment`.
 */
class m180204_235613_create_payment_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('payment', [
            'id' => $this->primaryKey(),
            'userId' => $this->integer(11) . ' COMMENT "ID пользователя"',
            'merchantOrderId' => $this->integer(11)->notNull() . ' COMMENT "ID заказа в системе магазина"',
            'gatewayOrderId' => $this->string(255) . ' COMMENT "ID заказа в системе шлюза"',
            'dateCreate' => $this->dateTime()->notNull() . ' COMMENT "Дата создания"',
            'paymentStatus' => $this->integer() . ' COMMENT "Статус оплаты в системе шлюза"',
            'paymentErrorCode' => $this->integer() . ' COMMENT "Код ошибки в системе шлюза"',
            'paymentErrorMessage' => $this->string(512) . ' COMMENT "Описание ошибки в системе шлюза"',
            'amount' => $this->integer() . ' COMMENT "Сумма"',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('payment');
    }
}
