<?php

use yii\db\Migration;

/**
 * Class m180205_173606_add_column_payment_table
 */
class m180205_173606_add_column_payment_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('payment', 'formUrl', $this->string(2000)->after('amount'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180205_173606_add_column_payment_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180205_173606_add_column_payment_table cannot be reverted.\n";

        return false;
    }
    */
}
