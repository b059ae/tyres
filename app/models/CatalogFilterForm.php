<?php
namespace app\models;

use Yii;

class CatalogFilterForm extends \yii\base\Model
{
    public $type;
    public $brand;
    public $diameter;
    public $width;
    public $height;
    public $axle;
    
    public $price_min;
    public $price_max;
    
    public $fixture;
    public $pcd;
    public $dia;
    public $et;
    
    public $restored;
    public $used;
    public $new;
    

    public function rules()
    {
        return [
            [['type','brand', 'diameter', 'width', 'height', 'axle'], 'string'],
            [['fixture', 'pcd', 'dia', 'et'], 'string'],
            [['price_min', 'price_max'], 'number'],
            [['restored', 'used', 'new'], 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'type' => 'Тип',
            'brand' => 'Производитель',
            'diameter' => 'Диаметр',
            'width' => 'Ширина',
            'height' => 'Высота',
            'axle' => 'Ось',
            'price_min' => 'Минимальная цена',
            'price_max' => 'Максимальная цена',
            
            'fixture' => 'Кол-во крепежных отверстий',
            'pcd' => 'PCD',
            'dia' => 'DIA',
            'et' => 'Вылет(ET)',
            'restored' => 'Восстановленные',
            'used' => 'Б/У',
            'new' => 'Новые',
        ];
    }
    
    public function formName(){
        return '';
    }
    
    public function parse()
    {
        $filters = [];

        if ($this->type) {
            $filters['type'] = $this->type;
        }
        if ($this->brand) {
            $filters['brand'] = $this->brand;
        }
        if ($this->diameter) {
            $filters['diameter'] = $this->diameter;
        }
        if ($this->width) {
            $filters['width'] = $this->width;
        }
        if ($this->height) {
            $filters['height'] = $this->height;
        }
        if ($this->axle) {
            $filters['axle'] = $this->axle;
        }
        
        if ($this->fixture) {
            $filters['fixture'] = $this->fixture;
        }
        if ($this->pcd) {
            $filters['pcd'] = $this->pcd;
        }
        if ($this->dia) {
            $filters['dia'] = $this->dia;
        }
        if ($this->et) {
            $filters['et'] = $this->et;
        }
        
        return $filters;
    }
    
    public function additionalFilters(){
        $filters = [];
        if($this->restored){
            $filters['restored'] = $this->restored;
        }
        if($this->used){
            $filters['used'] = $this->used;
        }
        return $filters;
    }
    
    public function notFilters(){
        $filters = [];
        if($this->new){
            $filters['restored'] = '1';
            $filters['used'] = '1';
        }
        return $filters;
    }
    
    
    
    public function getItemIds($cat_id = null, $cat_id_to_exclude = null){
        $filters = $this->parse();
        $where_sql = [];
        $left_join_sql = [];
        foreach($filters as $key => $value){
            $values = $this->getVariants($value);
            $sql_values = $this->valuesToSql($values);
            $left_join_sql[] = $this->leftJoinIn($key, $values);
            $where_sql[] = "`".$key."`.`value` IN (".$sql_values.")";
        }
        
        $not_filters = $this->notFilters();
        $additional_filters = $this->additionalFilters();
        $a_where_sql = [];
        $a_left_join_sql = [];
        $not_where_sql = [];
        $not_left_join_sql = [];
        if(count($not_filters)){
            foreach($not_filters as $key => $value){
                $values = $this->getVariants($value);
                $sql_values = $this->valuesToSql($values);
                $not_left_join_sql[] = $this->leftJoin($key);
                $not_where_sql[] = "`".$key."`.`value` NOT IN (".$sql_values.") OR "."`".$key."`.`value` IS NULL";
            }
        }
        else{
            foreach($additional_filters as $key => $value){
                $values = $this->getVariants($value);
                $sql_values = $this->valuesToSql($values);
                $a_left_join_sql[] = $this->leftJoinIn($key, $values);
                $a_where_sql[] = "`".$key."`.`value` IN (".$sql_values.")";
            }
        }
        
        
        if($this->price_min >= 10 && $this->price_max >= 20){
            $where_sql[] = "`cms_catalog_items`.`price` BETWEEN '".$this->price_min."' AND '".$this->price_max."'";
        }
        
        $where_sql = implode(' AND ', $where_sql);
        $left_join_sql = implode(' ', $left_join_sql);
        
        $a_where_sql = implode(' OR ', $a_where_sql);
        $a_left_join_sql = implode(' ', $a_left_join_sql);
        $not_where_sql = implode(' AND ', $not_where_sql);
        $not_left_join_sql = implode(' ', $not_left_join_sql);
        
        $sql = "SELECT `cms_catalog_items`.`id`
            FROM `cms_catalog_items`"
            .$left_join_sql
            .$a_left_join_sql
            .$not_left_join_sql;
        
        if($cat_id !== null){
            $sql .= " WHERE `cms_catalog_items`.`category_id` = '" . $cat_id . "'";
        }
        elseif($cat_id_to_exclude !== null){
            $sql .= " WHERE `cms_catalog_items`.`category_id` != '" . $cat_id_to_exclude. "'";
        }
        
        $sql .= !empty($where_sql) ? " AND " . $where_sql : '';
        $sql .= !empty($a_where_sql) ? " AND " . $a_where_sql : '';
        $sql .= !empty($not_where_sql) ? " AND ({$not_where_sql})" : '';
        $cmd = Yii::$app->db->createCommand($sql);
        $items = $cmd->queryAll();
        $ids = [];
        foreach($items as $item){
            if(empty($item['id'])){
                continue;
            }
            $ids[] = $item['id'];
        }
        return count($ids) ? $ids : [0];
    }
    
    
    private function leftJoinIn($key, $values){
        $values = $this->valuesToSql($values);
        return " LEFT JOIN(
            SELECT `value`, `item_id`
            FROM `cms_catalog_item_data`
            WHERE `name` = '".$key."'
            AND `value` IN (".$values.")
        )
        AS `".$key."` ON `cms_catalog_items`.`id` = `".$key."`.`item_id` ";
    }
    
    private function leftJoin($key){
        return " LEFT JOIN(
            SELECT `value`, `item_id`
            FROM `cms_catalog_item_data`
            WHERE `name` = '".$key."'
        )
        AS `".$key."` ON `cms_catalog_items`.`id` = `".$key."`.`item_id` ";
    }
    
    private function getVariants($str){
        $variants = [$str];
        if(is_numeric($str)){
            if(strpos($str, '.') !== false){
                $variants[] = str_replace('.', ',', $str);
            }
            else{
                $variants[] = $str . '.00';
                $variants[] = $str . ',00';
            }
        }
        return $variants;
    }
    
    private function valuesToSql($values){
        $values = array_map(function($e){
            return "'" . $e . "'";
        }, $values);
        return implode(',', $values);
    }
}