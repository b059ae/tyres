<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payment".
 *
 * @property int $id
 * @property int $userId ID пользователя
 * @property int $merchantOrderId ID заказа в системе магазина
 * @property string $gatewayOrderId ID заказа в системе шлюза
 * @property string $dateCreate Дата создания
 * @property int $paymentStatus Статус оплаты в системе шлюза
 * @property int $paymentErrorCode Код ошибки в системе шлюза
 * @property string $paymentErrorMessage Описание ошибки в системе шлюза
 * @property int $amount Сумма
 * @property string $formUrl
 * @property string $orderBundle

 */
class Payment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['merchantOrderId', 'dateCreate'], 'required'],
            [['userId', 'merchantOrderId', 'paymentStatus', 'paymentErrorCode', 'amount'], 'integer'],
            [['dateCreate'], 'safe'],
            [['gatewayOrderId'], 'string', 'max' => 255],
            [['paymentErrorMessage'], 'string', 'max' => 512],
            [['formUrl'], 'string', 'max' => 2000],
            [['orderBundle'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'User ID',
            'merchantOrderId' => 'Merchant Order ID',
            'gatewayOrderId' => 'Gateway Order ID',
            'dateCreate' => 'Date Create',
            'paymentStatus' => 'Payment Status',
            'paymentErrorCode' => 'Payment Error Code',
            'paymentErrorMessage' => 'Payment Error Message',
            'amount' => 'Amount',
            'formUrl' => 'FormUrl',
            'orderBundle' => 'OrderBundle',
        ];
    }
    
    /**
     * Возвращает значение json-поля в виде массива или в виде строки (если указан параметр)
     * @param $attribute
     * @param $field
     * @return mixed
     */
    public function getJsonField($attribute, $field = null){
        $data = json_decode($this->$attribute, true);
        return $field === null ? $data : $data[$field];
    }
    
    /**
     * Проверяет, равен ли массив полю orderBundle
     * @param array $data
     * @return boolean
     */
    public function equalsOrderBundle($data){
        $order_bundle = $this->getJsonField('orderBundle');
        $diff = array_diff(array_map('json_encode', $data), array_map('json_encode', $order_bundle));
        $diff = array_filter(array_map('json_decode', $diff, array(true)));
        return !count($diff);
    }
    
    public static function findByFullMerchantOrderId($full_merchant_order_id){
        $pair = explode('-', $full_merchant_order_id);
        $merchant_order_id = $pair[0];
        return self::findOne(['merchantOrderId' => $merchant_order_id]);
    }
    
    public function getFullMerchantOrderId(){
        return $this->attempt > 0 ? $this->merchantOrderId . '-' . $this->attempt : $this->merchantOrderId;
    }
}
