<?php

namespace app\models;

use Yii;
use yii\cms\models\Setting;
use app\models\Model;

class ServicesPriceForm extends Model
{
    public $email;
    public $service;
    
    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
            'service' => 'Услуга',
        ];
    }

    public function rules()
    {
        return [
            ['email', 'required'],
            [['service'], 'string', 'min' => 2],
            ['email', 'email'],
        ];
    }
    
    
    public function action($article){
        if(!$this->validate()){
            return false;
        }
        
        $msg_title = 'Спасибо, что выбрали нашу компанию!';
        $msg = 'Прайс отправлен на указанный Вами e-mail.';
        $file = Yii::getAlias('@webroot') . '/uploads/' . $article->model->file;
        
        if(file_exists($file) && is_file($file)){
            $this->sendEmailTo(
                $this->email,
                $this->formTitle(),
                $this->service . ': Прайс',
                [$file]
            );
        }
        
        Yii::$app->session->setFlash('title', $msg_title);
        Yii::$app->session->setFlash('message', $msg);
        
        return true;
    }
    
    public function report(){
        return Yii::$app->view->renderFile('@app/views/email/report.php', [
            'form_title' => $this->formTitle(),
            'data' => $this->labelsAndValues(),
        ]);
    }
    
    public function formTitle(){
        return 'Прайс ('.$this->service.')';
    }
}
