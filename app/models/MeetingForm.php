<?php

namespace app\models;

use Yii;
use yii\cms\models\Setting;
use app\validators\PhoneFilterValidator;
use app\validators\PhoneMatchValidator;

class MeetingForm extends \app\models\Model
{
    public $name;
    public $phone;
    public $datetime;
    
    public function init(){
        $this->datetime = time() + 30 * 60;
    }
    
    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'phone' => 'Телефон',
            'datetime' => 'Дата и время',
        ];
    }

    public function rules()
    {
        return [
            [['phone', 'datetime'], 'required'],
            ['phone', PhoneFilterValidator::class],
            ['phone', PhoneMatchValidator::class],
            [['name', 'datetime'], 'string'],
            [['name', 'datetime'], 'trim'],
        ];
    }
    
    
    public function action(){
        if(!$this->validate()){
            return false;
        }
        $subject = 'Заявка с сайта (назначить встречу)';
        $report = $this->report();
        $lead_data = [
            'name' => $subject,
            'datetime' => $this->datetime,
        ];
        $contact_data = $this->toArray(['name', 'phone']);
        $this->sendToAmo($lead_data, $contact_data);
        $this->sendEmailToManager($subject, $report);
        
        $msg_title = 'Спасибо, что выбрали нашу компанию!';
        $msg = 'Назначен прием на указанную вами дату. '
            . 'Наш менеджер свяжется с вами в ближайшее время.';
        
        Yii::$app->session->setFlash('title', $msg_title);
        Yii::$app->session->setFlash('message', $msg);
        
        return true;
    }
    
    public function report(){
        return Yii::$app->view->renderFile('@app/views/email/meeting_report.php', [
            'data' => $this->labelsAndValues(),
        ]);
    }
    
    public function notification(){
        return Yii::$app->view->renderFile('@app/views/email/meeting_notification.php', [
            'data' => $this->labelsAndValues(),
        ]);
    }
    
    public function formData(){
        $data = $this->toArray();
        return $data;
    }
}
