<?php

namespace app\models;

use Yii;
use app\validators\PhoneFilterValidator;
use app\validators\PhoneMatchValidator;
use app\models\Model;

class FeedbackForm extends Model
{
    public $name;
    public $phone;
    
    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'phone' => 'Телефон',
        ];
    }

    public function rules()
    {
        return [
            [['phone'], 'required'],
            ['phone', PhoneFilterValidator::class],
            ['phone', PhoneMatchValidator::class],
            ['name', 'string'],
            ['name', 'trim'],
        ];
    }
    
    
    public function action(){
        if(!$this->validate()){
            return false;
        }
        $subject = $this->formTitle();
        $report = $this->report();
        $lead_data = [
            'name' => $subject
        ];
        $this->sendToAmo($lead_data, $this->toArray());
        $this->sendEmailToManager($subject, $report);
        
        $msg_title = 'Спасибо, что выбрали нашу компанию!';
        $msg = 'Заявка отправлена. '
            . 'Наш менеджер свяжется с вами в течение 30 минут.';
        
        Yii::$app->session->setFlash('title', $msg_title);
        Yii::$app->session->setFlash('message', $msg);
        
        return true;
    }
    
    public function report(){
        return Yii::$app->view->renderFile('@app/views/email/report.php', [
            'form_title' => $this->formTitle(),
            'data' => $this->labelsAndValues(),
        ]);
    }
    
    public function formData(){
        $data = $this->toArray();
        return $data;
    }
    
    public function formTitle(){
        return 'Форма обратной связи: ' . $this->name;
    }
}
