<?php

namespace app\models;

use app\validators\NameMatchValidator;
use app\validators\PhoneFilterValidator;
use app\validators\PhoneMatchValidator;
use yii\cms\modules\feedback\api\Feedback;

abstract class AbstractFeedbackForm extends \yii\base\Model
{
    public $name;
    public $phone;

    public function formName()
    {
        return 'Feedback';
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'phone' => 'Телефон',
        ];
    }

    public function rules()
    {
        return [
            [['name', 'phone'], 'required'],
            [['name'], NameMatchValidator::class],
            ['phone', PhoneFilterValidator::class],
            ['phone', PhoneMatchValidator::class],
        ];
    }

    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        // Сохранение заявки с сайта
        try {
            $model = new Feedback();
            $model->api_save([
                'name' => $this->name,
                'phone' => $this->phone,
            ]);
        } catch (\Exception $e) {
        }

        // Автоматический звонок
        try {
            /** @var Beeline $caller */
            //$caller = \Yii::$app->beeline;
            // $caller->call($this->phone);
        } catch (\Exception $e) {
        }

        return true;
    }
}
