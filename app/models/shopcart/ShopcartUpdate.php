<?php

namespace app\models\shopcart;

use Yii;
use yii\base\Model;
use yii\cms\modules\shopcart\api\Shopcart;

class ShopcartUpdate extends Model{
    
    public $count;
    
    
    public function rules(){
        return [
            [['count'], 'required'],
            ['count', 'each', 'rule' => ['integer']],
        ];
    }
    
    
    public function attributeLabels(){
        return [
            'count' => 'Кол-во',
        ];
    }
    
    
    public function formName(){
        return '';
    }
    
    
    public function action(){
        if(!$this->validate()){
            Yii::$app->session->setFlash('add_message', 'Поля заполнены не правильно!');
            return false;
        }
        Shopcart::update($this->count);
        Yii::$app->session->setFlash('add_message', 'Корзина обновлена!');
        return Yii::$app->response->redirect(['shopcart/index']);
    }
}
