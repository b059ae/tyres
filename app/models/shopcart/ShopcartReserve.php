<?php

namespace app\models\shopcart;

use Yii;
use app\models\shopcart\Shopcart;
use yii\cms\models\Setting;

class ShopcartReserve extends Shopcart{
    
    public function formTitle(){
        return 'Заявка с сайта (зарезервировать товар)';
    }
    
    public function action(){
        if(!$this->validate()){
            Yii::$app->session->setFlash('add_message', 'Поля заполнены не правильно!');
            return false;
        }
        if(!$this->canBuy()){
            return false;
        }
        $this->saveOrder();
        $subject = $this->formTitle();
        $report = $this->report(true);
        $notification = $this->notification();
        
        $lead_data = [
            'name' => $subject,
            'price' => \yii\cms\modules\shopcart\api\Shopcart::cost(),
            'fulltitle' => $this->getOrderFulltitle(),
        ];
        
        $contact_data = $this->toArray(['name', 'phone', 'email']);
        $this->sendToAmo($lead_data, $contact_data);
        $this->sendEmailToManager($subject, $report);
        $this->sendEmailTo($this->email, 'Ваш заказ', $notification);
        
        $msg_title = 'Спасибо, что выбрали нашу компанию!';
        $msg = 'Наш менеджер свяжется с вами в ближайшее время.'
            . 'Сопроводительное письмо отправлено на ваш e-mail.';
        
        Yii::$app->session->setFlash('title', $msg_title);
        Yii::$app->session->setFlash('message', $msg);
        return Yii::$app->response->redirect(['site/message']);
    }
}
