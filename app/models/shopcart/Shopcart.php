<?php

namespace app\models\shopcart;

use Yii;
use app\models\Model;
use app\validators\PhoneMatchValidator;
use app\validators\PhoneFilterValidator;
use app\helpers\CatalogHelper;
use yii\cms\modules\shopcart\models\Order;


class Shopcart extends Model{
    
    private $_order;
    
    public $count;
    public $name;
    public $phone;
    public $email;
    public $delivery = 0;
    public $city;
    public $address;
    public $courier_name;
    
    
    public function formTitle(){}
    public function action(){}
    
    
    public function rules() {
        return [
            [['name', 'phone', 'email', 'count'], 'required'],
            
            /*[
                ['city', 'address'],
                'required',
                'when' => function($model){
                    return $model->delivery == '1';
                },
                'whenClient' => "function(attr, val){
                    return $('#" . strtolower($this->formName()) . "-delivery').val() == '1';
                }"
            ],*/
            
            ['count', 'each', 'rule' => ['integer']],
            
            [['name', 'city', 'address'], 'safe'],
            [['name', 'city', 'address'], 'string', 'min' => 2],
            [['name', 'city', 'address'], 'trim'],
            
            ['phone', PhoneFilterValidator::class],
            ['phone', PhoneMatchValidator::class],
            
            ['email', 'email'],
            ['delivery', 'integer'],
            ['courier_name', 'string', 'min' => '3']
        ];
    }
    
    
    public function attributeLabels() {
        return [
            'name' => 'Ваше имя',
            'phone' => 'Контактный телефон',
            'email' => 'E-mail',
            'delivery' => 'Способ доставки',
            'city' => 'Город',
            'address' => 'Адрес',
            'courier_name' => 'ФИО получателя',
        ];
    }
    
    
    public function valueLabels(){
        return [
            'delivery' => [
                '0' => 'Самовывоз',
                '1' => 'Доставка водитель',
            ],
        ];
    }
    
    public function saveOrder($status = Order::STATUS_PENDING){
        \yii\cms\modules\shopcart\api\Shopcart::update($this->count);
        $this->order->setAttributes($this->toArray([
            'name', 'email', 'phone', 'delivery', 'city', 'address', 'courier_name'
        ]), false);
        $this->order->comment = $this->report();
        $this->order->save();
        
        $shopcart = new \yii\cms\modules\shopcart\api\Shopcart();
        $this->_order = $shopcart->order->model;
        $this->_order->status = $status;
        $this->_order->save();
    }
    
    public function report($html = false){
        $template = $html ? 'report_html' : 'report_text';
        return Yii::$app->view->renderFile('@app/views/site/'.$template.'.php', [
            'data' => $this->labelsAndValues(),
            'order' => $this->order,
        ]);
    }
    
    
    public function notification($filename = null){
        $filename = $filename ? $filename : 'notification';
        return Yii::$app->view->renderFile('@app/views/email/' . $filename . '.php', [
            'data' => $this->labelsAndValues(),
            'order' => $this->order,
        ]);
    }
    
    public function getOrder(){
        if($this->_order === null){
            $shopcart = new \yii\cms\modules\shopcart\api\Shopcart();
            $this->_order = $shopcart->order->model;
        }
        return $this->_order;
    }
    
    public function loadOrder($order){
        $this->_order = $order;
        $this->setAttributes($order->toArray());
    }
    
    public function formData(){
        return $this->getAttributes(null, ['count']);
    }
    
    public function canBuy(){
        $goods = $this->order->goods;
        $errors = [];
        foreach($goods as $key => $good){
            // Если кол-во товаров ограничено и пользователь выбрал слишком много
            if(!CatalogHelper::isFreeCountItem($good->item)
            && $this->count[$good->good_id] > $good->item->available){
                $errors[] = [
                    'fullname' => CatalogHelper::getItemTitle($good->item),
                    'available' => $good->item->available,
                ];
                
            }
        }
        if(count($errors)){
            Yii::$app->session->setFlash('add_message', $this->cannotBuyErrorMessage($errors));
            return false;
        }
        return true;
    }
    
    private function cannotBuyErrorMessage($errors){
        $message = 'Недостаточно товаров на складе. В наличии:<br/>';
        foreach($errors as $error){
            $message .= $error['fullname'] . ' - <b>' . $error['available'] . ' шт.</b><br/>';
        }
        return $message;
    }
    
    public function getOrderFulltitle(){
        $fulltitle = array_map(function($good){
            return CatalogHelper::getItemTitle($good->item);
        }, $this->order->goods);
        return implode(' | ', $fulltitle);
    }
}