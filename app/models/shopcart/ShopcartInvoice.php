<?php

namespace app\models\shopcart;

use Yii;
use app\models\shopcart\Shopcart;
use app\validators\InnValidator;
use yii\cms\models\Setting;

class ShopcartInvoice extends Shopcart{
    
    public $inn;
    public $company;
    
    
    public function rules(){
        return array_merge(parent::rules(), [
            [['inn', 'company'], 'required'],
            ['company', 'string', 'min' => '3'],
            ['inn', InnValidator::className()],
        ]);
    }
    
    
    public function attributeLabels(){
        return array_merge(parent::attributeLabels(), [
            'inn' => 'ИНН',
            'company' => 'Наименование организации',
        ]);
    }
    
    
    public function action(){
        if(!$this->validate()){
            Yii::$app->session->setFlash('add_message', 'Поля заполнены не правильно!');
            return false;
        }
        if(!$this->canBuy()){
            return false;
        }
        $this->saveOrder();
        $files = [$this->saveFile()];
        $subject = $this->formTitle();
        $report = $this->report(true);
        $notification = $this->notification();
        
        $lead_data = [
            'name' => $subject,
            'price' => \yii\cms\modules\shopcart\api\Shopcart::cost(),
            'fulltitle' => $this->getOrderFulltitle(),
        ];
        $contact_data = $this->toArray(['name', 'phone', 'inn', 'company', 'email']);
        $this->sendToAmo($lead_data, $contact_data);
        $this->sendEmailToManager($subject, $report, $files);
        
        $manager_email_2 = Setting::get('manager_email_2');
        if(filter_var($manager_email_2, FILTER_VALIDATE_EMAIL)){
            $this->sendEmailTo($manager_email_2, $subject, $report, $files);
        }
        
        $this->sendEmailTo($this->email, 'Ваш заказ', $notification, $files);
        
        $msg_title = 'Спасибо, что выбрали нашу компанию!';
        $msg = 'Наш менеджер свяжется с вами в ближайшее время.'
            . 'Счет и сопроводительное письмо отправлены на ваш e-mail.';
        
        Yii::$app->session->setFlash('title', $msg_title);
        Yii::$app->session->setFlash('message', $msg);
        return Yii::$app->response->redirect(['site/message']);
    }
    
    
    public function saveFile(){
        $site_address = Yii::$app->request->hostInfo;
        $file_to_download = Yii::getAlias('@runtime') . '/downloads/order-' . $this->order->id . '.pdf';
        Yii::$app->html2pdf
            ->render('invoice', [
                'order' => $this->order,
                'customer_company' => $this->company,
                'customer_inn' => $this->inn,
                'site_address' => $site_address,
            ])
            ->saveAs($file_to_download);
        return $file_to_download;
    }
    
    
    public function formTitle(){
        return 'Заявка с сайта (счет)';
    }
}
