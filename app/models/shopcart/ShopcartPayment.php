<?php

namespace app\models\shopcart;

use Yii;
use yii\cms\modules\shopcart\models\Order;
use app\models\shopcart\Shopcart;

class ShopcartPayment extends Shopcart{
    
    public function rules() {
        $rules = parent::rules();
        $rules[] = ['courier_name', 'required'];
        return $rules;
    }
    
    public function action(){
        if(!$this->validate()){
            Yii::$app->session->setFlash('add_message', 'Поля заполнены не правильно!');
            return false;
        }
        if(!$this->canBuy()){
            return false;
        }
        $this->saveOrder(Order::STATUS_BLANK);
        return Yii::$app->response->redirect(['/sberbank-payment']);
    }
    
    public function formTitle(){
        return 'Новый заказ в интернет-магазине ТД КОРДМАСТЕР';
    }
    
    public static function sendReport($order){
        $s = new ShopcartPayment();
        $s->loadOrder($order);
        $subject = $s->formTitle();
        $lead_data = [
            'name' => $subject,
            'price' => \yii\cms\modules\shopcart\api\Shopcart::cost(),
            'fulltitle' => $this->getOrderFulltitle(),
        ];
        $contact_data = $s->toArray(['name', 'phone', 'email']);
        $s->sendToAmo($lead_data, $contact_data);
        $s->sendEmailToManager($subject, $s->report(true));
        $s->sendEmailTo($s->email, $subject, $s->notification('payment_notification'));
        return $s;
    }
}