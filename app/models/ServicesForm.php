<?php

namespace app\models;

use Yii;
use app\validators\PhoneFilterValidator;
use app\validators\PhoneMatchValidator;
use yii\cms\models\Setting;
use app\models\Model;

class ServicesForm extends Model
{
    public $name;
    public $phone;
    public $service;
    
    public function attributeLabels()
    {
        return [
            'name' => 'Ваше имя',
            'phone' => 'Телефон',
            'service' => 'Услуга',
        ];
    }

    public function rules()
    {
        return [
            [['name', 'phone'], 'required'],
            [['name', 'service'], 'string', 'min' => 2],
            ['phone', PhoneFilterValidator::class],
            ['phone', PhoneMatchValidator::class],
        ];
    }
    
    
    public function action(){
        if(!$this->validate()){
            return false;
        }
        $subject = $this->formTitle();
        $report = $this->report();
        $lead_data = [
            'name' => $subject,
        ];
        $contact_data = $this->toArray();
        $this->sendToAmo($lead_data, $contact_data);
        $this->sendEmailToManager($subject, $report);
        
        $msg_title = 'Спасибо, что выбрали нашу компанию!';
        $msg = 'Заявка отправлена. '
            . 'Наш менеджер перезвонит Вам в течение 30 минут и сообщит необходимую информацию.';
        
        Yii::$app->session->setFlash('title', $msg_title);
        Yii::$app->session->setFlash('message', $msg);
        
        return true;
    }
    
    public function report(){
        return Yii::$app->view->renderFile('@app/views/email/report.php', [
            'form_title' => $this->formTitle(),
            'data' => $this->labelsAndValues(),
        ]);
    }
    
    public function formTitle(){
        return 'Узнать стоимость ('.$this->service.'): ' . $this->name;
    }
}
