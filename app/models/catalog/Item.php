<?php
namespace app\models\catalog;

use Yii;
use corpsepk\yml\behaviors\YmlOfferBehavior;
use corpsepk\yml\models\Offer;
use app\helpers\CatalogHelper;
use app\models\CatalogFilterForm;

class Item extends \yii\cms\modules\catalog\models\Item{
    public function behaviors(){
        return [
            'ymlOffer' => [
                'class' => YmlOfferBehavior::className(),
                'scope' => function ($model){
                    $model->andWhere(['status' => 1]);
                },
                'dataClosure' => function($model){
                    $catalog_filter = new CatalogFilterForm();
                    $labels = $catalog_filter->attributeLabels();
                    $data = json_decode($model->data, true);
                    $params = [];
                    foreach($data as $key => $value){
                        if($value === null || $value === ''){
                            continue;
                        }
                        if(array_key_exists($key, $labels)){
                            if(is_array($value)){
                                if(array_key_exists(0, $value)){
                                    $value =  $value[0];
                                }
                                else{
                                    continue;
                                }
                            }
                            elseif($key === 'used' || $key === 'restored'){
                                $value = (int) $value === 0 ? 'Нет' : 'Да';
                            }
                            $params[] = [
                                'name' => $labels[$key],
                                'value' => $value,
                            ];
                        }
                    }
                    return new Offer([
                        'id' => $model->id,
                        'url' => Yii::$app->request->hostInfo . '/catalog/' . $model->category->slug . '/' .$model->slug,
                        'available' => $model->available ? true : false,
                        'price' => $model->price,
                        'currencyId' => 'RUR',
                        'categoryId' => $model->category_id,
                        'picture' => Yii::$app->request->hostInfo . '/uploads/' . $model->image_file,
                        'name' => CatalogHelper::getItemTitle($model),
                        'vendor' => isset($model->data->brand) ? $model->data->brand : null,
                        'description' => strip_tags($model->description),
                        'param' => $params,
                    ]);
                }
            ],
        ];
    }
}

