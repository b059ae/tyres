<?php
namespace app\models\catalog;

use corpsepk\yml\behaviors\YmlCategoryBehavior;

class Category extends \yii\cms\modules\catalog\models\Category{
    public function behaviors(){
        return [
            'ymlCategory' => [
                'class' => YmlCategoryBehavior::className(),
                'scope' => function ($model) {
                    $model->select(['id', 'title']);
                },
                'dataClosure' => function ($model) {
                    return [
                        'id' => $model->id,
                        'name' => $model->title,
                    ];
                }
            ],
        ];
    }
}

