<?php

namespace app\models;

use Yii;
use yii\cms\models\Setting;

class Model extends \yii\base\Model{
    private function dataToFormat($key, $value){
        if($key == 'phone' || $key == 'email'){
            return [
                [$value, 'WORK']
            ];
        }
        return $value;
    }

    public function sendToAmo($lead_data, $contact_data, $ru_id = '1'){
        $resp_usr_id = Setting::get('amocrm_ru_id_' . $ru_id);
        if((int) $resp_usr_id === 0){
            return false;
        }
        
        $amo = Yii::$app->amocrm->getClient();
        $cf_keys = array_keys($amo->fields['custom_fields']);

        $lead = $amo->lead;
        $lead['name'] = $lead_data['name'];
        $lead['price'] = $lead_data['price'];
        $lead['responsible_user_id'] = $resp_usr_id;
        $lead['pipeline_id'] = $amo->fields['pipeline_id'];
        foreach($lead_data as $key => $value){
            if(!in_array($key, $cf_keys)){
                continue;
            }
            $lead->addCustomField(
                $amo->fields['custom_fields'][$key],
                $this->dataToFormat($key, $value)
            );
        }
        $lead_id = $lead->apiAdd();

        $contact = $amo->contact;
        $contact['name'] = $contact_data['name'];
        $contact['responsible_user_id'] = $resp_usr_id;
        foreach($contact_data as $key => $value){
            if(!in_array($key, $cf_keys)){
                continue;
            }
            $contact->addCustomField(
                $amo->fields['custom_fields'][$key],
                $this->dataToFormat($key, $value)
            );
        }
        $contact_id = $contact->apiAdd();

        $contact = $amo->contact;
        $contact->setLinkedLeadsId([$lead_id]);
        return $contact->apiUpdate((int)$contact_id, 'now');
    }


    public function valueLabels(){
        return [];
    }

    public function getValueLabels($attribute, $value = null){
        $labels = $this->valueLabels();
        return $value !== null ? $labels[$attribute][$value] : $labels[$attribute];
    }

    public function anotherLabels(){
        return [];
    }

    public function labelsAndValues($pair_glue = null, $glue = null){
        $labels = array_merge($this->attributeLabels(), $this->anotherLabels());
        $form_data = $this->formData();
        $value_labels = $this->valueLabels();
        $result = [];
        $pair_glue_type = gettype($pair_glue);

        foreach($form_data as $key => $value){
            if(!empty($value_labels[$key])){
                if(is_array($value)){
                    $v = [];
                    foreach($value as $subvalue){
                        $v[] = $value_labels[$key][$subvalue];
                    }
                    $v = implode(', ', $v);
                }
                else{
                    $v = isset($value_labels[$key][$value]) ? $value_labels[$key][$value] : $value;
                }
            }
            else{
                $v = $value;
            }
            $new_key = isset($labels[$key]) ? $labels[$key] : $key;
            if($pair_glue_type === 'string'){
                $result[] = $new_key . $pair_glue . $v;
            }
            elseif($pair_glue_type === 'object'){
                $result[] = $pair_glue($new_key, $v);
            }
            else{
                $result[$new_key] = $v;
            }
        }

        if($glue !== null){
            $result = implode($glue, $result);
        }
        return array_filter($result);
    }

    public function formData(){
        return $this->toArray();
    }

    public function sendEmailTo($to_email, $subject, $message, $files = []){
        $from_email = Setting::get('robot_email');
        $from_email_password = Setting::get('robot_email_password');
        Yii::$app->mailer->transport->setUsername($from_email);
        Yii::$app->mailer->transport->setPassword($from_email_password);
        $m = Yii::$app->mailer->compose();
        $m->setFrom([$from_email => Yii::$app->name])
            ->setTo($to_email)
            ->setSubject($subject)
            ->setHtmlBody($message);
        
        foreach($files as $file){
            $m->attach($file);
        }
        return $m->send();
    }
    
    public function sendEmailToManager($subject, $message, $files = []){
        $manager_email = Setting::get('manager_email');
        $emails = [Setting::get('admin_email')];
        if(filter_var($manager_email, FILTER_VALIDATE_EMAIL)){
            $emails[] = $manager_email;
        }
        return $this->sendEmailTo(
            $emails,
            $subject,
            $message,
            $files
        );
    }
}
