<?php
/* @var $order yii\cms\modules\shopcart\api\OrderObject */
/* @var $customer_company string */
/* @var $customer_inn string */
/* @var $site_address string */

use app\helpers\CatalogHelper;
use yii\cms\models\Setting;

$inn = Setting::findOne(['name' => 'inn'])->value;
$kpp = Setting::findOne(['name' => 'kpp'])->value;
$company = Setting::findOne(['name' => 'company'])->value;
$bank = Setting::findOne(['name' => 'bank'])->value;
$bik = Setting::findOne(['name' => 'bik'])->value;
$ogrn = Setting::findOne(['name' => 'ogrn'])->value;
$okpo = Setting::findOne(['name' => 'okpo'])->value;
$rschet = Setting::findOne(['name' => 'rschet'])->value;
$kschet = Setting::findOne(['name' => 'kschet'])->value;
$ur_address = Setting::findOne(['name' => 'ur_address'])->value;
$phone = Setting::findOne(['name' => 'phone'])->value;
$director = Setting::findOne(['name' => 'director'])->value;

/* @var $nds string НДС 18%. Расчет: X * 18 / 118, где X сумма к оплате */
$nds = round($order->cost * 18 / 118);

?>
<table class="bordered">
    <tr>
        <td colspan="2" class="w-60-p h-75 va-t">
            Банк получателя:<br><strong><?=$bank;?></strong>
        </td>
        <td class="va-t">
            БИК: <strong><?=$bik;?></strong>
            <br>
            Сч. №: <strong><?=$kschet;?></strong>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="w-60-p">
            Получатель:<br><strong><?=$company;?></strong>
        </td>
        <td rowspan="3" class="va-t">
            Сч. № <strong><?=$rschet;?></strong>
        </td>
    </tr>
    <tr>
        <td>
            ИНН: <strong><?=$inn;?></strong>
        </td>
        <td>
            КПП: <strong><?=$kpp;?></strong>
        </td>
    </tr>
    <tr>
        <td>
            ОГРН: <strong><?=$ogrn;?></strong>
        </td>
        <td>
            ОКПО: <strong><?=$okpo;?></strong>
        </td>
    </tr>
    
</table>
<h2>Счет на оплату №И-<?=$order->id;?> от <?=date('d.m.Y');?></h2>
<hr>
<p>Поставщик<br>(Исполнитель): <strong><?=$company;?>, <?=$ur_address;?>, тел.: <?=$phone;?></strong></p>
<p class="mb-30">
    Покупатель<br>(Заказчик): <strong>компания: <?=$customer_company;?>, ИНН: <?=$customer_inn;?></strong>
</p>
<table class="bordered">
    <tr>
        <th>№</th>
        <th>Товар</th>
        <th>Кол-во</th>
        <th>Ед.</th>
        <th>Цена</th>
        <th>Сумма</th>
    </tr>
    
    <?php $number = 0; ?>
    <?php foreach($order->goods as $good): ?>
    <?php $number++; ?>
    <tr>
        <td><?=$number;?></td>
        <td><?=CatalogHelper::getItemTitle($good->item);?></td>
        <td><?=$good->count;?></td>
        <td>шт.</td>
        <td><?=number_format($good->item->price, 2, '.', '');?></td>
        <td><?=number_format($good->price * $good->count, 2, '.', '');?></td>
    </tr>
    <?php endforeach; ?>
</table>
<div class="total">
    <div class="left text-right w-200">
        <p><strong>Итого:</strong></p>
        <p><strong>В том числе НДС:</strong></p>
        <p><strong>Всего к оплате:</strong></p>
    </div>
    <div class="right text-right w-200">
        <p><?=number_format($order->cost, 2, '.', '');?> руб.</p>
        <p><?=number_format($nds, 2, '.', '');?> руб.</p>
        <p><?=number_format($order->cost, 2, '.', '');?> руб.</p>
    </div>
    <div class="clear"></div>
</div>
<div class="clear"></div>
<div class="left w-200">
    <p><strong>Всего наименований:</strong></p>
</div>
<div class="left w-200">
    <p><?=count($order->goods);?></p>
</div>
<div class="clear"></div>
<div class="warning">
    <p>Внимание!</p>
    <p>Оплата данного счета означает согласие с условиями поставки товара.</p>
    <p>Уведомление об оплате обязательно, в противном случае не гарантируется наличие товара на складе.</p>
    <p>Товар отпускается по факту прихода денег на р/с Поставщика, самовывозом, при наличии доверенности и паспорта.</p>
</div>
<hr>
<div class="left">
    <table>
        <tr>
            <td>
                <strong>Генеральный директор:</strong> <?=$director;?>
            </td>
            <td>
                <img src="invoice-img/signature.png">
            </td>
        </tr>
        <tr>
            <td><strong>Бухгалтер:</strong> <?=$director;?></td>
            <td><img src="invoice-img/signature.png"></td>
        </tr>
    </table>
    
</div>
<div class="right">
    <img src="invoice-img/stamp.png">
</div>