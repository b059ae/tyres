<?php
namespace app\helpers;

use Yii;
use yii\cms\modules\catalog\api\Catalog;
use yii\cms\modules\catalog\api\CategoryObject;
use yii\cms\modules\catalog\api\ItemObject;
use yii\cms\modules\catalog\models\Category;

class CatalogHelper
{

    public static function getItemTitle($item, $middle_separator = '')
    {
        //ЕСЛИ ДИСКИ
        $cat_slug = Category::findOne($item->category_id)->slug;
        if($cat_slug == 'wheels'){
            $item_class = get_class($item);
            if($item_class == 'yii\cms\modules\catalog\api\ItemObject'){
                $brand = isset($item->brand) ? $item->brand : '';
                $width = isset($item->width) ? $item->width : '';
                $diameter = isset($item->diameter) ? $item->diameter : '';
                $fixture = isset($item->fixture) ? $item->fixture : '';
                $pcd = isset($item->pcd) ? $item->pcd : '';
                $dia = isset($item->dia) ? $item->dia : '';
                $et = isset($item->et) ? $item->et : '';
            }
            elseif(
                $item_class == 'yii\cms\modules\catalog\models\Item'
                || $item_class == 'app\models\catalog\Item'
            ){
                $data = $item->data;
                if(is_string($data)){
                    $data = json_decode($data);
                }
                $brand = isset($data->brand) ? $data->brand : '';
                $width = isset($data->width) ? $data->width : '';
                $diameter = isset($data->diameter) ? $data->diameter : '';
                $fixture = isset($data->fixture) ? $data->fixture : '';
                $pcd = isset($data->pcd) ? $data->pcd : '';
                $dia = isset($data->dia) ? $data->dia : '';
                $et = isset($data->et) ? $data->et : '';
            }
            $param_1 = implode('x', [$width, $diameter]);
            $param_2 = implode('/', [$param_1, $fixture . 'x' . $pcd, $dia, $et]);
            $middle_separator = ($middle_separator == '') ? ' ' : $middle_separator;
            
            return $brand . $middle_separator . $param_2;
        }
        
        //ЕСЛИ ШИНЫ
        $item_class = get_class($item);
        if($item_class == 'yii\cms\modules\catalog\api\ItemObject'){
            $width = isset($item->width) ? $item->width : '';
            $height = isset($item->height) ? $item->height : '';
            $diameter = isset($item->diameter) ? $item->diameter : '';
            $brand = isset($item->brand) ? $item->brand : '';
            $title = $item->getTitle();
        }
        elseif(
            $item_class == 'yii\cms\modules\catalog\models\Item'
            || $item_class == 'app\models\catalog\Item'
        ){
            $data = $item->data;
            if(is_string($data)){
                $data = json_decode($data);
            }
            $width = isset($data->width) ? $data->width : '';
            $height = isset($data->height) ? $data->height : '';
            $diameter = isset($data->diameter) ? $data->diameter : '';
            $brand = isset($data->brand) ? $data->brand : '';
            $title = $item->title;
        }
        
        $params = [];
        if(!empty($width)){
            $params[] = $width;
        }
        if(!empty($height)){
            $params[] = $height;
        }
        $three_params = implode('/', $params);
        $three_params .= $diameter ? 'R' . $diameter : '';
        $three_params .= $middle_separator;
        
        return implode(' ', [
            $three_params,
            $brand,
            $title,
        ]);
    }
    
    public static function isFreeCountItem($item){
        return (int) $item->available === 0;
    }
    
    public static function getItemThumb(ItemObject $item, \app\assets\AppAsset $asset, $w = 480, $h = 480){
        $image_path = Yii::getAlias('@webroot') . $item->image;
        //echo file_exists($image_path);exit();
        if(!file_exists($image_path) || is_dir($image_path)){
            //$item->image = '/img/dummy.jpg';
            return $asset->baseUrl . '/img/dummy.jpg';
        }
        return $item->thumb($w, $h);
    }
    
    
    
    public static function badge(ItemObject $item){ 
        if(!empty($item->used)){
            if($item->used){
                return '<span class="product_badge sale">Б/У</span>';
            }
        }
        if(!empty($item->restored)){
            if($item->restored){
                return '<span class="product_badge new">Восстановленные</span>';
            }
        }
        return '';
    }
    
    
    public static function statusBadge($order){
        if($order->model->status == 4 || $order->model->status == 7){
            $status = 'good';
        }
        elseif($order->model->status == 3 || $order->model->status == 6){
            $status = 'bad';
        }
        else{
            $status = 'neutral';
        }
        return '<span class="p_badge '.$status.'-status">'. $order->status . '</span>';
    }
    
    
    public static function getFieldVariants($name, $category_id = null, $not = ''){
        $DB = Yii::$app->getDb();
        $sql = "SELECT DISTINCT REPLACE(REPLACE(`value`, ',', '.'), '.00', '') AS `value`
            FROM `cms_catalog_item_data`
            LEFT JOIN `cms_catalog_items`
            ON (`cms_catalog_items`.`id` = `cms_catalog_item_data`.`item_id`)
            WHERE `cms_catalog_item_data`.`name` = '".$name."'";
        if($category_id !== null){
            $sql .= " AND `cms_catalog_items`.`category_id` {$not}= '".$category_id."'";
        }
        $sql .= " ORDER BY CAST(`value` AS SIGNED) ASC";
        $cmd = $DB->createCommand($sql);
        $items = $cmd->queryAll();
        $values = [];
        foreach($items as $item){
            if(empty($item['value'])){
                continue;
            }
            $values[] = $item['value'];
        }
        return $values;
    }
    
    
    public static function searchItemIDs($search_query){
        if(strlen(trim($search_query)) < 3) return [];
        $search_words = preg_split('/[\s]+/', $search_query);
        $where_search_query = [];
        foreach($search_words as $word){
            $where_search_query[] = " `title` LIKE '%".$word."%'
            OR `description` LIKE '%".$word."%'
            OR (`cms_catalog_item_data`.`name` = 'brand'
            AND `cms_catalog_item_data`.`value` LIKE '%".$word."%') ";
        }
        $where_search_query = implode('OR', $where_search_query);
        
        $DB = Yii::$app->getDb();
        $cmd = $DB->createCommand(
            "SELECT `cms_catalog_items`.`id`
            FROM `cms_catalog_items`
            LEFT JOIN `cms_catalog_item_data`
            ON (`cms_catalog_item_data`.`item_id` = `cms_catalog_items`.`id`)
            WHERE " . $where_search_query
        );
        $items = $cmd->queryAll();
        $ids = [];
        foreach($items as $item){
            if(empty($item['id'])){
                continue;
            }
            $ids[] = $item['id'];
        }
        return $ids;
    }
    
    
    public static function getBodyCssClass(){
        $controller = Yii::$app->controller;
        $action =  $controller->id . '/' . $controller->action->id;
        switch($action){
            case 'site/index' : $css_class = 'template-index';
                break;
            case 'shopcart/index' : $css_class = 'template-cart';
                break;
            case 'catalog/cat' : $css_class = 'template-category';
                break;
            case 'catalog/view' : $css_class = 'template-view';
                break;
            default : $css_class = '';
                break;
        }
        return $css_class;
    }

    /**
     * Get catalog categories as flat array, where key is slug
     *
     * @return CategoryObject[]
     */
    public static function cats(){
        /** @var CategoryObject[] $cats */
        $cats = Catalog::cats();
        $result = [];
        foreach ($cats as $cat){
            $result[$cat->slug] = $cat;
        }

        return $result;
    }
    
    
    public static function getRecommended($item){
        if(empty($item->data->recommended)){
            return [];
        }
        $ids = $item->data->recommended;
        return Catalog::items([
            'where' => ['in', 'id', $ids]
        ]);
    }


    public static function getFieldVariantsInFormat($name, $id = null, $not = '', $sort = false){
        $keys = self::getFieldVariants($name, $id, $not);
        $values = array_map(function($value){
            return str_replace('.', ',', $value);
        }, $keys);
        if($sort){
            sort($values);
        }
        return ['' => 'Все'] + array_combine($keys, $values);
    }
}