<?php

namespace app\helpers;

use app\models\CallbackForm;
use app\models\FeedbackForm;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

class Html extends \yii\helpers\Html
{
    /**
     * Возвращает текст первого параграфа или первой строки до переноса
     * @param string $text
     * @return string|null
     */
    public static function firstParagraph($text)
    {
        preg_match('/<p>(.*?)<\/p>/si', $text, $paragraphs);
        if (count($paragraphs) > 0) {
            return strip_tags($paragraphs[0]); // Paragraph 1
        } else {
            list($paragraph) = explode("\n", trim($text));
            if (is_array($paragraph) && count($paragraph) > 0) {
                return strip_tags($paragraph[0]); // First row
            }
        }

        return null;
    }
}