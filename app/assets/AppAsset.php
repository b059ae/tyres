<?php
namespace app\assets;

class AppAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@app/media';

    public $css = [
        'css/font-awesome.css',
        'css/assets.css',
        'css/style.css',
        'css/responsive.css',
        'bxslider/jquery.bxslider.min.css',
        'nouislider/nouislider.min.css',
        'select2/select2.min.css',
        'css/custom.css?d=26_02_2018',
    ];
    public $js = [
        'js/jquery.migrate.min.js',
        'js/jquery.api.min.js',
        'js/device.min.js',
        'js/jquery.easing.min.js',
        'js/jquery.superfish.min.js',
        'js/sf-touchscreen.min.js',
        'js/wow.min.js',
        'js/stick-up.min.js',
        'bxslider/jquery.bxslider.min.js',
        'js/shop.js',
        'js/jquery.elevatezoom.js',
        'js/jquery.parallax.min.js',
        'nouislider/nouislider.min.js',
        'select2/select2.min.js',
        
        'js/script.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
