jQuery(function($){
    
    //КРАСИВЫЙ ЕЛЕМЕНТ SELECT (ФИЛЬТР НА ГЛАВНОЙ)
    $('.main-filter-select').select2({
        minimumResultsForSearch: Infinity,
        width: '111px'
    });
    
});


function getUrlParams(){
    var search = document.location.search.replace('?','');
    if(search.length == 0) return {};
    var pairs = unescape(search).split('&');
    var pairs_len = pairs.length;
    var data = {};
    var pair;
    for(var i = 0; i < pairs_len; i++){
        pair = pairs[i].split('=');
        if(pair[1].length == 0) continue;
        data[pair[0]] = pair[1];
    }
    return data;
}


//AJAX-ФОРМА ДЛЯ ДОБАВЛЕНИЯ В КОРЗИНУ
function addToCart(f){
    var form = $(f);
    $.post(
         form.attr('action'),
         form.serialize(),
         function(response){
             $('.cart-total-items .count').html(response.goods_count);
             var top_param = window.scrollY >= 111 ? '80px' : '';
             $('<div class="cart_popup">'+response.add_message+'</div>')
                 .appendTo('body')
                 .css('top', top_param)
                 .slideDown(500)
                 .delay(2000)
                 .fadeOut(500, function(){$(this).remove();});
         },
         'json'
     ).fail(function(){
         
     });
     return false;
 };
 
 
 
 function redirect(url, target){
     if(target.tagName == 'DIV'){
        document.location = url;
    }
 }
 
 
 jQuery.fn.synchWith = function(form_selector, prefix){
    return jQuery(this).each(function(){
        var field = jQuery(this);

        var generateFullFieldName = function(str, prefix){
            if(prefix === undefined){
                return str;
            }
            if(typeof prefix !== 'string'){
                return str;
            }
            var f_name = str.replace(/\[\w+\]/g, ''),
                f_subnames = f_name.length === str.length ? '' : str.substring(f_name.length);
            return prefix + '[' + f_name + ']' + f_subnames;
        };

        var field_name = field.attr('name'),
            full_field_name = generateFullFieldName(field_name, prefix),
            field_value = field.val(),
            hidden_copy_selector = 'input[name="' + full_field_name + '"]';


        jQuery(form_selector).each(function(){
            var form = jQuery(this),
                _prefix;

            if(prefix !== undefined){
                if(typeof prefix === 'function'){
                    _prefix = prefix(form);
                    full_field_name = generateFullFieldName(field_name, _prefix);
                    hidden_copy_selector = 'input[name="' + full_field_name + '"]';
                }
            }

            hidden_copy = form.find(hidden_copy_selector);

            if(!hidden_copy.length){
                hidden_copy = jQuery('<input name="' + full_field_name + '" value="' + field_value + '" type="hidden">');
                hidden_copy.appendTo(form);
            }

            hidden_copy.val(field_value);
        });
    });
};