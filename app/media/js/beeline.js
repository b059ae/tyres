var username = '';
var isUseMask = (username.toLowerCase ().indexOf('mpbx') == 0);

var ERROR_CLASS = 'error';
var SUCCESS_CLASS = 'success';
var PROMPT_CLASS = 'prompt';

jQuery(function($){

    $('#callButton').bind('click', call);

    $('input').bind('keypress', function () {
        if (event.which == 13) {
            call();
            return false;
        }
    });
    if (isUseMask) {
        $("input").mask("+7 (999) 999-99-99");
    }
    $("input").focus ();
});

function call () {
    var inputValue = $('input').val();
    $("input").focus ();
    
    if (isUseMask) {
        if (inputValue != null && inputValue.trim () != '') {
            var clearedValue = inputValue.replace (/[^0-9]/g, '' );

            if (clearedValue.length == 10) {
                runCall('+7' + clearedValue);
            }
            else if ((clearedValue.length == 11) && (clearedValue[0] == '7' || clearedValue [0] == '8')) {
                runCall('+7' + clearedValue.substring(1,11));
            }
            else {
                showPhoneError();
            }

        }
        else {
            showPhoneError();
        }
    }
    else {
        runCall(inputValue);
    }
}

function runCall (phoneNumber) {
    if (username != '') {

        setBlockClass(PROMPT_CLASS);

        showMessage('Пожалуйста подождите...');

        $.ajax({
            type : 'POST',
            url : 'http://vn.beeline.ru/com.broadsoft.xsi-actions/v2.0/user/' + username + '/calls/CallMeNow?address=' + encodeURIComponent(phoneNumber),
            cache: false,
            data : '',
            dataType : 'xml',
            success : function (xml) {
                var message = 'Мы скоро перезвоним Вам на номер ' + formatPhone (phoneNumber);
                var passcode = $(xml).find("passcode").eq(0).text();

                if (passcode != null && passcode.trim() != '') {
                    message += ". Введите " + passcode + " при запросе кода доступа.";
                }
                showSuccess(message);
            },
            error : function (jqXHR, textStatus, errorThrown) {
                var xml = jqXHR.responseText;	
                var message = xml;	
                var i = xml.indexOf("<summary>");
                var j = xml.indexOf("</summary>")
                if (i < j) {
                    message = xml.substring(i + 9, j);
                }

                if (message == '' || message == null) {
                    showError('Произошла ошибка при подключении, попробуйте еще раз попозже.');
                }
                else if (message == 'Вызов  запрещен правилами на платформе') {
                    showMessage('Проверьте корректность номера');
                }
                else {
                    showError(message);
                }
            }
        });
    }
    else {
        showError('Введенный номер некорректен для использования в сервисе.');
    }
}

function formatPhone (phoneNumber) {
    if (isUseMask) {
        phoneNumber = phoneNumber.substring(2);
        return '+7 ' + phoneNumber.substring (0,3) + ' ' + phoneNumber.substring(3,6) + ' ' + phoneNumber.substring(6,8) + '-' + phoneNumber.substring(8,10);
    }
    else{
        return phoneNumber;
    }
}

function showPhoneError () {
    showError('Введите правильно номер телефона и повторите еще раз.');
}

function showError (message) {
    setBlockClass(ERROR_CLASS);
    showMessage(message);
}

function showSuccess (message) {
    setBlockClass(SUCCESS_CLASS);
    $('#successMessageField').html(message);
}

function showMessage (message) {
    $('#messageField').text(message);
}

function setBlockClass (className) {
    $('#callMeNowBlock').removeClass(SUCCESS_CLASS)
                        .removeClass(ERROR_CLASS)
                        .removeClass(PROMPT_CLASS)
                        .addClass(className);
}
