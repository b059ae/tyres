<?php
namespace app\validators;

use yii\validators\FilterValidator;


class PhoneFilterValidator extends FilterValidator
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->filter = function($value){
            return preg_replace('/[^+\d]/', '', $value);
        };
        parent::init();
    }
}