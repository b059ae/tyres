-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: 194.58.123.54    Database: tyres
-- ------------------------------------------------------
-- Server version	5.6.36-82.0

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `auth_key` varchar(60) DEFAULT '',
  `access_token` varchar(60) DEFAULT '',
  `password` varchar(60) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (2,'GmxZv6YaSWs8Zx_Y8A6SS6N2USNr7V5l','','$2y$13$GVFcdYeje3rdBYbuJk0RX.wSOUfbZdMVTPBwjeMoHYCf/IcGTIExK','stariy.roker@yandex.ru','Rocker'),(4,'CjTgqvkACBK7W2aCYIx6xMhzHOsIg9NO','','$2y$13$EFLpsCCkbLGs9C25RHGmCegMgPqtH73IrkgL4UbuI8CF.EYcxma3G','iddqd.roman@gmail.com','Rocker'),(5,'qLqPXKN1H4c69K8LZ6U27GXOkDCkFzFX','','$2y$13$Y5xkca5MrcDf90CRb8sdze0Ly1neN953E9jhesmL8SvbhazjHmRme','vk@telecombg.ru','Владимир'),(6,'6zz2EaNEOTre6tA8Dwz8VNwYSqGXn_LH','','$2y$13$2u4tm.HQ7mswZl4x2afUGOO45WfI8j0yJpOfcUnuRouNUNtmLM9XG','xezn.er@hceap.info','DwayneHoalo');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_admins`
--

DROP TABLE IF EXISTS `cms_admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `password` varchar(64) NOT NULL,
  `auth_key` varchar(128) DEFAULT NULL,
  `access_token` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `access_token` (`access_token`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_admins`
--

LOCK TABLES `cms_admins` WRITE;
/*!40000 ALTER TABLE `cms_admins` DISABLE KEYS */;
INSERT INTO `cms_admins` VALUES (1,'ilya','18488f3ee3f229ec5773c44fef17e5ed8eeb7961','FOCGw-xjrp7G6klPXmiyxglqU4CX9gnM',NULL),(2,'admin','76799962e9c2d52293c53e42bf059b7266658e7e','GkRX_FCwDMp0Gc2BeachSMT1aSmbjp4Z',NULL);
/*!40000 ALTER TABLE `cms_admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_article_categories`
--

DROP TABLE IF EXISTS `cms_article_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_article_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `image_file` varchar(128) DEFAULT NULL,
  `order_num` int(11) DEFAULT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `tree` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_article_categories`
--

LOCK TABLES `cms_article_categories` WRITE;
/*!40000 ALTER TABLE `cms_article_categories` DISABLE KEYS */;
INSERT INTO `cms_article_categories` VALUES (1,'Блог','',NULL,1,'blog',1,1,2,0,1),(2,'Услуги','',NULL,2,'uslugi',2,1,2,0,1);
/*!40000 ALTER TABLE `cms_article_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_article_items`
--

DROP TABLE IF EXISTS `cms_article_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_article_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(128) NOT NULL,
  `image_file` varchar(128) DEFAULT NULL,
  `short` varchar(1024) DEFAULT NULL,
  `text` text,
  `slug` varchar(128) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `views` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_article_items`
--

LOCK TABLES `cms_article_items` WRITE;
/*!40000 ALTER TABLE `cms_article_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_article_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_carousel`
--

DROP TABLE IF EXISTS `cms_carousel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_carousel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_file` varchar(128) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` varchar(1024) DEFAULT NULL,
  `order_num` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_carousel`
--

LOCK TABLES `cms_carousel` WRITE;
/*!40000 ALTER TABLE `cms_carousel` DISABLE KEYS */;
INSERT INTO `cms_carousel` VALUES (1,'carousel/index-89493b928b.jpg','','авпавппвап','',1,1),(2,'carousel/dikhlormetan800x800-019c4f590e.jpg','','','',2,1);
/*!40000 ALTER TABLE `cms_carousel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_catalog_categories`
--

DROP TABLE IF EXISTS `cms_catalog_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_catalog_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `image_file` varchar(128) DEFAULT NULL,
  `fields` text,
  `slug` varchar(128) DEFAULT NULL,
  `tree` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `order_num` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_catalog_categories`
--

LOCK TABLES `cms_catalog_categories` WRITE;
/*!40000 ALTER TABLE `cms_catalog_categories` DISABLE KEYS */;
INSERT INTO `cms_catalog_categories` VALUES (4,'Спецшины','<p>Спецшины для погрузчиков, экскаваторов, тракторов, строительной и дорожной техники. Предназначены для работы на бездорожье, сложных и тяжёлых грунтах, на стройплощадках и в карьерах.</p>','catalog/001-8e5048bef4.jpg','[{\"name\":\"height\",\"title\":\"\\u0412\\u044b\\u0441\\u043e\\u0442\\u0430\",\"type\":\"string\",\"options\":\"\"},{\"name\":\"diameter\",\"title\":\"\\u0414\\u0438\\u0430\\u043c\\u0435\\u0442\\u0440\",\"type\":\"string\",\"options\":\"\"},{\"name\":\"width\",\"title\":\"\\u0428\\u0438\\u0440\\u0438\\u043d\\u0430\",\"type\":\"string\",\"options\":\"\"},{\"name\":\"axle\",\"title\":\"\\u041e\\u0441\\u044c \\u0443\\u0441\\u0442\\u0430\\u043d\\u043e\\u0432\\u043a\\u0438\",\"type\":\"checkbox\",\"options\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\",\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\",\"\\u041f\\u0440\\u0438\\u0446\\u0435\\u043f\\u043d\\u0430\\u044f\",\"\\u0423\\u043d\\u0438\\u0432\\u0435\\u0440\\u0441\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f\"]},{\"name\":\"type\",\"title\":\"\\u0422\\u0438\\u043f \\u043f\\u0440\\u043e\\u0442\\u0435\\u043a\\u0442\\u043e\\u0440\\u0430\",\"type\":\"checkbox\",\"options\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\",\"\\u0417\\u0438\\u043c\\u043d\\u0438\\u0439\",\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\",\"\\u041a\\u0430\\u0440\\u044c\\u0435\\u0440\\u043d\\u044b\\u0439\",\"\\u0423\\u0441\\u0438\\u043b\\u0435\\u043d\\u043d\\u044b\\u0439\",\"\\u0423\\u043d\\u0438\\u0432\\u0435\\u0440\\u0441\\u0430\\u043b\\u044c\\u043d\\u044b\\u0439\",\"\\u0411\\u0435\\u0437\\u0434\\u043e\\u0440\\u043e\\u0436\\u044c\\u0435\"]},{\"name\":\"brand\",\"title\":\"\\u0411\\u0440\\u0435\\u043d\\u0434\\u044b\",\"type\":\"select\",\"options\":[\"NOKIAN\",\"\\u041a\\u0410\\u041c\\u0410\",\"SAVA\",\"KELLY\",\"CORDIANT\",\"TYREX\",\"Kormoran\",\"\\u0410\\u043b\\u0442\\u0430\\u0439\\u0448\\u0438\\u043d\\u0430\"]},{\"name\":\"popular\",\"title\":\"\\u041f\\u043e\\u043f\\u0443\\u043b\\u044f\\u0440\\u043d\\u044b\\u0439 \\u0442\\u043e\\u0432\\u0430\\u0440\",\"type\":\"checkbox\",\"options\":[\"\\u0414\\u0430\"]},{\"name\":\"used\",\"title\":\"\\u0411\\/\\u0423\",\"type\":\"boolean\",\"options\":\"\"},{\"name\":\"1cid\",\"title\":\"ID \\u0442\\u043e\\u0432\\u0430\\u0440\\u0430 \\u0432 1C\",\"type\":\"string\",\"options\":\"\"}]','spec-tyres',4,1,2,0,1,1),(5,'Грузовые шины','<p>Огромный выбор новых и восстановленных шин для грузовиков. Магистральные, региональные, карьерные и внедорожные шины для рулевых осей, ведущих мостов и прицепов. В наличии и под заказ.<br></p>','catalog/001-f0ede02787.jpg','[{\"name\":\"height\",\"title\":\"\\u0412\\u044b\\u0441\\u043e\\u0442\\u0430\",\"type\":\"string\",\"options\":\"\"},{\"name\":\"diameter\",\"title\":\"\\u0414\\u0438\\u0430\\u043c\\u0435\\u0442\\u0440\",\"type\":\"string\",\"options\":\"\"},{\"name\":\"width\",\"title\":\"\\u0428\\u0438\\u0440\\u0438\\u043d\\u0430\",\"type\":\"string\",\"options\":\"\"},{\"name\":\"axle\",\"title\":\"\\u041e\\u0441\\u044c \\u0443\\u0441\\u0442\\u0430\\u043d\\u043e\\u0432\\u043a\\u0438\",\"type\":\"checkbox\",\"options\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\",\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\",\"\\u041f\\u0440\\u0438\\u0446\\u0435\\u043f\\u043d\\u0430\\u044f\",\"\\u0423\\u043d\\u0438\\u0432\\u0435\\u0440\\u0441\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f\"]},{\"name\":\"type\",\"title\":\"\\u0422\\u0438\\u043f \\u043f\\u0440\\u043e\\u0442\\u0435\\u043a\\u0442\\u043e\\u0440\\u0430\",\"type\":\"checkbox\",\"options\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\",\"\\u0417\\u0438\\u043c\\u043d\\u0438\\u0439\",\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\",\"\\u041a\\u0430\\u0440\\u044c\\u0435\\u0440\\u043d\\u044b\\u0439\",\"\\u0423\\u0441\\u0438\\u043b\\u0435\\u043d\\u043d\\u044b\\u0439\",\"\\u0423\\u043d\\u0438\\u0432\\u0435\\u0440\\u0441\\u0430\\u043b\\u044c\\u043d\\u044b\\u0439\",\"\\u0411\\u0435\\u0437\\u0434\\u043e\\u0440\\u043e\\u0436\\u044c\\u0435\"]},{\"name\":\"brand\",\"title\":\"\\u0411\\u0440\\u0435\\u043d\\u0434\\u044b\",\"type\":\"select\",\"options\":[\"AEOLUS\",\"AGATE\",\"BFGoodrich\",\"BRIDGESTONE\",\"Bandamatic\",\"CONTINENTAL\",\"CORDIANT\",\"DUNLOP\",\"FESITE\",\"FIRESTONE\",\"GOODYEAR\",\"HANKOOK\",\"JT RADIAL\",\"KELLY\",\"KUMHO\",\"Kormoran\",\"MATADOR\",\"MICHELIN\",\"Marangoni\",\"NEXEN\",\"NOKIAN\",\"NORDMAN\",\"O\'GREEN\",\"OMSKSHINA\",\"PIRELLI\",\"SATOYA\",\"SAVA\",\"TAITONG\",\"TYREX\",\"WESTLIKE\",\"YOKOHAMA\",\"\\u0410\\u043b\\u0442\\u0430\\u0439\\u0448\\u0438\\u043d\\u0430\",\"\\u041a\\u0410\\u041c\\u0410\"]},{\"name\":\"popular\",\"title\":\"\\u041f\\u043e\\u043f\\u0443\\u043b\\u044f\\u0440\\u043d\\u044b\\u0439 \\u0442\\u043e\\u0432\\u0430\\u0440\",\"type\":\"checkbox\",\"options\":[\"\\u0414\\u0430\"]},{\"name\":\"used\",\"title\":\"\\u0411\\/\\u0423\",\"type\":\"boolean\",\"options\":\"\"},{\"name\":\"1cid\",\"title\":\"ID \\u0442\\u043e\\u0432\\u0430\\u0440\\u0430 \\u0432 1\\u0421\",\"type\":\"string\",\"options\":\"\"}]','truck-tyres',5,1,2,0,2,1),(6,'Шины Б/У','<p>Шины для легковых автомобилей, фургонов, внедорожников от бюджетных до премиальных. <br>\r\n</p>','catalog/nu701-8a93ce61e4.jpg','[{\"name\":\"height\",\"title\":\"\\u0412\\u044b\\u0441\\u043e\\u0442\\u0430\",\"type\":\"string\",\"options\":\"\"},{\"name\":\"diameter\",\"title\":\"\\u0414\\u0438\\u0430\\u043c\\u0435\\u0442\\u0440\",\"type\":\"string\",\"options\":\"\"},{\"name\":\"width\",\"title\":\"\\u0428\\u0438\\u0440\\u0438\\u043d\\u0430\",\"type\":\"string\",\"options\":\"\"},{\"name\":\"axle\",\"title\":\"\\u041e\\u0441\\u044c \\u0443\\u0441\\u0442\\u0430\\u043d\\u043e\\u0432\\u043a\\u0438\",\"type\":\"checkbox\",\"options\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\",\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\",\"\\u041f\\u0440\\u0438\\u0446\\u0435\\u043f\\u043d\\u0430\\u044f\",\"\\u0423\\u043d\\u0438\\u0432\\u0435\\u0440\\u0441\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f\"]},{\"name\":\"type\",\"title\":\"\\u0422\\u0438\\u043f \\u043f\\u0440\\u043e\\u0442\\u0435\\u043a\\u0442\\u043e\\u0440\\u0430\",\"type\":\"checkbox\",\"options\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\",\"\\u0417\\u0438\\u043c\\u043d\\u0438\\u0439\",\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\",\"\\u041a\\u0430\\u0440\\u044c\\u0435\\u0440\\u043d\\u044b\\u0439\",\"\\u0423\\u0441\\u0438\\u043b\\u0435\\u043d\\u043d\\u044b\\u0439\",\"\\u0423\\u043d\\u0438\\u0432\\u0435\\u0440\\u0441\\u0430\\u043b\\u044c\\u043d\\u044b\\u0439\",\"\\u0411\\u0435\\u0437\\u0434\\u043e\\u0440\\u043e\\u0436\\u044c\\u0435\"]},{\"name\":\"brand\",\"title\":\"\\u0411\\u0440\\u0435\\u043d\\u0434\\u044b\",\"type\":\"select\",\"options\":[\"AEOLUS\",\"AGATE\",\"BFGoodrich\",\"BRIDGESTONE\",\"Bandamatic\",\"CONTINENTAL\",\"CORDIANT\",\"DUNLOP\",\"FESITE\",\"FIRESTONE\",\"GOODYEAR\",\"HANKOOK\",\"JT RADIAL\",\"KELLY\",\"KUMHO\",\"Kormoran\",\"MATADOR\",\"MICHELIN\",\"Marangoni\",\"NEXEN\",\"NOKIAN\",\"NORDMAN\",\"O\'GREEN\",\"OMSKSHINA\",\"PIRELLI\",\"SATOYA\",\"SAVA\",\"TAITONG\",\"TYREX\",\"WESTLIKE\",\"YOKOHAMA\",\"\\u0410\\u043b\\u0442\\u0430\\u0439\\u0448\\u0438\\u043d\\u0430\",\"\\u041a\\u0410\\u041c\\u0410\"]},{\"name\":\"popular\",\"title\":\"\\u041f\\u043e\\u043f\\u0443\\u043b\\u044f\\u0440\\u043d\\u044b\\u0439 \\u0442\\u043e\\u0432\\u0430\\u0440\",\"type\":\"checkbox\",\"options\":[\"\\u0414\\u0430\"]},{\"name\":\"used\",\"title\":\"\\u0411\\/\\u0423\",\"type\":\"boolean\",\"options\":\"\"},{\"name\":\"1cid\",\"title\":\"ID \\u0442\\u043e\\u0432\\u0430\\u0440\\u0430 \\u0432 1\\u0421\",\"type\":\"string\",\"options\":\"\"}]','auto-tyres',6,1,2,0,3,1),(7,'Колесные диски','<p>Кованые, литые и штампованные диски для всех моделей грузовиков, легковых автомобилей и прицепов. <br>\r\n</p>','catalog/001-e4da6ee0cc.jpg','[{\"name\":\"diameter\",\"title\":\"\\u0414\\u0438\\u0430\\u043c\\u0435\\u0442\\u0440\",\"type\":\"string\",\"options\":\"\"},{\"name\":\"width\",\"title\":\"\\u0428\\u0438\\u0440\\u0438\\u043d\\u0430\",\"type\":\"string\",\"options\":\"\"},{\"name\":\"fixture\",\"title\":\"\\u041a\\u043e\\u043b\\u0438\\u0447\\u0435\\u0441\\u0442\\u0432\\u043e \\u043a\\u0440\\u0435\\u043f\\u0435\\u0436\\u043d\\u044b\\u0445 \\u043e\\u0442\\u0432\\u0435\\u0440\\u0441\\u0442\\u0438\\u0439\",\"type\":\"string\",\"options\":\"\"},{\"name\":\"pcd\",\"title\":\"PCD\",\"type\":\"string\",\"options\":\"\"},{\"name\":\"dia\",\"title\":\"DIA\",\"type\":\"string\",\"options\":\"\"},{\"name\":\"et\",\"title\":\"\\u0412\\u044b\\u043b\\u0435\\u0442(\\u0415\\u0422)\",\"type\":\"string\",\"options\":\"\"},{\"name\":\"type\",\"title\":\"\\u0422\\u0438\\u043f \\u0434\\u0438\\u0441\\u043a\\u0430\",\"type\":\"checkbox\",\"options\":[\"\\u041b\\u0438\\u0442\\u043e\\u0439\",\"\\u041a\\u043e\\u0432\\u0430\\u043d\\u043d\\u044b\\u0439\",\"\\u0428\\u0442\\u0430\\u043c\\u043f\\u043e\\u0432\\u0430\\u043d\\u043d\\u044b\\u0439\"]},{\"name\":\"brand\",\"title\":\"\\u0411\\u0440\\u0435\\u043d\\u0434\\u044b\",\"type\":\"select\",\"options\":[\"ASTERRO\",\"SANT\",\"BETTER\",\"Maxion\",\"Lemmerz\"]},{\"name\":\"popular\",\"title\":\"\\u041f\\u043e\\u043f\\u0443\\u043b\\u044f\\u0440\\u043d\\u044b\\u0439 \\u0442\\u043e\\u0432\\u0430\\u0440\",\"type\":\"checkbox\",\"options\":[\"\\u0414\\u0430\"]},{\"name\":\"used\",\"title\":\"\\u0411\\/\\u0423\",\"type\":\"boolean\",\"options\":\"\"},{\"name\":\"1cid\",\"title\":\"ID \\u0442\\u043e\\u0432\\u0430\\u0440\\u0430 \\u0432 1\\u0421\",\"type\":\"string\",\"options\":\"\"}]','wheels',7,1,2,0,4,1);
/*!40000 ALTER TABLE `cms_catalog_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_catalog_item_data`
--

DROP TABLE IF EXISTS `cms_catalog_item_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_catalog_item_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `value` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `item_id_name` (`item_id`,`name`),
  KEY `value` (`value`(300))
) ENGINE=MyISAM AUTO_INCREMENT=12263 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_catalog_item_data`
--

LOCK TABLES `cms_catalog_item_data` WRITE;
/*!40000 ALTER TABLE `cms_catalog_item_data` DISABLE KEYS */;
INSERT INTO `cms_catalog_item_data` VALUES (9913,67,'width','12'),(11252,140,'pcd','335'),(10283,87,'used','0'),(10279,87,'width','385'),(10280,87,'axle','Рулевая'),(10278,87,'diameter','22.5'),(7475,50,'1cid','00000001624'),(11217,141,'used','0'),(11215,141,'type','Штампованный'),(11209,141,'diameter','22.5'),(11277,139,'used','0'),(11256,140,'brand','Maxion'),(11253,140,'dia','281'),(11254,140,'et','120'),(11255,140,'type','Штампованный'),(11249,140,'diameter','22.5'),(11250,140,'width','11.75'),(11251,140,'fixture','10'),(11278,139,'1cid',''),(11270,139,'width','11.75'),(11271,139,'fixture','10'),(11272,139,'pcd','335'),(11273,139,'dia','281'),(11274,139,'et','120'),(11275,139,'type','Штампованный'),(11269,139,'diameter','22.5'),(11687,138,'used','0'),(11686,138,'brand','SANT'),(11682,138,'pcd','335'),(11683,138,'dia','281'),(11684,138,'et',''),(11685,138,'type','Штампованный'),(11679,138,'diameter','22.5'),(10776,142,'1cid',''),(7278,63,'type','Карьерный'),(7277,63,'type','Летний'),(7273,63,'height','80'),(7294,62,'axle','Ведущая'),(7295,62,'type','Летний'),(7296,62,'brand','КАМА'),(7297,62,'used','0'),(7291,62,'height','80'),(7292,62,'diameter','22.5'),(7293,62,'width','315'),(10819,143,'type','Летний'),(7274,63,'diameter','22.5'),(7312,61,'brand','КАМА'),(7329,60,'used','0'),(7327,60,'type','Летний'),(7313,61,'used','0'),(7311,61,'type','Летний'),(7324,60,'diameter','22.5'),(7325,60,'width','315'),(7326,60,'axle','Ведущая'),(7346,59,'1cid','00000001563'),(7342,59,'axle','Рулевая'),(7343,59,'type','Летний'),(7344,59,'brand','КАМА'),(7345,59,'used','0'),(7361,58,'used','0'),(7362,58,'1cid','00000001565'),(7357,58,'width','315'),(7358,58,'axle','Рулевая'),(7359,58,'type','Летний'),(7360,58,'brand','КАМА'),(7355,58,'height','70'),(7356,58,'diameter','22.5'),(7323,60,'height','60'),(10866,144,'type','Всесезонный'),(10821,143,'used','0'),(10822,143,'1cid',''),(11492,157,'height','33х12.5'),(11493,157,'diameter','15'),(11494,157,'width',''),(11484,29,'1cid',''),(11481,29,'brand','КАМА'),(11482,29,'popular','Да'),(11483,29,'used','0'),(11478,29,'diameter','15'),(11479,29,'width','185'),(11480,29,'type','Зимний'),(11477,29,'height','65'),(11216,141,'brand','Lemmerz'),(11214,141,'et','130'),(11211,141,'fixture','10'),(11212,141,'pcd','335'),(11213,141,'dia','281'),(12190,133,'width','6.00'),(12191,133,'fixture','6'),(12192,133,'pcd','222,25'),(12189,133,'diameter','17.5'),(11402,154,'axle','Универсальная'),(10674,135,'1cid',''),(10554,136,'1cid',''),(11688,138,'1cid',''),(10815,143,'height','70'),(10816,143,'diameter','17.5'),(10769,142,'height',''),(10770,142,'diameter','20'),(10771,142,'width','11.00'),(10772,142,'axle','Ведущая'),(10773,142,'type','Карьерный'),(10774,142,'brand','NOKIAN'),(10775,142,'used','0'),(10470,132,'et','160'),(10566,137,'width','9.00'),(10567,137,'fixture','10'),(10568,137,'pcd','335'),(10569,137,'dia','281'),(10570,137,'et','175'),(10571,137,'type','Штампованный'),(11348,134,'1cid',''),(10673,135,'used','0'),(10672,135,'brand','SANT'),(10665,135,'diameter','17.5'),(10666,135,'width','6.75'),(10667,135,'fixture','10'),(10668,135,'pcd','225'),(10669,135,'dia','176'),(10670,135,'et','142'),(10671,135,'type','Штампованный'),(10553,136,'used','0'),(10545,136,'diameter','22.5'),(10546,136,'width','8.25'),(10547,136,'fixture','10'),(10548,136,'pcd','335'),(10549,136,'dia','281'),(10550,136,'et','154'),(10551,136,'type','Штампованный'),(10552,136,'brand','ASTERRO'),(11341,134,'fixture','6'),(11342,134,'pcd','205'),(11400,154,'diameter','24'),(11399,154,'height',''),(11398,33,'1cid',''),(11396,33,'brand','Алтайшина'),(11397,33,'used','0'),(11391,33,'height',''),(11392,33,'diameter','20'),(11393,33,'width','10'),(11394,33,'axle','Универсальная'),(11395,33,'type','Бездорожье'),(10182,86,'1cid','00000001570'),(11346,134,'brand','ASTERRO'),(7555,46,'1cid','00000001693'),(7548,46,'height','70'),(7523,47,'1cid','00000001692'),(7500,48,'height','75'),(7539,45,'1cid','00000001661'),(9547,94,'used','0'),(7651,41,'1cid','00000001660'),(7646,41,'width','245'),(7647,41,'axle','Рулевая'),(7599,42,'axle','Ведущая'),(7582,43,'width','275'),(11258,140,'1cid',''),(11257,140,'used','0'),(11276,139,'brand','SANT'),(11680,138,'width','11.75'),(11681,138,'fixture','10'),(10572,137,'brand','BETTER'),(10565,137,'diameter','22.5'),(11344,134,'et','115'),(11345,134,'type','Штампованный'),(7281,63,'used','0'),(7298,62,'1cid','00000001568'),(7279,63,'type','Усиленный'),(7280,63,'brand','КАМА'),(7307,61,'height','80'),(7308,61,'diameter','22.5'),(7309,61,'width','315'),(7310,61,'axle','Рулевая'),(7341,59,'width','315'),(7339,59,'height','60'),(7340,59,'diameter','22.5'),(10861,121,'1cid',''),(11118,147,'1cid',''),(10867,144,'brand','NOKIAN'),(10863,144,'diameter','22.5'),(10864,144,'width','295'),(10865,144,'axle','Ведущая'),(10862,144,'height','80'),(10820,143,'brand','NOKIAN'),(10573,137,'used','0'),(10574,137,'1cid',''),(9642,99,'diameter','22.5'),(7568,44,'type','Летний'),(7569,44,'brand','КАМА'),(7570,44,'used','0'),(7571,44,'1cid','00000001561'),(7585,43,'brand','КАМА'),(7586,43,'used','0'),(7580,43,'height','70'),(7581,43,'diameter','22.5'),(6896,37,'used','0'),(6894,37,'type','Летний'),(6895,37,'brand','КАМА'),(6890,37,'height','75'),(6891,37,'diameter','17.5'),(7699,38,'1cid','00000001558'),(7698,38,'used','0'),(7692,38,'height','75'),(7693,38,'diameter','17.5'),(7694,38,'width','235'),(6897,37,'1cid','00000001621'),(6892,37,'width','215'),(6893,37,'axle','Ведущая'),(7679,39,'axle','Ведущая'),(7680,39,'type','Летний'),(7681,39,'brand','КАМА'),(7676,39,'height','75'),(7667,40,'1cid','00000001560'),(7666,40,'used','0'),(7665,40,'brand','КАМА'),(7660,40,'height','75'),(7661,40,'diameter','17.5'),(7662,40,'width','235'),(7650,41,'used','0'),(7645,41,'diameter','19.5'),(7677,39,'diameter','17.5'),(7678,39,'width','235'),(7695,38,'axle','Рулевая'),(7696,38,'type','Летний'),(7697,38,'brand','КАМА'),(7663,40,'axle','Прицепная'),(7664,40,'type','Летний'),(7506,48,'used','0'),(7648,41,'type','Летний'),(7649,41,'brand','КАМА'),(7644,41,'height','70'),(7602,42,'used','0'),(7603,42,'1cid','00000001664'),(7596,42,'height','70'),(7597,42,'diameter','19.5'),(7598,42,'width','245'),(7587,43,'1cid','00000001665'),(7583,43,'axle','Рулевая'),(7584,43,'type','Летний'),(7565,44,'diameter','22.5'),(7566,44,'width','275'),(7567,44,'axle','Ведущая'),(7564,44,'height','70'),(7537,45,'brand','КАМА'),(7538,45,'used','0'),(7532,45,'height','70'),(7533,45,'diameter','19.5'),(7534,45,'width','265'),(7535,45,'axle','Прицепная'),(7600,42,'type','Летний'),(7601,42,'brand','КАМА'),(7554,46,'used','0'),(7552,46,'type','Летний'),(7553,46,'brand','КАМА'),(7549,46,'diameter','19.5'),(7550,46,'width','285'),(7551,46,'axle','Рулевая'),(7522,47,'used','0'),(7516,47,'height','70'),(7517,47,'diameter','19.5'),(7518,47,'width','285'),(7519,47,'axle','Ведущая'),(7520,47,'type','Летний'),(7275,63,'width','315'),(7276,63,'axle','Универсальная'),(7521,47,'brand','КАМА'),(7536,45,'type','Летний'),(7683,39,'1cid','00000001559'),(7704,69,'type','Летний'),(7504,48,'type','Летний'),(7501,48,'diameter','22.5'),(7502,48,'width','295'),(7503,48,'axle','Рулевая'),(7486,49,'width','295'),(7487,49,'axle','Ведущая'),(7488,49,'type','Летний'),(7484,49,'height','75'),(7485,49,'diameter','22.5'),(7505,48,'brand','КАМА'),(7490,49,'used','0'),(7491,49,'1cid','00000001694'),(7489,49,'brand','КАМА'),(7474,50,'used','0'),(7469,50,'width','10'),(7470,50,'axle','Универсальная'),(7471,50,'type','Всесезонный'),(7472,50,'brand','КАМА'),(7467,50,'height',''),(10276,51,'1cid','00000001750'),(10269,51,'height',''),(10270,51,'diameter','20'),(10271,51,'width','10'),(10272,51,'axle','Универсальная'),(10273,51,'type','Всесезонный'),(10274,51,'brand','JT RADIAL'),(7453,52,'width','11'),(7454,52,'axle','Рулевая'),(7455,52,'type','Карьерный'),(7456,52,'brand','КАМА'),(7451,52,'height',''),(7452,52,'diameter','22.5'),(7682,39,'used','0'),(7468,50,'diameter','20'),(7419,53,'height',''),(7420,53,'diameter','20'),(7421,53,'width','11'),(7422,53,'axle','Универсальная'),(7423,53,'type','Всесезонный'),(7424,53,'brand','КАМА'),(7425,53,'used','0'),(7473,50,'popular','Да'),(7457,52,'used','0'),(7426,53,'1cid','00000001659'),(7458,52,'1cid','00000001780'),(10275,51,'used','0'),(9226,120,'1cid',''),(10467,132,'fixture','6'),(9935,64,'height',''),(9936,64,'diameter','20'),(9937,64,'width','8.25'),(9938,64,'axle','Универсальная'),(9939,64,'type','Летний'),(7328,60,'brand','КАМА'),(7373,55,'width','315'),(7374,55,'axle','Ведущая'),(7375,55,'type','Летний'),(7442,56,'1cid','00000001623'),(7439,56,'type','Летний'),(7440,56,'brand','КАМА'),(7441,56,'used','0'),(7435,56,'height','80'),(7436,56,'diameter','22.5'),(7437,56,'width','295'),(7394,57,'1cid','00000001562'),(7392,57,'brand','КАМА'),(7393,57,'used','0'),(7389,57,'width','295'),(7390,57,'axle','Рулевая'),(7391,57,'type','Летний'),(7387,57,'height','80'),(7388,57,'diameter','22.5'),(7438,56,'axle','Ведущая'),(9940,64,'brand','КАМА'),(9941,64,'used','0'),(7378,55,'1cid','00000001566'),(7377,55,'used','0'),(7376,55,'brand','КАМА'),(7371,55,'height','70'),(7372,55,'diameter','22.5'),(10473,132,'used','0'),(10472,132,'brand','ASTERRO'),(9933,65,'used','0'),(7330,60,'1cid','00000001564'),(9932,65,'brand','КАМА'),(9930,65,'axle','Универсальная'),(9931,65,'type','Карьерный'),(9927,65,'height',''),(10260,66,'1cid','00000001834'),(10254,66,'axle','Рулевая'),(10255,66,'axle','Ведущая'),(10256,66,'axle','Универсальная'),(10257,66,'type','Летний'),(10258,66,'brand','OMSKSHINA'),(10259,66,'used','0'),(10251,66,'height',''),(10252,66,'diameter','20'),(10253,66,'width','9.00'),(9484,89,'1cid','00000001714'),(9942,64,'1cid','00000001807'),(9928,65,'diameter','20'),(9929,65,'width','10'),(7507,48,'1cid','00000001572'),(9914,67,'axle','Универсальная'),(9915,67,'type','Всесезонный'),(9916,67,'brand','КАМА'),(9911,67,'height',''),(9912,67,'diameter','20'),(6878,68,'type','Всесезонный'),(6874,68,'height','75'),(6875,68,'diameter','16'),(6876,68,'width','185'),(6877,68,'axle','Универсальная'),(9615,98,'used','0'),(7700,69,'height','75'),(7701,69,'diameter','17.5'),(7702,69,'width','215'),(7703,69,'axle','Рулевая'),(9515,92,'used','0'),(9451,70,'used','0'),(9445,70,'height','75'),(9446,70,'diameter','17.5'),(9447,70,'width','215'),(9467,79,'used','0'),(10325,78,'height','75'),(10326,78,'diameter','17.5'),(9464,79,'axle','Ведущая'),(9918,67,'1cid','00000001625'),(10242,75,'1cid','00000001738'),(10241,75,'used','0'),(10331,78,'used','0'),(10332,78,'1cid','00000001461'),(10224,77,'brand','CORDIANT'),(10225,77,'used','0'),(10221,77,'width','385'),(10222,77,'axle','Прицепная'),(10223,77,'type','Всесезонный'),(10219,77,'height','65'),(10327,78,'width','235'),(10328,78,'axle','Ведущая'),(10329,78,'type','Летний'),(10330,78,'brand','NOKIAN'),(10237,75,'width','235'),(10238,75,'axle','Прицепная'),(10239,75,'type','Всесезонный'),(10240,75,'brand','CORDIANT'),(10235,75,'height','75'),(10236,75,'diameter','17.5'),(9465,79,'type','Летний'),(9466,79,'brand','SAVA'),(9461,79,'height','70'),(9462,79,'diameter','19.5'),(9463,79,'width','265'),(9376,80,'1cid','00000001782'),(9374,80,'brand','AEOLUS'),(9368,80,'height','60'),(9369,80,'diameter','22.5'),(9370,80,'width','295'),(9371,80,'axle','Рулевая'),(9812,108,'1cid',''),(8528,81,'width','295'),(8529,81,'axle','Ведущая'),(8530,81,'type','Летний'),(8526,81,'height','60'),(8527,81,'diameter','22.5'),(9373,80,'type','Летний'),(8531,81,'brand','AEOLUS'),(8532,81,'used','0'),(8533,81,'1cid','00000001825'),(9898,82,'axle','Рулевая'),(9899,82,'type','Летний'),(9900,82,'brand','SATOYA'),(9901,82,'used','0'),(9895,82,'height','70'),(9896,82,'diameter','22.5'),(9897,82,'width','315'),(9468,79,'1cid','00000001733'),(9450,70,'brand','SAVA'),(9448,70,'axle','Ведущая'),(9449,70,'type','Всесезонный'),(7706,69,'used','0'),(7707,69,'1cid','00000001622'),(9375,80,'used','0'),(9372,80,'axle','Прицепная'),(9902,82,'1cid','00000001832'),(9862,112,'1cid',''),(7705,69,'brand','КАМА'),(9917,67,'used','0'),(6879,68,'brand','КАМА'),(10818,143,'axle','Прицепная'),(12202,179,'axle','Ведущая'),(12098,85,'1cid',''),(12097,85,'used','0'),(12096,85,'brand','TYREX'),(12095,85,'type','Усиленный'),(12093,85,'type','Всесезонный'),(12094,85,'type','Карьерный'),(12089,85,'height','80'),(10181,86,'used','0'),(10180,86,'brand','КАМА'),(10179,86,'type','Летний'),(10176,86,'diameter','22.5'),(10177,86,'width','385'),(10178,86,'axle','Прицепная'),(10175,86,'height','65'),(11347,134,'used','0'),(10284,87,'1cid',''),(10281,87,'type','Летний'),(10282,87,'brand','КАМА'),(10277,87,'height','65'),(11373,88,'used','0'),(11374,88,'1cid','00000001806'),(11367,88,'height','65'),(11368,88,'diameter','22.5'),(11369,88,'width','385'),(11370,88,'axle','Прицепная'),(11371,88,'type','Летний'),(11210,141,'width','11.75'),(11339,134,'diameter','17.5'),(11372,88,'brand','SAVA'),(9479,89,'width','315'),(9480,89,'axle','Ведущая'),(9481,89,'type','Летний'),(9482,89,'brand','SAVA'),(9477,89,'height','70'),(10311,90,'used','0'),(10310,90,'brand','NOKIAN'),(10305,90,'height','70'),(10306,90,'diameter','22.5'),(10307,90,'width','315'),(9493,91,'height','70'),(9478,89,'diameter','22.5'),(10312,90,'1cid','00000001596'),(10308,90,'axle','Рулевая'),(10309,90,'type','Летний'),(9483,89,'used','0'),(9494,91,'diameter','19.5'),(9499,91,'used','0'),(9495,91,'width','285'),(9496,91,'axle','Рулевая'),(9497,91,'type','Летний'),(9498,91,'brand','SAVA'),(9514,92,'brand','SAVA'),(9509,92,'height','75'),(9510,92,'diameter','17.5'),(9511,92,'width','235'),(9512,92,'axle','Рулевая'),(11343,134,'dia','161'),(12090,85,'diameter','22.5'),(12091,85,'width','315'),(12092,85,'axle','Универсальная'),(9532,93,'1cid',''),(9525,93,'height','70'),(9526,93,'diameter','19.5'),(9527,93,'width','245'),(9528,93,'axle','Рулевая'),(9529,93,'type','Летний'),(9530,93,'brand','SAVA'),(9545,94,'type','Всесезонный'),(9541,94,'height','70'),(9542,94,'diameter','19.5'),(9543,94,'width','245'),(9544,94,'axle','Ведущая'),(9560,95,'axle','Ведущая'),(9561,95,'type','Всесезонный'),(9562,95,'brand','SAVA'),(9563,95,'used','0'),(9557,95,'height','80'),(9558,95,'diameter','22.5'),(9559,95,'width','295'),(9579,96,'used','0'),(9580,96,'1cid',''),(9575,96,'width','315'),(9576,96,'axle','Ведущая'),(9577,96,'type','Летний'),(9578,96,'brand','SAVA'),(9573,96,'height','80'),(9574,96,'diameter','22.5'),(9597,97,'used','0'),(9598,97,'1cid',''),(9589,97,'height','80'),(9590,97,'diameter','22.5'),(9591,97,'width','315'),(9592,97,'axle','Ведущая'),(9593,97,'type','Всесезонный'),(9594,97,'type','Карьерный'),(9595,97,'type','Усиленный'),(9596,97,'brand','SAVA'),(9610,98,'axle','Рулевая'),(9611,98,'type','Всесезонный'),(9612,98,'type','Карьерный'),(9613,98,'type','Усиленный'),(9614,98,'brand','SAVA'),(9607,98,'height','80'),(9608,98,'diameter','22.5'),(9609,98,'width','315'),(9646,99,'brand','SAVA'),(9643,99,'width','385'),(9644,99,'axle','Рулевая'),(9645,99,'type','Летний'),(9664,100,'1cid',''),(9662,100,'brand','Kormoran'),(9663,100,'used','0'),(9657,100,'height','75'),(9658,100,'diameter','17.5'),(9676,101,'axle','Ведущая'),(9677,101,'type','Всесезонный'),(9678,101,'brand','Kormoran'),(9679,101,'used','0'),(9673,101,'height','75'),(9674,101,'diameter','17.5'),(9675,101,'width','235'),(9696,102,'1cid',''),(9694,102,'brand','Kormoran'),(9695,102,'used','0'),(9689,102,'height','80'),(9690,102,'diameter','22.5'),(9691,102,'width','295'),(9692,102,'axle','Ведущая'),(9693,102,'type','Всесезонный'),(9680,101,'1cid',''),(9711,103,'used','0'),(9710,103,'brand','Kormoran'),(9708,103,'axle','Ведущая'),(9705,103,'height','80'),(9706,103,'diameter','22.5'),(9707,103,'width','315'),(9728,104,'1cid',''),(9721,104,'height','75'),(9722,104,'diameter','17.5'),(9723,104,'width','235'),(9724,104,'axle','Рулевая'),(9725,104,'type','Всесезонный'),(9726,104,'brand','Kormoran'),(9727,104,'used','0'),(9744,105,'1cid',''),(9739,105,'width','315'),(9740,105,'axle','Рулевая'),(9741,105,'type','Всесезонный'),(9742,105,'brand','Kormoran'),(9743,105,'used','0'),(9737,105,'height','80'),(9738,105,'diameter','22.5'),(9762,106,'1cid',''),(9756,106,'axle','Ведущая'),(9757,106,'type','Всесезонный'),(9758,106,'type','Карьерный'),(9759,106,'type','Усиленный'),(9760,106,'brand','Kormoran'),(9761,106,'used','0'),(9753,106,'height','80'),(9754,106,'diameter','22.5'),(9755,106,'width','315'),(9777,107,'used','0'),(9776,107,'brand','Kormoran'),(9771,107,'height','65'),(9772,107,'diameter','22.5'),(9809,108,'type','Всесезонный'),(9774,107,'axle','Прицепная'),(9773,107,'width','385'),(9513,92,'type','Летний'),(9548,94,'1cid',''),(9531,93,'used','0'),(9616,98,'1cid',''),(9546,94,'brand','SAVA'),(9564,95,'1cid',''),(9647,99,'used','0'),(9778,107,'1cid',''),(9807,108,'width','10'),(9808,108,'axle','Ведущая'),(9805,108,'height',''),(9806,108,'diameter','20'),(9794,109,'brand','SATOYA'),(9793,109,'type','Летний'),(9791,109,'axle','Ведущая'),(9787,109,'height',''),(9788,109,'diameter','20'),(9822,110,'diameter','20'),(9823,110,'width','11'),(9824,110,'axle','Рулевая'),(9825,110,'axle','Ведущая'),(9826,110,'axle','Универсальная'),(9821,110,'height',''),(9795,109,'used','0'),(9789,109,'width','10'),(9790,109,'axle','Рулевая'),(9846,111,'1cid',''),(9839,111,'height','75'),(9840,111,'diameter','17.5'),(9841,111,'width','235'),(9842,111,'axle','Ведущая'),(9843,111,'type','Всесезонный'),(9844,111,'brand','SATOYA'),(9792,109,'axle','Универсальная'),(9861,112,'used','0'),(9858,112,'axle','Ведущая'),(9859,112,'type','Всесезонный'),(9860,112,'brand','SATOYA'),(9874,113,'axle','Рулевая'),(9875,113,'type','Всесезонный'),(9876,113,'brand','SATOYA'),(9877,113,'used','0'),(9878,113,'1cid',''),(9871,113,'height','80'),(9872,113,'diameter','22.5'),(9873,113,'width','295'),(9890,114,'axle','Ведущая'),(9891,114,'type','Всесезонный'),(9892,114,'brand','SATOYA'),(9893,114,'used','0'),(9894,114,'1cid',''),(9887,114,'height','70'),(9888,114,'diameter','22.5'),(9889,114,'width','315'),(9127,115,'height','70'),(9128,115,'diameter','22.5'),(9129,115,'width','315'),(9130,115,'axle','Рулевая'),(9131,115,'type','Всесезонный'),(9148,116,'type','Всесезонный'),(9149,116,'type','Карьерный'),(9150,116,'type','Усиленный'),(9151,116,'type','Бездорожье'),(10894,117,'used','0'),(10887,117,'height','65'),(10888,117,'diameter','22.5'),(10889,117,'width','385'),(10890,117,'axle','Прицепная'),(10891,117,'type','Всесезонный'),(10892,117,'brand','SATOYA'),(10893,117,'popular','Да'),(9154,116,'1cid',''),(9144,116,'height','80'),(9145,116,'diameter','22.5'),(9146,116,'width','315'),(9147,116,'axle','Ведущая'),(9296,124,'used','0'),(9185,118,'type','Летний'),(9186,118,'brand','SATOYA'),(9181,118,'height','65'),(9182,118,'diameter','22.5'),(9203,119,'axle','Универсальная'),(9204,119,'type','Летний'),(9205,119,'brand','SATOYA'),(9206,119,'used','0'),(9207,119,'1cid',''),(9198,119,'height',''),(9199,119,'diameter','16'),(9200,119,'width','7.50'),(9201,119,'axle','Рулевая'),(9202,119,'axle','Ведущая'),(9188,118,'1cid',''),(9184,118,'axle','Прицепная'),(9225,120,'used','0'),(9223,120,'type','Летний'),(9224,120,'brand','SATOYA'),(9217,120,'height',''),(9218,120,'diameter','16'),(9219,120,'width','8.25'),(10858,121,'brand','KELLY'),(10852,121,'height','65'),(10853,121,'diameter','22.5'),(10854,121,'width','385'),(10855,121,'axle','Прицепная'),(10856,121,'type','Всесезонный'),(9260,122,'used','0'),(9257,122,'axle','Ведущая'),(9258,122,'type','Всесезонный'),(9255,122,'diameter','22.5'),(9256,122,'width','11'),(10877,123,'used','0'),(10870,123,'height',''),(10871,123,'diameter','22.5'),(10872,123,'width','11'),(10873,123,'axle','Ведущая'),(10874,123,'type','Всесезонный'),(10875,123,'brand','AEOLUS'),(9292,124,'axle','Ведущая'),(9293,124,'axle','Универсальная'),(9294,124,'type','Летний'),(9288,124,'height',''),(9289,124,'diameter','20'),(9311,125,'type','Всесезонный'),(9312,125,'brand','AEOLUS'),(9307,125,'height',''),(9308,125,'diameter','20'),(9309,125,'width','11'),(9327,126,'axle','Ведущая'),(9328,126,'type','Всесезонный'),(9329,126,'brand','AEOLUS'),(9330,126,'used','0'),(9324,126,'height',''),(9325,126,'diameter','22.5'),(9345,127,'axle','Ведущая'),(9346,127,'axle','Универсальная'),(9347,127,'type','Летний'),(9348,127,'brand','AEOLUS'),(9349,127,'used','0'),(9350,127,'1cid',''),(9341,127,'height',''),(9342,127,'diameter','20'),(9343,127,'width','12'),(9344,127,'axle','Рулевая'),(9367,128,'1cid',''),(9362,128,'width','265'),(9363,128,'axle','Ведущая'),(9364,128,'type','Всесезонный'),(9365,128,'brand','AEOLUS'),(9366,128,'used','0'),(9360,128,'height','70'),(5853,129,'brand','AEOLUS'),(5854,129,'used','0'),(5847,129,'height','60'),(5848,129,'diameter','22.5'),(5849,129,'width','295'),(11237,130,'used','0'),(11238,130,'1cid',''),(11236,130,'brand','ASTERRO'),(11229,130,'diameter','22.5'),(11230,130,'width','11.75'),(11231,130,'fixture','10'),(11232,130,'pcd','335'),(11233,130,'dia','281'),(11234,130,'et','135'),(10948,131,'brand','NOKIAN'),(10949,131,'used','0'),(10950,131,'1cid',''),(10946,131,'type','Карьерный'),(10947,131,'type','Усиленный'),(10941,131,'height','70'),(10942,131,'diameter','22.5'),(10943,131,'width','315'),(10944,131,'axle','Ведущая'),(10945,131,'type','Всесезонный'),(9132,115,'brand','SATOYA'),(9133,115,'used','0'),(9134,115,'1cid',''),(6880,68,'used','0'),(9712,103,'1cid',''),(9297,124,'1cid',''),(10465,132,'diameter','16'),(10466,132,'width','5.50'),(9934,65,'1cid','00000001688'),(9810,108,'brand','SATOYA'),(9830,110,'1cid',''),(9829,110,'used','0'),(9827,110,'type','Летний'),(9828,110,'brand','SATOYA'),(9220,120,'axle','Рулевая'),(9221,120,'axle','Ведущая'),(9222,120,'axle','Универсальная'),(9153,116,'used','0'),(9152,116,'brand','SATOYA'),(5850,129,'axle','Рулевая'),(5851,129,'axle','Прицепная'),(5852,129,'type','Летний'),(9709,103,'type','Всесезонный'),(9660,100,'axle','Ведущая'),(9661,100,'type','Всесезонный'),(9659,100,'width','215'),(9259,122,'brand','AEOLUS'),(9254,122,'height',''),(10860,121,'used','0'),(10859,121,'popular','Да'),(10857,121,'type','Карьерный'),(11235,130,'type','Штампованный'),(9187,118,'used','0'),(9183,118,'width','385'),(9261,122,'1cid',''),(7282,63,'1cid','00000001569'),(5855,129,'1cid',''),(6881,68,'1cid','00000001698'),(7314,61,'1cid','00000001567'),(9648,99,'1cid',''),(9641,99,'height','65'),(9845,111,'used','0'),(9775,107,'type','Всесезонный'),(9811,108,'used','0'),(9856,112,'diameter','22.5'),(9857,112,'width','295'),(9855,112,'height','80'),(10876,123,'popular','Да'),(9295,124,'brand','AEOLUS'),(9291,124,'axle','Рулевая'),(9290,124,'width','11'),(10817,143,'width','245'),(9313,125,'used','0'),(9310,125,'axle','Ведущая'),(9326,126,'width','12'),(11340,134,'width','6.00'),(9361,128,'diameter','19.5'),(10471,132,'type','Штампованный'),(10226,77,'1cid','00000001589'),(9314,125,'1cid',''),(9331,126,'1cid',''),(10220,77,'diameter','22.5'),(9452,70,'1cid','00000001738'),(9500,91,'1cid',''),(9516,92,'1cid',''),(11401,154,'width','16'),(9796,109,'1cid',''),(10474,132,'1cid',''),(10468,132,'pcd','170'),(10469,132,'dia','130'),(12198,133,'1cid',''),(12197,133,'used','0'),(12196,133,'brand','SANT'),(12193,133,'dia','164'),(12194,133,'et','135'),(12195,133,'type','Штампованный'),(10868,144,'used','0'),(10869,144,'1cid',''),(10878,123,'1cid',''),(10896,145,'height','80'),(10898,145,'width','295'),(10899,145,'axle','Ведущая'),(10897,145,'diameter','22.5'),(10895,117,'1cid',''),(10900,145,'type','Всесезонный'),(10901,145,'brand','NOKIAN'),(10902,145,'used','0'),(10903,145,'1cid',''),(10915,146,'axle','Ведущая'),(10912,146,'height','70'),(10914,146,'width','305'),(10913,146,'diameter','22.5'),(10916,146,'type','Всесезонный'),(10917,146,'brand','NOKIAN'),(10918,146,'used','0'),(10919,146,'1cid',''),(11111,147,'height','60'),(11112,147,'diameter','22.5'),(11113,147,'width','315'),(11114,147,'axle','Ведущая'),(11115,147,'type','Всесезонный'),(11116,147,'brand','NOKIAN'),(11117,147,'used','0'),(11148,148,'brand','NOKIAN'),(11145,148,'width','315'),(11146,148,'axle','Ведущая'),(11147,148,'type','Карьерный'),(11506,149,'1cid',''),(11504,149,'brand','NOKIAN'),(11505,149,'used','0'),(11501,149,'width','315'),(11502,149,'axle','Прицепная'),(11503,149,'type','Всесезонный'),(11499,149,'height','70'),(11500,149,'diameter','22.5'),(11158,150,'1cid',''),(11156,150,'brand','NOKIAN'),(11152,150,'diameter','22.5'),(11153,150,'width','315'),(11154,150,'axle','Ведущая'),(11026,151,'axle','Ведущая'),(11023,151,'height','70'),(11025,151,'width','315'),(11024,151,'diameter','22.5'),(11027,151,'type','Зимний'),(11028,151,'brand','NOKIAN'),(11029,151,'used','0'),(11030,151,'1cid',''),(11042,152,'axle','Ведущая'),(11039,152,'height','70'),(11041,152,'width','315'),(11040,152,'diameter','22.5'),(11043,152,'type','Зимний'),(11044,152,'brand','NOKIAN'),(11045,152,'used','0'),(11046,152,'1cid',''),(11157,150,'used','0'),(11155,150,'type','Зимний'),(11507,158,'height','80'),(11151,150,'height','70'),(11149,148,'used','0'),(11150,148,'1cid',''),(11143,148,'height','70'),(11144,148,'diameter','22.5'),(11218,141,'1cid',''),(11677,153,'used','0'),(11670,153,'width','11.75'),(11676,153,'brand',''),(11671,153,'fixture','10'),(11672,153,'pcd','335'),(11673,153,'dia','281'),(11674,153,'et',''),(11675,153,'type','Штампованный'),(11669,153,'diameter','22.5'),(11403,154,'type','Бездорожье'),(11404,154,'brand','Алтайшина'),(11405,154,'used','0'),(11406,154,'1cid',''),(11427,155,'type','Бездорожье'),(11428,155,'brand','Алтайшина'),(11423,155,'height',''),(11424,155,'diameter','24'),(11425,155,'width','16'),(11426,155,'axle','Универсальная'),(11429,155,'used','0'),(11430,155,'1cid',''),(11431,156,'height',''),(11432,156,'diameter','24'),(11433,156,'width','14'),(11434,156,'axle','Универсальная'),(11435,156,'type','Бездорожье'),(11436,156,'brand','Алтайшина'),(11437,156,'used','0'),(11438,156,'1cid',''),(11497,157,'used','0'),(11496,157,'brand','Алтайшина'),(11495,157,'type','Бездорожье'),(11498,157,'1cid',''),(11508,158,'diameter','18'),(11509,158,'width','12.5'),(11510,158,'axle','Универсальная'),(11511,158,'type','Карьерный'),(11512,158,'type','Бездорожье'),(11513,158,'brand','Алтайшина'),(11514,158,'used','0'),(11515,158,'1cid',''),(11562,159,'type','Усиленный'),(11558,159,'height',''),(11559,159,'diameter','20'),(11560,159,'width','14.00'),(11561,159,'axle','Универсальная'),(11536,160,'type','Бездорожье'),(11532,160,'height',''),(11533,160,'diameter','20'),(11534,160,'width','14.00'),(11535,160,'axle','Универсальная'),(11537,160,'brand','Алтайшина'),(11538,160,'used','0'),(11539,160,'1cid',''),(11554,161,'type','Бездорожье'),(11549,161,'height',''),(11550,161,'diameter','20'),(11551,161,'width','12'),(11552,161,'axle','Универсальная'),(11553,161,'type','Усиленный'),(11555,161,'brand','Алтайшина'),(11556,161,'used','0'),(11557,161,'1cid',''),(11563,159,'type','Бездорожье'),(11564,159,'brand','Алтайшина'),(11565,159,'used','0'),(11566,159,'1cid',''),(11567,162,'height','65'),(11568,162,'diameter','14'),(11569,162,'width','155'),(11570,162,'type','Зимний'),(11571,162,'brand','NORDMAN'),(11572,162,'used','0'),(11573,162,'1cid',''),(11574,163,'height','55'),(11575,163,'diameter','15'),(11576,163,'width','185'),(11577,163,'type','Зимний'),(11578,163,'brand','NORDMAN'),(11579,163,'used','0'),(11580,163,'1cid',''),(11581,164,'height','60'),(11582,164,'diameter','22.5'),(11583,164,'width','315'),(11584,164,'axle','Ведущая'),(11585,164,'type','Всесезонный'),(11586,164,'brand','NOKIAN'),(11587,164,'used','0'),(11588,164,'1cid',''),(11597,165,'height','70'),(11599,165,'width','315'),(11600,165,'axle','Ведущая'),(11598,165,'diameter','22.5'),(11601,165,'type','Всесезонный'),(11602,165,'brand','NOKIAN'),(11603,165,'used','0'),(11604,165,'1cid',''),(11613,166,'height','70'),(11615,166,'width','315'),(11616,166,'axle','Ведущая'),(11614,166,'diameter','22.5'),(11617,166,'type','Всесезонный'),(11618,166,'brand','NOKIAN'),(11619,166,'used','0'),(11620,166,'1cid',''),(11629,167,'height','70'),(11631,167,'width','315'),(11632,167,'axle','Ведущая'),(11630,167,'diameter','22.5'),(11633,167,'type','Всесезонный'),(11634,167,'brand','NOKIAN'),(11635,167,'used','0'),(11636,167,'1cid',''),(11645,168,'height','70'),(11647,168,'width','315'),(11648,168,'axle','Ведущая'),(11646,168,'diameter','22.5'),(11649,168,'type','Всесезонный'),(11650,168,'brand','NOKIAN'),(11651,168,'used','0'),(11652,168,'1cid',''),(11661,169,'height','70'),(11663,169,'width','315'),(11664,169,'axle','Ведущая'),(11662,169,'diameter','22.5'),(11665,169,'type','Всесезонный'),(11666,169,'brand','NOKIAN'),(11667,169,'used','0'),(11668,169,'1cid',''),(11678,153,'1cid',''),(11984,170,'1cid',''),(11981,170,'type','Всесезонный'),(11982,170,'brand',''),(11983,170,'used','0'),(11977,170,'height','65'),(11978,170,'diameter','22.5'),(11979,170,'width','385'),(12000,171,'1cid',''),(11998,171,'brand',''),(11999,171,'used','0'),(11995,171,'width','385'),(11996,171,'axle','Прицепная'),(11997,171,'type','Всесезонный'),(11993,171,'height','65'),(11994,171,'diameter','22.5'),(12016,172,'1cid',''),(12014,172,'brand',''),(12015,172,'used','0'),(12011,172,'width','385'),(12012,172,'axle','Прицепная'),(12013,172,'type','Всесезонный'),(12009,172,'height','65'),(12010,172,'diameter','22.5'),(12022,173,'brand',''),(12017,173,'height','65'),(12018,173,'diameter','22.5'),(12019,173,'width','385'),(12020,173,'axle','Прицепная'),(12021,173,'type','Всесезонный'),(12056,174,'1cid',''),(12053,174,'type','Всесезонный'),(12054,174,'brand','NOKIAN'),(12050,174,'diameter','22.5'),(12051,174,'width','385'),(12052,174,'axle','Прицепная'),(12049,174,'height','65'),(12044,175,'axle','Прицепная'),(12045,175,'type','Всесезонный'),(12046,175,'brand','NOKIAN'),(12047,175,'used','0'),(12041,175,'height','65'),(12042,175,'diameter','22.5'),(12043,175,'width','385'),(12055,174,'used','0'),(12071,176,'used','0'),(12069,176,'type','Всесезонный'),(12070,176,'brand','NOKIAN'),(12066,176,'diameter','22.5'),(12067,176,'width','385'),(12068,176,'axle','Прицепная'),(12065,176,'height','65'),(12048,175,'1cid',''),(12087,177,'used','0'),(12085,177,'type','Всесезонный'),(12086,177,'brand','NOKIAN'),(12082,177,'diameter','22.5'),(12083,177,'width','385'),(12084,177,'axle','Прицепная'),(12081,177,'height','65'),(12072,176,'1cid',''),(12088,177,'1cid',''),(12262,178,'1cid',''),(12261,178,'used','0'),(12257,178,'width','445'),(12258,178,'axle','Прицепная'),(12259,178,'type','Летний'),(12255,178,'height','45'),(12256,178,'diameter','19.5'),(11980,170,'axle','Прицепная'),(12023,173,'used','0'),(12024,173,'1cid',''),(12203,179,'type','Универсальный'),(12199,179,'height','70'),(12201,179,'width','315'),(12200,179,'diameter','22.5'),(12220,180,'brand',''),(12219,180,'type','Всесезонный'),(12215,180,'height','80'),(12216,180,'diameter','22.5'),(12238,181,'1cid',''),(12235,181,'type','Всесезонный'),(12236,181,'brand',''),(12237,181,'used','0'),(12231,181,'height','80'),(12232,181,'diameter','22.5'),(12233,181,'width','295'),(12234,181,'axle','Ведущая'),(12218,180,'axle','Ведущая'),(12217,180,'width','295'),(12260,178,'brand',''),(12252,182,'brand',''),(12247,182,'height','75'),(12248,182,'diameter','22.5'),(12249,182,'width','295'),(12250,182,'axle','Ведущая'),(12251,182,'type','Всесезонный'),(12204,179,'brand',''),(12205,179,'used','0'),(12206,179,'1cid',''),(12221,180,'used','0'),(12222,180,'1cid',''),(12253,182,'used','0'),(12254,182,'1cid','');
/*!40000 ALTER TABLE `cms_catalog_item_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_catalog_items`
--

DROP TABLE IF EXISTS `cms_catalog_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_catalog_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(128) NOT NULL,
  `description` text,
  `available` int(11) DEFAULT '1',
  `price` float DEFAULT '0',
  `discount` int(11) DEFAULT '0',
  `data` text,
  `image_file` varchar(128) DEFAULT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=183 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_catalog_items`
--

LOCK TABLES `cms_catalog_items` WRITE;
/*!40000 ALTER TABLE `cms_catalog_items` DISABLE KEYS */;
INSERT INTO `cms_catalog_items` VALUES (141,7,'Lemmerz 11.75×22.5 10/335/281/130 (рулевой)','',2,10900,NULL,'{\"diameter\":\"22.5\",\"width\":\"11.75\",\"fixture\":\"10\",\"pcd\":\"335\",\"dia\":\"281\",\"et\":\"130\",\"type\":[\"\\u0428\\u0442\\u0430\\u043c\\u043f\\u043e\\u0432\\u0430\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"Lemmerz\",\"used\":\"0\",\"1cid\":\"\"}','catalog/1175225-22-10335281130-d383afd10a.jpeg','lemmerz-1175225-10335281130',1507026244,1),(140,7,'MAXION 11.75x22.5 10/335/281/120','',1,6600,NULL,'{\"diameter\":\"22.5\",\"width\":\"11.75\",\"fixture\":\"10\",\"pcd\":\"335\",\"dia\":\"281\",\"et\":\"120\",\"type\":[\"\\u0428\\u0442\\u0430\\u043c\\u043f\\u043e\\u0432\\u0430\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"Maxion\",\"used\":\"0\",\"1cid\":\"\"}','catalog/1175225-22-10335281130-d281444b09.jpeg','maxion-1175x225-10335281120',1506937494,1),(48,5,'NF-202','<p \"=\"\">Индекс нагрузки 148/145 Исполнение бескамерное Максимальная скорость 130 Сезонность Лето Тип конструкции радиальная Тип рисунка протектора дорожный Категория(индекс) скорости M Вид шин Грузовые ЦМК</p>',2,16700,NULL,'{\"height\":\"75\",\"diameter\":\"22.5\",\"width\":\"295\",\"axle\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"\\u041a\\u0410\\u041c\\u0410\",\"used\":\"0\",\"1cid\":\"00000001572\"}','catalog/nf202-9884beb2eb.jpg','nf-202-3',1505901867,1),(39,5,'NR-202','<p>Индекс нагрузки 132/130 Исполнение бескамерное Максимальная скорость 130 Сезонность Лето Тип конструкции радиальная Тип рисунка протектора дорожный Категория(индекс) скорости M	Вид шин Грузовые ЦМК</p>',4,9000,NULL,'{\"height\":\"75\",\"diameter\":\"17.5\",\"width\":\"235\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"\\u041a\\u0410\\u041c\\u0410\",\"used\":\"0\",\"1cid\":\"00000001559\"}','catalog/nr202-cf258bf7f4.jpg','nr-202',1505892398,1),(139,7,'SANT 11.75x22.5 10/335/281/120','',2,4800,NULL,'{\"diameter\":\"22.5\",\"width\":\"11.75\",\"fixture\":\"10\",\"pcd\":\"335\",\"dia\":\"281\",\"et\":\"120\",\"type\":[\"\\u0428\\u0442\\u0430\\u043c\\u043f\\u043e\\u0432\\u0430\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"SANT\",\"used\":\"0\",\"1cid\":\"\"}','catalog/1175225-22-10335281130-575819a51c.jpeg','sant-1175x225-10335281120',1506937382,1),(138,7,'SANT 11.75х22.5 10/335/281/0','',4,4500,NULL,'{\"diameter\":\"22.5\",\"width\":\"11.75\",\"fixture\":\"10\",\"pcd\":\"335\",\"dia\":\"281\",\"et\":\"\",\"type\":[\"\\u0428\\u0442\\u0430\\u043c\\u043f\\u043e\\u0432\\u0430\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"SANT\",\"used\":\"0\",\"1cid\":\"\"}','catalog/1175225-0-d29582b2fc.jpg','sant-1175225-103352810',1506937150,1),(67,5,'310','',4,13800,NULL,'{\"height\":\"\",\"diameter\":\"20\",\"width\":\"12\",\"axle\":[\"\\u0423\\u043d\\u0438\\u0432\\u0435\\u0440\\u0441\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"\\u041a\\u0410\\u041c\\u0410\",\"used\":\"0\",\"1cid\":\"00000001625\"}','catalog/310-554643cc97.jpg','310-3',1506072721,1),(145,5,'295/80 R22.5 NOKTOP 40 шина грузовая восстановленная на каркасе Michelin','',1,13500,NULL,'{\"height\":\"80\",\"diameter\":\"22.5\",\"width\":\"295\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"NOKIAN\",\"used\":\"0\",\"1cid\":\"\"}','catalog/noktop-40-84df723b31.jpg','29580-r225-noktop-40-michelin',1506945394,1),(60,5,'NR-201','<p>Индекс нагрузки 152/148 Исполнение бескамерное Максимальная скорость 110 Сезонность Лето Тип конструкции радиальная Тип рисунка протектора дорожный Категория(индекс) скорости K	Вид шин Грузовые ЦМК</p>',4,18000,NULL,'{\"height\":\"60\",\"diameter\":\"22.5\",\"width\":\"315\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"\\u041a\\u0410\\u041c\\u0410\",\"used\":\"0\",\"1cid\":\"00000001564\"}','catalog/nr201-426c9a02b6.jpg','nr-201-5',1505972483,1),(58,5,'NF-202','<p>Индекс нагрузки 154/150 Исполнение бескамерное Максимальная скорость 120 Сезонность Лето Тип конструкции радиальная Тип рисунка протектора дорожный Категория(индекс) скорости L Вид шин Грузовые ЦМК</p>',2,17200,NULL,'{\"height\":\"70\",\"diameter\":\"22.5\",\"width\":\"315\",\"axle\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"\\u041a\\u0410\\u041c\\u0410\",\"used\":\"0\",\"1cid\":\"00000001565\"}','catalog/nf202-3ffdee9f16.jpg','nf-202-4',1505972019,1),(144,5,'295/80 R22.5 NOKTOP 40 шина грузовая восстановленная на каркасе Cordiant','',2,12000,NULL,'{\"height\":\"80\",\"diameter\":\"22.5\",\"width\":\"295\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"NOKIAN\",\"used\":\"0\",\"1cid\":\"\"}','catalog/noktop-40-84df723b31.jpg','29580-r225-noktop-40-cordiant',1506944885,1),(37,5,'NR-201','<p \"=\"\">Индекс нагрузки - 126/124. Исполнение бескамерное. Максимальная скорость  - 130. Сезонность - Лето. Тип конструкции радиальная. Тип рисунка протектора - дорожный. Категория(индекс) скорости - M. Вид шин - Грузовые ЦМК.</p>',2,7500,NULL,'{\"height\":\"75\",\"diameter\":\"17.5\",\"width\":\"215\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"\\u041a\\u0410\\u041c\\u0410\",\"used\":\"0\",\"1cid\":\"00000001621\"}','catalog/nr201-d623d2496d.jpg','nr-201',1505889855,1),(143,5,'NOKTOP 72 шина грузовая восстановленная','',2,7000,NULL,'{\"height\":\"70\",\"diameter\":\"17.5\",\"width\":\"245\",\"axle\":[\"\\u041f\\u0440\\u0438\\u0446\\u0435\\u043f\\u043d\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"NOKIAN\",\"used\":\"0\",\"1cid\":\"\"}','catalog/noktop-72-1301215b51.jpg','noktop-72',1506944761,1),(134,7,'ASTERRO 6.00×17.5 М18 6/205/161/115','',0,3700,NULL,'{\"diameter\":\"17.5\",\"width\":\"6.00\",\"fixture\":\"6\",\"pcd\":\"205\",\"dia\":\"161\",\"et\":\"115\",\"type\":[\"\\u0428\\u0442\\u0430\\u043c\\u043f\\u043e\\u0432\\u0430\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"ASTERRO\",\"used\":\"0\",\"1cid\":\"\"}','catalog/600175-18-6205161115-ca6333aef7.jpg','asterro-600175-18-6205161115',1506936033,1),(142,5,'NOKTOP 68 шина грузовая восстановленная на каркасе GOODRIDE','',2,6000,NULL,'{\"height\":\"\",\"diameter\":\"20\",\"width\":\"11.00\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u041a\\u0430\\u0440\\u044c\\u0435\\u0440\\u043d\\u044b\\u0439\"],\"brand\":\"NOKIAN\",\"used\":\"0\",\"1cid\":\"\"}','catalog/noktop-68-09b2b21e31.jpg','noktop-68-goodride',1506944499,1),(29,6,'ЕВРО 519','',0,2300,NULL,'{\"height\":\"65\",\"diameter\":\"15\",\"width\":\"185\",\"type\":[\"\\u0417\\u0438\\u043c\\u043d\\u0438\\u0439\"],\"brand\":\"\\u041a\\u0410\\u041c\\u0410\",\"popular\":[\"\\u0414\\u0430\"],\"used\":\"0\",\"1cid\":\"\"}','catalog/519-413857a0d3.jpg','519',1504874316,1),(157,6,'Forward Safari 500 33х12,5- 15','',0,5900,NULL,'{\"height\":\"33\\u044512.5\",\"diameter\":\"15\",\"width\":\"\",\"type\":[\"\\u0411\\u0435\\u0437\\u0434\\u043e\\u0440\\u043e\\u0436\\u044c\\u0435\"],\"brand\":\"\\u0410\\u043b\\u0442\\u0430\\u0439\\u0448\\u0438\\u043d\\u0430\",\"used\":\"0\",\"1cid\":\"\"}','catalog/forward-safari-500-02-493d8134ef.jpg','forward-safari-500-33125-15',1507030943,1),(77,5,'PROFESSIONAL TR-1','',2,18500,NULL,'{\"height\":\"65\",\"diameter\":\"22.5\",\"width\":\"385\",\"axle\":[\"\\u041f\\u0440\\u0438\\u0446\\u0435\\u043f\\u043d\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"CORDIANT\",\"used\":\"0\",\"1cid\":\"00000001589\"}','catalog/cordiant-tr1-5fb4c4e1a2.jpg','professional-tr-1-2',1506155674,1),(136,7,'ASTERRO 8.25×22.5 M22 10/335/281/154','',3,5000,NULL,'{\"diameter\":\"22.5\",\"width\":\"8.25\",\"fixture\":\"10\",\"pcd\":\"335\",\"dia\":\"281\",\"et\":\"154\",\"type\":[\"\\u0428\\u0442\\u0430\\u043c\\u043f\\u043e\\u0432\\u0430\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"ASTERRO\",\"used\":\"0\",\"1cid\":\"\"}','catalog/825225-m22-10335281154-9f9896de15.jpg','asterro-825225-m22-10335281154',1506936768,1),(137,7,'BETTER 9.00×22.5 10/335/281/175','',4,4500,NULL,'{\"diameter\":\"22.5\",\"width\":\"9.00\",\"fixture\":\"10\",\"pcd\":\"335\",\"dia\":\"281\",\"et\":\"175\",\"type\":[\"\\u0428\\u0442\\u0430\\u043c\\u043f\\u043e\\u0432\\u0430\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"BETTER\",\"used\":\"0\",\"1cid\":\"\"}','catalog/825225-m22-10335281154-9f9896de15.jpg','better-900225-10335281175',1506936928,1),(135,7,'SANT 6.75х17.5 10х225/176/142 (прицеп)','',2,4000,NULL,'{\"diameter\":\"17.5\",\"width\":\"6.75\",\"fixture\":\"10\",\"pcd\":\"225\",\"dia\":\"176\",\"et\":\"142\",\"type\":[\"\\u0428\\u0442\\u0430\\u043c\\u043f\\u043e\\u0432\\u0430\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"SANT\",\"used\":\"0\",\"1cid\":\"\"}','catalog/sant-675175-10225-et142-d176-40130f8665.jpg','sant-675175-10225176142',1506936215,1),(33,4,'NORTEC ER-109 н.с.16 инд.146 спецшина','<p \"=\"\">Для экскаваторов, погрузчиков.</p>',0,11900,NULL,'{\"height\":\"\",\"diameter\":\"20\",\"width\":\"10\",\"axle\":[\"\\u0423\\u043d\\u0438\\u0432\\u0435\\u0440\\u0441\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f\"],\"type\":[\"\\u0411\\u0435\\u0437\\u0434\\u043e\\u0440\\u043e\\u0436\\u044c\\u0435\"],\"brand\":\"\\u0410\\u043b\\u0442\\u0430\\u0439\\u0448\\u0438\\u043d\\u0430\",\"used\":\"0\",\"1cid\":\"\"}','catalog/er-109-620bda43e5.png','nortec-er-109-16-146',1504874316,1),(38,5,'NF-202','<p>Индекс нагрузки 132/130 Исполнение бескамерное Максимальная скорость 130 Сезонность Лето Тип конструкции радиальная Тип рисунка протектора дорожный Категория(индекс) скорости M	Вид шин Грузовые ЦМК</p>',4,8700,NULL,'{\"height\":\"75\",\"diameter\":\"17.5\",\"width\":\"235\",\"axle\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"\\u041a\\u0410\\u041c\\u0410\",\"used\":\"0\",\"1cid\":\"00000001558\"}','catalog/nf202-780ddba904.jpg','nf-202',1505890980,1),(68,5,'301','',4,3000,NULL,'{\"height\":\"75\",\"diameter\":\"16\",\"width\":\"185\",\"axle\":[\"\\u0423\\u043d\\u0438\\u0432\\u0435\\u0440\\u0441\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"\\u041a\\u0410\\u041c\\u0410\",\"used\":\"0\",\"1cid\":\"00000001698\"}','catalog/301-4a8488e206.jpg','301',1505889627,1),(69,5,'NF 202','',6,7400,NULL,'{\"height\":\"75\",\"diameter\":\"17.5\",\"width\":\"215\",\"axle\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"\\u041a\\u0410\\u041c\\u0410\",\"used\":\"0\",\"1cid\":\"00000001622\"}','catalog/nf202-1a19995056.png','nf-202-5',1505890285,1),(70,5,'ORJAK 4 126/124 M','<p>Индекс нагрузки 126/124</p>',2,9900,NULL,'{\"height\":\"75\",\"diameter\":\"17.5\",\"width\":\"215\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"SAVA\",\"used\":\"0\",\"1cid\":\"00000001738\"}','catalog/sava-orjak-4-2a04b930fd.jpg','orjak-4-126124-m',1506156372,1),(40,5,'NT-202','<p>Индекс нагрузки 143/141 Исполнение бескамерное Максимальная скорость 100 Сезонность Лето Тип конструкции радиальная Тип рисунка протектора дорожный Категория(индекс) скорости J	Вид шин Грузовые ЦМК</p>',0,NULL,NULL,'{\"height\":\"75\",\"diameter\":\"17.5\",\"width\":\"235\",\"axle\":[\"\\u041f\\u0440\\u0438\\u0446\\u0435\\u043f\\u043d\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"\\u041a\\u0410\\u041c\\u0410\",\"used\":\"0\",\"1cid\":\"00000001560\"}','catalog/nt202-175-44573648d3.jpg','nt-202',1505892814,1),(41,5,'NF-201','<p>Индекс нагрузки 136/134 Исполнение бескамерное Максимальная скорость 130 Сезонность Лето Тип конструкции радиальная Тип рисунка протектора дорожный Категория(индекс) скорости M	Вид шин Грузовые ЦМК</p>',1,9000,NULL,'{\"height\":\"70\",\"diameter\":\"19.5\",\"width\":\"245\",\"axle\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"\\u041a\\u0410\\u041c\\u0410\",\"used\":\"0\",\"1cid\":\"00000001660\"}','catalog/nf201-e30bedbb83.png','nf-201-3',1505892993,1),(42,5,'NR-201','<p>Индекс нагрузки 136/134 Исполнение бескамерное Максимальная скорость 130 Сезонность Лето Тип конструкции радиальная Тип рисунка протектора дорожный Категория(индекс) скорости M	Вид шин Грузовые ЦМК</p>',4,9300,NULL,'{\"height\":\"70\",\"diameter\":\"19.5\",\"width\":\"245\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"\\u041a\\u0410\\u041c\\u0410\",\"used\":\"0\",\"1cid\":\"00000001664\"}','catalog/nr201-75016caea5.jpg','nr-201-2',1505893160,1),(43,5,'NF-201','<p>Индекс нагрузки 148/145 Исполнение бескамерное Максимальная скорость 130 Сезонность Лето Тип конструкции радиальная Тип рисунка протектора дорожный Категория(индекс) скорости M	Вид шин Грузовые ЦМК</p>',2,13600,NULL,'{\"height\":\"70\",\"diameter\":\"22.5\",\"width\":\"275\",\"axle\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"\\u041a\\u0410\\u041c\\u0410\",\"used\":\"0\",\"1cid\":\"00000001665\"}','catalog/nf201-878533889e.png','nf-201-4',1505893441,1),(44,5,'NR-201','<p>Индекс нагрузки 148/145 Исполнение бескамерное Максимальная скорость 120 Сезонность Лето Тип конструкции радиальная Тип рисунка протектора дорожный Категория(индекс) скорости L	Вид шин Грузовые ЦМК</p>',4,14500,NULL,'{\"height\":\"70\",\"diameter\":\"22.5\",\"width\":\"275\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"\\u041a\\u0410\\u041c\\u0410\",\"used\":\"0\",\"1cid\":\"00000001561\"}','catalog/nr201-f8d8bc9027.jpg','nr-201-3',1505893917,1),(45,5,'NT-202','<p>Индекс нагрузки 143/141 Исполнение бескамерное Максимальная скорость 100 Сезонность Лето Тип конструкции радиальная Тип рисунка протектора дорожный Категория(индекс) скорости J	Вид шин Грузовые ЦМК</p>',2,11000,NULL,'{\"height\":\"70\",\"diameter\":\"19.5\",\"width\":\"265\",\"axle\":[\"\\u041f\\u0440\\u0438\\u0446\\u0435\\u043f\\u043d\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"\\u041a\\u0410\\u041c\\u0410\",\"used\":\"0\",\"1cid\":\"00000001661\"}','catalog/nt-202-fc21b9f15f.jpg','nt-202-2',1505901246,1),(46,5,'NF-202','<p>Индекс нагрузки 145/143 Исполнение бескамерное Максимальная скорость 130 Сезонность Лето Тип конструкции радиальная Тип рисунка протектора дорожный Категория(индекс) скорости M	Вид шин Грузовые ЦМК</p>',2,12300,NULL,'{\"height\":\"70\",\"diameter\":\"19.5\",\"width\":\"285\",\"axle\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"\\u041a\\u0410\\u041c\\u0410\",\"used\":\"0\",\"1cid\":\"00000001693\"}','catalog/nf202-e6f398d334.jpg','nf-202-2',1505894340,1),(47,5,'NR-201','<p>Индекс нагрузки 145/143 Исполнение бескамерное Максимальная скорость 130 Сезонность Лето Тип конструкции радиальная Тип рисунка протектора дорожный Категория(индекс) скорости M	Вид шин Грузовые ЦМК</p>',4,12800,NULL,'{\"height\":\"70\",\"diameter\":\"19.5\",\"width\":\"285\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"\\u041a\\u0410\\u041c\\u0410\",\"used\":\"0\",\"1cid\":\"00000001692\"}','catalog/nr201-ee108595a2.jpg','nr-201-4',1505901567,1),(49,5,'NR-202','<p \"=\"\">Индекс нагрузки 148/145 Исполнение бескамерное Максимальная скорость 130 Сезонность Лето Тип конструкции радиальная Тип рисунка протектора дорожный  Категория(индекс) скорости M Вид шин Грузовые ЦМК</p>',4,16900,NULL,'{\"height\":\"75\",\"diameter\":\"22.5\",\"width\":\"295\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"\\u041a\\u0410\\u041c\\u0410\",\"used\":\"0\",\"1cid\":\"00000001694\"}','catalog/nr202-9f5679d4f1.jpg','nr-202-2',1505902643,1),(50,5,'310','<p \"=\"\">Шины Кама-310 имеют комбинированную конструкцию, камерное исполнение, комплектуется камерой 10,00-20. Максимальная нагрузка на одну шину составляет 3000 кгс, на сдвоенное колесо 2725 кгс. Обозначение НД: ГОСТ-5513-97.</p>',8,10500,NULL,'{\"height\":\"\",\"diameter\":\"20\",\"width\":\"10\",\"axle\":[\"\\u0423\\u043d\\u0438\\u0432\\u0435\\u0440\\u0441\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"\\u041a\\u0410\\u041c\\u0410\",\"popular\":[\"\\u0414\\u0430\"],\"used\":\"0\",\"1cid\":\"00000001624\"}','catalog/310-6b463ec908.jpg','310',1505903308,1),(51,5,'JT268','<p>Индекс нагрузки 149/146. Индекс скорости К. Норма слойности 18.</p>',4,15000,NULL,'{\"height\":\"\",\"diameter\":\"20\",\"width\":\"10\",\"axle\":[\"\\u0423\\u043d\\u0438\\u0432\\u0435\\u0440\\u0441\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"JT RADIAL\",\"used\":\"0\",\"1cid\":\"00000001750\"}','catalog/jt-radial-jt268-3b746c6928.jpg','jt268',1506073493,1),(80,5,'ASL33 TL PR 18 152/149 M','<p>Индекс нагрузки 152/149. Индекс скорости М.</p>',2,14700,NULL,'{\"height\":\"60\",\"diameter\":\"22.5\",\"width\":\"295\",\"axle\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\",\"\\u041f\\u0440\\u0438\\u0446\\u0435\\u043f\\u043d\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"AEOLUS\",\"used\":\"0\",\"1cid\":\"00000001782\"}','catalog/asl33-aeolus-593d73ae6f.png','asl33-tl-pr-18-152149-m',1506593160,1),(52,5,'NF 701','<p>Индекс нагрузки 148/145 Исполнение бескамерное Максимальная скорость 110 Сезонность Лето Тип конструкции радиальная Тип рисунка протектора дорожный/внедорожный Категория(индекс) скорости K	Вид шин Грузовые ЦМК</p>',4,13600,NULL,'{\"height\":\"\",\"diameter\":\"22.5\",\"width\":\"11\",\"axle\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\"],\"type\":[\"\\u041a\\u0430\\u0440\\u044c\\u0435\\u0440\\u043d\\u044b\\u0439\"],\"brand\":\"\\u041a\\u0410\\u041c\\u0410\",\"used\":\"0\",\"1cid\":\"00000001780\"}','catalog/nf701-94c0963395.png','nf-701',1505904412,1),(53,5,'310','',4,12200,NULL,'{\"height\":\"\",\"diameter\":\"20\",\"width\":\"11\",\"axle\":[\"\\u0423\\u043d\\u0438\\u0432\\u0435\\u0440\\u0441\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"\\u041a\\u0410\\u041c\\u0410\",\"used\":\"0\",\"1cid\":\"00000001659\"}','catalog/310-48cf2c5c6e.jpg','310-2',1505913677,1),(132,7,'ASTERRO 5.50×16/6х170/130/106 (Газель)','',2,2000,NULL,'{\"diameter\":\"16\",\"width\":\"5.50\",\"fixture\":\"6\",\"pcd\":\"170\",\"dia\":\"130\",\"et\":\"160\",\"type\":[\"\\u0428\\u0442\\u0430\\u043c\\u043f\\u043e\\u0432\\u0430\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"ASTERRO\",\"used\":\"0\",\"1cid\":\"\"}','catalog/55016-6170130106-6239ffbf0d.jpg','asterro-550166170130106',1506595238,1),(61,5,'NF-201','<p>Индекс нагрузки 156/150 Исполнение бескамерное Максимальная скорость 120 Сезонность Лето Тип конструкции радиальная Тип рисунка протектора дорожный Ширина профиля 315 Высота профиля 80 Посадочный диаметр 22,5 Категория(индекс) скорости L	Вид шин Грузовые ЦМК</p>',2,18000,NULL,'{\"height\":\"80\",\"diameter\":\"22.5\",\"width\":\"315\",\"axle\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"\\u041a\\u0410\\u041c\\u0410\",\"used\":\"0\",\"1cid\":\"00000001567\"}','catalog/nf201-68980e8c28.png','nf-201-7',1505972633,1),(55,5,'NR-202','<p \"=\"\">Индекс нагрузки 152/148 Исполнение бескамерное Максимальная скорость 120 Сезонность Лето Тип конструкции радиальная Тип рисунка протектора дорожный Категория(индекс) скорости L Вид шин Грузовые ЦМК</p>',4,18900,NULL,'{\"height\":\"70\",\"diameter\":\"22.5\",\"width\":\"315\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"\\u041a\\u0410\\u041c\\u0410\",\"used\":\"0\",\"1cid\":\"00000001566\"}','catalog/nr202-3022813eb0.jpg','nr-202-4',1505971518,1),(59,5,'NF-201','<p>Индекс нагрузки 152/148 Исполнение бескамерное Максимальная скорость 120 Сезонность Лето Тип конструкции радиальная Тип рисунка протектора дорожный Категория(индекс) скорости L	Вид шин Грузовые ЦМК</p>',2,18000,NULL,'{\"height\":\"60\",\"diameter\":\"22.5\",\"width\":\"315\",\"axle\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"\\u041a\\u0410\\u041c\\u0410\",\"used\":\"0\",\"1cid\":\"00000001563\"}','catalog/nf201-8d3ab9d919.png','nf-201-6',1505972314,1),(56,5,'NR-202','<p>Индекс нагрузки 152/148 Исполнение бескамерное Максимальная скорость 130 Сезонность Лето Тип конструкции радиальная Тип рисунка протектора дорожный Категория(индекс) скорости M	Вид шин Грузовые ЦМК</p>',4,18500,NULL,'{\"height\":\"80\",\"diameter\":\"22.5\",\"width\":\"295\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"\\u041a\\u0410\\u041c\\u0410\",\"used\":\"0\",\"1cid\":\"00000001623\"}','catalog/nr201-aee46f6bd1.jpg','nr-202-3',1505913507,1),(57,5,'NF-201','<p>Индекс нагрузки 152/148 Исполнение бескамерное Максимальная скорость 130 Сезонность Лето Тип конструкции радиальная Тип рисунка протектора дорожный Категория(индекс) скорости M	Вид шин Грузовые ЦМК</p>',2,16800,NULL,'{\"height\":\"80\",\"diameter\":\"22.5\",\"width\":\"295\",\"axle\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"\\u041a\\u0410\\u041c\\u0410\",\"used\":\"0\",\"1cid\":\"00000001562\"}','catalog/nf201-1cd60cf707.png','nf-201-5',1505913865,1),(62,5,'NR-201','<p>Индекс нагрузки 156/150 Исполнение бескамерное Максимальная скорость 120 Сезонность Лето Тип конструкции радиальная Тип рисунка протектора дорожный Посадочный диаметр 22,5 Категория(индекс) скорости L	Вид шин Грузовые ЦМК</p>',4,18600,NULL,'{\"height\":\"80\",\"diameter\":\"22.5\",\"width\":\"315\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"\\u041a\\u0410\\u041c\\u0410\",\"used\":\"0\",\"1cid\":\"00000001568\"}','catalog/nr201-5eeb9c6055.jpg','nr-201-6',1505972759,1),(63,5,'NU-701','<p>Индекс нагрузки 156/150 Исполнение бескамерное Максимальная скорость 110 Новинка Да Сезонность Лето Тип конструкции радиальная Тип рисунка протектора универсальный Категория(индекс) скорости K	Вид шин Грузовые ЦМК</p>',4,19900,NULL,'{\"height\":\"80\",\"diameter\":\"22.5\",\"width\":\"315\",\"axle\":[\"\\u0423\\u043d\\u0438\\u0432\\u0435\\u0440\\u0441\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\",\"\\u041a\\u0430\\u0440\\u044c\\u0435\\u0440\\u043d\\u044b\\u0439\",\"\\u0423\\u0441\\u0438\\u043b\\u0435\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"\\u041a\\u0410\\u041c\\u0410\",\"used\":\"0\",\"1cid\":\"00000001569\"}','catalog/nu701-1e13b0f288.jpg','nu-701',1505973792,1),(64,5,'У-2 НК. ШЗ 10 125/122 J','<p>Индекс нагрузки 125/122 Исполнение камерное Максимальная скорость 100 Сезонность Лето Тип конструкции радиальная Тип рисунка протектора универсальный Категория(индекс) скорости J	Вид шин Грузовые Комбинированные</p>',0,6000,NULL,'{\"height\":\"\",\"diameter\":\"20\",\"width\":\"8,25\",\"axle\":[\"\\u0423\\u043d\\u0438\\u0432\\u0435\\u0440\\u0441\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"\\u041a\\u0410\\u041c\\u0410\",\"used\":\"0\",\"1cid\":\"00000001807\"}','catalog/2-7b3f37c369.jpg','2-10-125122-j',1505974038,1),(65,5,'701 (16 норма слойности)','<p>Индекс нагрузки 147/143 Исполнение камерное Конструкция каркаса и брекера комбинированная Максимальная нагрузка 3 075/2 725 Максимальная скорость 80 Обод рекомендуемый 7.5-20 Сезонность Лето Средняя масса покрышки 64,5 Тип конструкции радиальная Тип рисунка протектора карьерный Ширина профиля 280 Посадочный диаметр 20 Категория(индекс) скорости F	Вид шин Грузовые Комбинированные</p>',0,10900,NULL,'{\"height\":\"\",\"diameter\":\"20\",\"width\":\"10\",\"axle\":[\"\\u0423\\u043d\\u0438\\u0432\\u0435\\u0440\\u0441\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f\"],\"type\":[\"\\u041a\\u0430\\u0440\\u044c\\u0435\\u0440\\u043d\\u044b\\u0439\"],\"brand\":\"\\u041a\\u0410\\u041c\\u0410\",\"used\":\"0\",\"1cid\":\"00000001688\"}','catalog/701-3387a32005.jpg','701-16',1505975048,1),(66,5,'ИН-142Б-1 (без о/л)','<p>Индекс нагрузки 140/137 Исполнение камерное Максимальная скорость 110 Сезонность Лето Тип конструкции радиальная Тип рисунка протектора универсальный Ширина профиля 9,00 Посадочный диаметр 20 Категория(индекс) скорости J	Вид шин Грузовые Комбинированные</p>',4,8000,NULL,'{\"height\":\"\",\"diameter\":\"20\",\"width\":\"9.00\",\"axle\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\",\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\",\"\\u0423\\u043d\\u0438\\u0432\\u0435\\u0440\\u0441\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"OMSKSHINA\",\"used\":\"0\",\"1cid\":\"00000001834\"}','catalog/142-65a9899c86.png','142-1',1506094057,1),(81,5,'HN 355 TL PR18 150/147К','<p>Индекс нагрузки 150/147. Индекс скорости К</p>',4,17000,NULL,'{\"height\":\"60\",\"diameter\":\"22.5\",\"width\":\"295\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"AEOLUS\",\"used\":\"0\",\"1cid\":\"00000001825\"}','catalog/aeolushn355-7e39c11064.jpg','hn-355-tl-pr18-150147',1506593254,1),(75,5,'PROFESSIONAL TR-1','<p>Индекс нагрузки 143/141. Индекс скорости J</p>',8,9200,NULL,'{\"height\":\"75\",\"diameter\":\"17.5\",\"width\":\"235\",\"axle\":[\"\\u041f\\u0440\\u0438\\u0446\\u0435\\u043f\\u043d\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"CORDIANT\",\"used\":\"0\",\"1cid\":\"00000001738\"}','catalog/cordiant-professional-tr-1-64dfc3f7ce.jpg','professional-tr-1',1506147878,1),(78,5,'NTR 45','',1,9900,NULL,'{\"height\":\"75\",\"diameter\":\"17.5\",\"width\":\"235\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"NOKIAN\",\"used\":\"0\",\"1cid\":\"00000001461\"}','catalog/nokian-ntr-45-17-e577ae38a4.png','ntr-45',1506155959,1),(79,5,'ORJAK 3 140/138 M','<p>Индекс нагрузки 140/138.</p>',4,14600,NULL,'{\"height\":\"70\",\"diameter\":\"19.5\",\"width\":\"265\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"SAVA\",\"used\":\"0\",\"1cid\":\"00000001733\"}','catalog/sava-orjak-3-417710853f.jpg','orjak-3-140138-m',1506426478,1),(82,5,'SF-042 TL PR20 154/150','<p>Индекс нагрузки 154/152.</p>',2,16700,NULL,'{\"height\":\"70\",\"diameter\":\"22.5\",\"width\":\"315\",\"axle\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"SATOYA\",\"used\":\"0\",\"1cid\":\"00000001832\"}','catalog/satoya-sf042-1f70ace03f.jpg','sf-042-tl-pr20-154150',1506589422,1),(85,5,'VM-1','<p>Индекс нагрузки 156/150. Индекс скорости К</p>',4,15500,NULL,'{\"height\":\"80\",\"diameter\":\"22.5\",\"width\":\"315\",\"axle\":[\"\\u0423\\u043d\\u0438\\u0432\\u0435\\u0440\\u0441\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\",\"\\u041a\\u0430\\u0440\\u044c\\u0435\\u0440\\u043d\\u044b\\u0439\",\"\\u0423\\u0441\\u0438\\u043b\\u0435\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"TYREX\",\"used\":\"0\",\"1cid\":\"\"}','catalog/tyrex-vm1-47e62010f3.jpg','vm-1',1506155819,1),(179,5,'Bandamatic DDL 18 шина грузовая восстановленная на каркасе GoodYear','',4,12000,NULL,'{\"height\":\"70\",\"diameter\":\"22.5\",\"width\":\"315\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0423\\u043d\\u0438\\u0432\\u0435\\u0440\\u0441\\u0430\\u043b\\u044c\\u043d\\u044b\\u0439\"],\"brand\":\"\",\"used\":\"0\",\"1cid\":\"\"}','catalog/10-7e191480a2.jpg','bandamatic-ddl-18-goodyear',1507110132,1),(86,5,'NT-201','<p>Индекс нагрузки 160</p>',4,18900,NULL,'{\"height\":\"65\",\"diameter\":\"22.5\",\"width\":\"385\",\"axle\":[\"\\u041f\\u0440\\u0438\\u0446\\u0435\\u043f\\u043d\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"\\u041a\\u0410\\u041c\\u0410\",\"used\":\"0\",\"1cid\":\"00000001570\"}','catalog/kama-nt201-7d6ed358fc.png','nt-201',1506072811,1),(87,5,'NF-202','',2,19600,NULL,'{\"height\":\"65\",\"diameter\":\"22.5\",\"width\":\"385\",\"axle\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"\\u041a\\u0410\\u041c\\u0410\",\"used\":\"0\",\"1cid\":\"\"}','catalog/nf202-500fe4a4d2.png','nf-202-6',1506073256,1),(88,5,'CARGO 4 TL 164/158 L','<p>Увеличенная максимальная нагрузка - 5 тонн.</p>',2,21000,NULL,'{\"height\":\"65\",\"diameter\":\"22.5\",\"width\":\"385\",\"axle\":[\"\\u041f\\u0440\\u0438\\u0446\\u0435\\u043f\\u043d\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"SAVA\",\"used\":\"0\",\"1cid\":\"00000001806\"}','catalog/sava-cargo-719a0ae7b8.jpg','cargo-4-tl-164158-l',1506512280,1),(89,5,'ORJAK 4 152/148M','',4,19500,NULL,'{\"height\":\"70\",\"diameter\":\"22.5\",\"width\":\"315\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"SAVA\",\"used\":\"0\",\"1cid\":\"00000001714\"}','catalog/sava-orjak-4-cfec18c283.jpg','orjak-4-152148m',1506427875,1),(90,5,'NTR 52','',2,22000,NULL,'{\"height\":\"70\",\"diameter\":\"22,5\",\"width\":\"315\",\"axle\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"NOKIAN\",\"used\":\"0\",\"1cid\":\"00000001596\"}','catalog/nokian-ntr-45-17-3972cfdc25.png','ntr-52',1506156131,1),(91,5,'AVANT A3','<p>Индекс нагрузки 146/140</p>',0,17450,NULL,'{\"height\":\"70\",\"diameter\":\"19.5\",\"width\":\"285\",\"axle\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"SAVA\",\"used\":\"0\",\"1cid\":\"\"}','catalog/sava-avant-a3-2bece4d507.jpg','avant-a3',1506428100,1),(92,5,'AVANT 4','',0,12800,NULL,'{\"height\":\"75\",\"diameter\":\"17.5\",\"width\":\"235\",\"axle\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"SAVA\",\"used\":\"0\",\"1cid\":\"\"}','catalog/sava-avant4-c6785230ce.png','avant-4',1506429971,1),(93,5,'AVANT A3','',0,14730,NULL,'{\"height\":\"70\",\"diameter\":\"19.5\",\"width\":\"245\",\"axle\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"SAVA\",\"used\":\"0\",\"1cid\":\"\"}','catalog/sava-avant-a3-1203c81e94.jpg','avant-a3-2',1506433711,1),(94,5,'ORJAK 3','',0,15000,NULL,'{\"height\":\"70\",\"diameter\":\"19.5\",\"width\":\"245\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"SAVA\",\"used\":\"0\",\"1cid\":\"\"}','catalog/sava-orjak-3-248095cc11.jpg','orjak-3',1506433830,1),(95,5,'ORJAK 4','',0,19850,NULL,'{\"height\":\"80\",\"diameter\":\"22.5\",\"width\":\"295\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"SAVA\",\"used\":\"0\",\"1cid\":\"\"}','catalog/sava-orjak-4-5d263643f9.jpg','orjak-4',1506510399,1),(96,5,'ORJAK 4 PLUS','',0,19380,NULL,'{\"height\":\"80\",\"diameter\":\"22.5\",\"width\":\"315\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"SAVA\",\"used\":\"0\",\"1cid\":\"\"}','catalog/sava-orjak-4-1ac5be9eb2.jpg','orjak-4-plus',1506511519,1),(97,5,'ORJAK MS','',0,23200,NULL,'{\"height\":\"80\",\"diameter\":\"22.5\",\"width\":\"315\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\",\"\\u041a\\u0430\\u0440\\u044c\\u0435\\u0440\\u043d\\u044b\\u0439\",\"\\u0423\\u0441\\u0438\\u043b\\u0435\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"SAVA\",\"used\":\"0\",\"1cid\":\"\"}','catalog/sava-orjak-ms-494c5ef995.jpg','orjak-ms',1506511880,1),(98,5,'AVANT MS2','',0,21550,NULL,'{\"height\":\"80\",\"diameter\":\"22.5\",\"width\":\"315\",\"axle\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\",\"\\u041a\\u0430\\u0440\\u044c\\u0435\\u0440\\u043d\\u044b\\u0439\",\"\\u0423\\u0441\\u0438\\u043b\\u0435\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"SAVA\",\"used\":\"0\",\"1cid\":\"\"}','catalog/sava-avant-ms2-333af9e2bf.png','avant-ms2',1506512075,1),(99,5,'AVANT 4 PLUS','',0,24000,NULL,'{\"height\":\"65\",\"diameter\":\"22.5\",\"width\":\"385\",\"axle\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"SAVA\",\"used\":\"0\",\"1cid\":\"\"}','catalog/sava-avant4-02b11e9031.png','avant-4-plus',1506512459,1),(100,5,'ROADS 2D','',0,11100,NULL,'{\"height\":\"75\",\"diameter\":\"17.5\",\"width\":\"215\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"Kormoran\",\"used\":\"0\",\"1cid\":\"\"}','catalog/kormoran-roads-2d-7cbcb9eb42.jpg','roads-2d',1506512618,1),(101,5,'ROADS 2D','',1,12900,NULL,'{\"height\":\"75\",\"diameter\":\"17.5\",\"width\":\"235\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"Kormoran\",\"used\":\"0\",\"1cid\":\"\"}','catalog/kormoran-roads-2d-379099d687.jpg','roads-2d-4',1506512792,1),(102,5,'ROADS 2D','',0,20200,NULL,'{\"height\":\"80\",\"diameter\":\"22.5\",\"width\":\"295\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"Kormoran\",\"used\":\"0\",\"1cid\":\"\"}','catalog/kormoran-roads-2d-074ae0e987.jpg','roads-2d-2',1506513014,1),(103,5,'ROADS 2D','',0,21500,NULL,'{\"height\":\"80\",\"diameter\":\"22.5\",\"width\":\"315\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"Kormoran\",\"used\":\"0\",\"1cid\":\"\"}','catalog/kormoran-roads-2d-02ce21d407.jpg','roads-2d-3',1506513520,1),(104,5,'ROADS 2F','',0,13300,NULL,'{\"height\":\"75\",\"diameter\":\"17.5\",\"width\":\"235\",\"axle\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"Kormoran\",\"used\":\"0\",\"1cid\":\"\"}','catalog/kormoran-roads-2f-2cccff08e5.jpg','roads-2f',1506513632,1),(105,5,'ROADS 2F','',2,21000,NULL,'{\"height\":\"80\",\"diameter\":\"22.5\",\"width\":\"315\",\"axle\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"Kormoran\",\"used\":\"0\",\"1cid\":\"\"}','catalog/kormoran-roads-2f-091bed4c10.jpg','roads-2f-2',1506513716,1),(106,5,'D ON/OFF','',0,23500,NULL,'{\"height\":\"80\",\"diameter\":\"22.5\",\"width\":\"315\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\",\"\\u041a\\u0430\\u0440\\u044c\\u0435\\u0440\\u043d\\u044b\\u0439\",\"\\u0423\\u0441\\u0438\\u043b\\u0435\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"Kormoran\",\"used\":\"0\",\"1cid\":\"\"}','catalog/kormoran-d-8996cc1f56.jpg','d-onoff',1506513817,1),(107,5,'ON/OFF','',0,24350,NULL,'{\"height\":\"65\",\"diameter\":\"22.5\",\"width\":\"385\",\"axle\":[\"\\u041f\\u0440\\u0438\\u0446\\u0435\\u043f\\u043d\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"Kormoran\",\"used\":\"0\",\"1cid\":\"\"}','catalog/kormoran-onoff-672a2072b9.jpg','onoff',1506513903,1),(108,5,'SD-062 TT PR18 149/146 K M+S Ведущая','<p>Индекс нагрузки 149/146. Индекс скорости К. Норма слойности 20</p>',0,16850,NULL,'{\"height\":\"\",\"diameter\":\"20\",\"width\":\"10\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"SATOYA\",\"used\":\"0\",\"1cid\":\"\"}','catalog/satoya-sd-062-98477ade14.jpg','sd-062-tt-pr18-149146-k-ms',1506514666,1),(109,5,'SU-022 TT PR18 149/146 K Универсальная','',0,16450,NULL,'{\"height\":\"\",\"diameter\":\"20\",\"width\":\"10\",\"axle\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\",\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\",\"\\u0423\\u043d\\u0438\\u0432\\u0435\\u0440\\u0441\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"SATOYA\",\"used\":\"0\",\"1cid\":\"\"}','catalog/satoya-su-022-d72a789d50.jpg','su-022-tt-pr18-149146-k',1506514492,1),(110,5,'SU-022 TT PR18 152/149 K Универсальная','',0,18500,NULL,'{\"height\":\"\",\"diameter\":\"20\",\"width\":\"11\",\"axle\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\",\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\",\"\\u0423\\u043d\\u0438\\u0432\\u0435\\u0440\\u0441\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"SATOYA\",\"used\":\"0\",\"1cid\":\"\"}','catalog/satoya-su-022-6222b16b65.jpg','su-022-tt-pr18-152149-k',1506514975,1),(111,5,'SD-060 TL PR16 132/129 M M+S Ведущая','',0,9700,NULL,'{\"height\":\"75\",\"diameter\":\"17.5\",\"width\":\"235\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"SATOYA\",\"used\":\"0\",\"1cid\":\"\"}','catalog/satoya-sd-060-4af4ebb6cf.jpg','sd-060-tl-pr16-132129-m-ms',1506588838,1),(112,5,'SD-064 TL PR18 152/149 M M+S Ведущая','',0,17200,NULL,'{\"height\":\"80\",\"diameter\":\"22.5\",\"width\":\"295\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"SATOYA\",\"used\":\"0\",\"1cid\":\"\"}','catalog/satoya-sd-064-ad85c5fd02.jpg','sd-064-tl-pr18-152149-m-ms',1506589040,1),(113,5,'SF-042 TL PR18 152/149 M M+S Рулевая','',0,16650,NULL,'{\"height\":\"80\",\"diameter\":\"22.5\",\"width\":\"295\",\"axle\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"SATOYA\",\"used\":\"0\",\"1cid\":\"\"}','catalog/satoya-sf-042-c21e0cc99e.jpg','sf-042-tl-pr18-152149-m-ms',1506589161,1),(114,5,'SD-062 TL PR20 154/150 L M+S Ведущая','',0,17900,NULL,'{\"height\":\"70\",\"diameter\":\"22.5\",\"width\":\"315\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"SATOYA\",\"used\":\"0\",\"1cid\":\"\"}','catalog/satoya-sd-062-a05bcaefc0.jpg','sd-062-tl-pr20-154150-l-ms',1506589287,1),(115,5,'SF-042 TL PR20 154/150 L Рулевая','',0,16800,NULL,'{\"height\":\"70\",\"diameter\":\"22.5\",\"width\":\"315\",\"axle\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"SATOYA\",\"used\":\"0\",\"1cid\":\"\"}','catalog/satoya-sf-042-5d9331b97d.jpg','sf-042-tl-pr20-154150-l',1506589526,1),(116,5,'SD-066 II TL PR20 157/153 L Строительная M+S Ведущая','',0,17300,NULL,'{\"height\":\"80\",\"diameter\":\"22.5\",\"width\":\"315\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\",\"\\u041a\\u0430\\u0440\\u044c\\u0435\\u0440\\u043d\\u044b\\u0439\",\"\\u0423\\u0441\\u0438\\u043b\\u0435\\u043d\\u043d\\u044b\\u0439\",\"\\u0411\\u0435\\u0437\\u0434\\u043e\\u0440\\u043e\\u0436\\u044c\\u0435\"],\"brand\":\"SATOYA\",\"used\":\"0\",\"1cid\":\"\"}','catalog/satoya-66-4244440d80.png','sd-066-ii-tl-pr20-157153-l-ms',1506589651,1),(117,5,'ST-082 TL PR20 160 K M+S Прицепная','',0,17100,NULL,'{\"height\":\"65\",\"diameter\":\"22.5\",\"width\":\"385\",\"axle\":[\"\\u041f\\u0440\\u0438\\u0446\\u0435\\u043f\\u043d\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"SATOYA\",\"popular\":[\"\\u0414\\u0430\"],\"used\":\"0\",\"1cid\":\"\"}','catalog/satoya-st-082-eaf255760e.jpg','st-082-tl-pr20-160-k-ms',1506589751,1),(118,5,'ST-084 TL PR20 160 K Прицепная','',0,17100,NULL,'{\"height\":\"65\",\"diameter\":\"22.5\",\"width\":\"385\",\"axle\":[\"\\u041f\\u0440\\u0438\\u0446\\u0435\\u043f\\u043d\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"SATOYA\",\"used\":\"0\",\"1cid\":\"\"}','catalog/satoya-st-084-9c4e28f313.jpg','st-084-tl-pr20-160-k',1506589874,1),(119,5,'SU-022 TT PR14 122/118 K Универсальная','',0,8300,NULL,'{\"height\":\"\",\"diameter\":\"16\",\"width\":\"7.50\",\"axle\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\",\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\",\"\\u0423\\u043d\\u0438\\u0432\\u0435\\u0440\\u0441\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"SATOYA\",\"used\":\"0\",\"1cid\":\"\"}','catalog/satoya-su-022-91e64a4e4d.jpg','su-022-tt-pr14-122118-k',1506589999,1),(120,5,'SU-022 TT PR16 128/124 K Универсальная','',0,9350,NULL,'{\"height\":\"\",\"diameter\":\"16\",\"width\":\"8.25\",\"axle\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\",\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\",\"\\u0423\\u043d\\u0438\\u0432\\u0435\\u0440\\u0441\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"SATOYA\",\"used\":\"0\",\"1cid\":\"\"}','catalog/satoya-su-022-2603330673.jpg','su-022-tt-pr16-128124-k',1506590178,1),(121,5,'ARMORSTEEL KMT TL 160/158 K Строительная M+S Прицепная','',0,21000,NULL,'{\"height\":\"65\",\"diameter\":\"22.5\",\"width\":\"385\",\"axle\":[\"\\u041f\\u0440\\u0438\\u0446\\u0435\\u043f\\u043d\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\",\"\\u041a\\u0430\\u0440\\u044c\\u0435\\u0440\\u043d\\u044b\\u0439\"],\"brand\":\"KELLY\",\"popular\":[\"\\u0414\\u0430\"],\"used\":\"0\",\"1cid\":\"\"}','catalog/kelly-38565r225-armorsteel-kmt-8611992b1c.jpg','armorsteel-kmt-tl-160158-k-ms',1506590297,1),(122,5,'HN 306 TL PR16 148/144 M Ведущая M+S','',0,12900,NULL,'{\"height\":\"\",\"diameter\":\"22.5\",\"width\":\"11\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"AEOLUS\",\"used\":\"0\",\"1cid\":\"\"}','catalog/aeolus-hn-306-fde62538c4.jpg','hn-306-tl-pr16-148144-m-ms',1506590461,1),(123,5,'HN 355 TL PR16 146/143 M Ведущая M+S','',0,16300,NULL,'{\"height\":\"\",\"diameter\":\"22.5\",\"width\":\"11\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"AEOLUS\",\"popular\":[\"\\u0414\\u0430\"],\"used\":\"0\",\"1cid\":\"\"}','catalog/aeolushn355-b66e083d61.jpg','hn-355-tl-pr16-146143-m-ms',1506592223,1),(124,5,'HN 08 TT PR18 152/149 K Универсальная','',0,17100,NULL,'{\"height\":\"\",\"diameter\":\"20\",\"width\":\"11\",\"axle\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\",\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\",\"\\u0423\\u043d\\u0438\\u0432\\u0435\\u0440\\u0441\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"AEOLUS\",\"used\":\"0\",\"1cid\":\"\"}','catalog/aeolus-hn-08-c660084d33.jpg','hn-08-tt-pr18-152149-k',1506592469,1),(125,5,'HN 306 TT PR18 152/149 K Ведущая M+S','',0,18200,NULL,'{\"height\":\"\",\"diameter\":\"20\",\"width\":\"11\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"AEOLUS\",\"used\":\"0\",\"1cid\":\"\"}','catalog/aeolus-hn-306-a3f4ce5e25.jpg','hn-306-tt-pr18-152149-k-ms',1506592576,1),(126,5,'HN 355 TL PR18 152/149 M Ведущая M+S','',0,19800,NULL,'{\"height\":\"\",\"diameter\":\"22.5\",\"width\":\"12\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"AEOLUS\",\"used\":\"0\",\"1cid\":\"\"}','catalog/aeolushn355-a8d6674b83.jpg','hn-355-tl-pr18-152149-m-ms',1506592776,1),(127,5,'HN 08 TT PR18 154/151 K Универсальная M+S','',0,19900,NULL,'{\"height\":\"\",\"diameter\":\"20\",\"width\":\"12\",\"axle\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\",\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\",\"\\u0423\\u043d\\u0438\\u0432\\u0435\\u0440\\u0441\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"AEOLUS\",\"used\":\"0\",\"1cid\":\"\"}','catalog/aeolus-hn-08-61f3a0ac3c.jpg','hn-08-tt-pr18-154151-k-ms',1506592883,1),(128,5,'HN 355 TL PR16 140/138 M Ведущая','',0,12600,NULL,'{\"height\":\"70\",\"diameter\":\"19.5\",\"width\":\"265\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"AEOLUS\",\"used\":\"0\",\"1cid\":\"\"}','catalog/aeolushn355-62efd3bee2.jpg','hn-355-tl-pr16-140138-m',1506592998,1),(129,5,'ASL33 TL PR18 152/149 M Рулевая/Прицепная M+S','',0,14500,NULL,'{\"height\":\"60\",\"diameter\":\"22.5\",\"width\":\"295\",\"axle\":[\"\\u0420\\u0443\\u043b\\u0435\\u0432\\u0430\\u044f\",\"\\u041f\\u0440\\u0438\\u0446\\u0435\\u043f\\u043d\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"AEOLUS\",\"used\":\"0\",\"1cid\":\"\"}','catalog/aeolus-asl33-6504803523.jpg','asl33-tl-pr18-152149-m-ms',1506593535,1),(130,7,'ASTERRO 11.75x22.5/10x335/281/135 (рулевой)','',2,5800,NULL,'{\"diameter\":\"22.5\",\"width\":\"11.75\",\"fixture\":\"10\",\"pcd\":\"335\",\"dia\":\"281\",\"et\":\"135\",\"type\":[\"\\u0428\\u0442\\u0430\\u043c\\u043f\\u043e\\u0432\\u0430\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"ASTERRO\",\"used\":\"0\",\"1cid\":\"\"}','catalog/1175225-58e8b96115.jpg','asterro-1175x22510x335281135',1506937613,1),(131,5,'NOKTOP 68 восстановленная, на каркасе BRIDGESTONE','',4,14000,NULL,'{\"height\":\"70\",\"diameter\":\"22.5\",\"width\":\"315\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\",\"\\u041a\\u0430\\u0440\\u044c\\u0435\\u0440\\u043d\\u044b\\u0439\",\"\\u0423\\u0441\\u0438\\u043b\\u0435\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"NOKIAN\",\"used\":\"0\",\"1cid\":\"\"}','catalog/noktop-68-de8ffeb0bf.jpg','noktop-68-bridgestone',1506597199,1),(133,7,'SANT 6.00х17.5 6х222.25/164/135','<p><span style=\"color: rgb(119, 119, 119); font-family: &quot;Roboto Condensed&quot;; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;\">This is when online shops come in handy. You no longer need to wander around the town in search of the necessary car parts since all this can be found on our website. Join the club and bring wheels & tyres shopping to the whole new level. We keep track of the latest web design trends and try hard to keep our estore up-to-date and modern. It is designed in such a way that nothing will distract you from the items worth your attention.</span><span id=\"selection-marker-1\" class=\"redactor-selection-marker\"></span><span id=\"selection-marker-1\" class=\"redactor-selection-marker\"></span></p>',1,3700,NULL,'{\"diameter\":\"17.5\",\"width\":\"6.00\",\"fixture\":\"6\",\"pcd\":\"222,25\",\"dia\":\"164\",\"et\":\"135\",\"type\":[\"\\u0428\\u0442\\u0430\\u043c\\u043f\\u043e\\u0432\\u0430\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"SANT\",\"used\":\"0\",\"1cid\":\"\"}','catalog/600175-18-6205161115-0ac3ae62dc.jpg','sant-600175-622225164135',1506935543,1),(146,5,'NOKTOP 64 шина грузовая восстановленная','',4,9900,NULL,'{\"height\":\"70\",\"diameter\":\"22.5\",\"width\":\"305\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"NOKIAN\",\"used\":\"0\",\"1cid\":\"\"}','catalog/noktop-64-48c1a33a11.jpg','noktop-64',1506945493,1),(147,5,'NOKTOP 64 шина грузовая восстановленная','',4,12000,NULL,'{\"height\":\"60\",\"diameter\":\"22.5\",\"width\":\"315\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"NOKIAN\",\"used\":\"0\",\"1cid\":\"\"}','catalog/noktop-64-881a6b9855.jpg','noktop-64-2',1506945633,1),(148,5,'NOKTOP 68 шина грузовая восстановленная на каркасе BRIDGESTONE','',4,14000,NULL,'{\"height\":\"70\",\"diameter\":\"22.5\",\"width\":\"315\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u041a\\u0430\\u0440\\u044c\\u0435\\u0440\\u043d\\u044b\\u0439\"],\"brand\":\"NOKIAN\",\"used\":\"0\",\"1cid\":\"\"}','catalog/noktop-68-e5292dd8cd.jpg','noktop-68-bridgestone-2',1506945943,1),(149,5,'NOKTOP 72 шина грузовая восстановленная на каркасе BRIDGESTONE','',1,14000,NULL,'{\"height\":\"70\",\"diameter\":\"22.5\",\"width\":\"315\",\"axle\":[\"\\u041f\\u0440\\u0438\\u0446\\u0435\\u043f\\u043d\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"NOKIAN\",\"used\":\"0\",\"1cid\":\"\"}','catalog/noktop-72-b383da9582.jpg','noktop-72-bridgestone',1506945759,1),(150,5,'NOKTOP HKPL E шина грузовая восстановленная на каркасе КАМА','',19,11500,NULL,'{\"height\":\"70\",\"diameter\":\"22.5\",\"width\":\"315\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0417\\u0438\\u043c\\u043d\\u0438\\u0439\"],\"brand\":\"NOKIAN\",\"used\":\"0\",\"1cid\":\"\"}','catalog/hkpl-fb19180efb.jpg','noktop-hkpl-e',1506946029,1),(151,5,'NOKTOP HKPL E шина грузовая восстановленная на каркасе BRIDGESTONE','',4,14000,NULL,'{\"height\":\"70\",\"diameter\":\"22.5\",\"width\":\"315\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0417\\u0438\\u043c\\u043d\\u0438\\u0439\"],\"brand\":\"NOKIAN\",\"used\":\"0\",\"1cid\":\"\"}','catalog/hkpl-fb19180efb.jpg','noktop-hkpl-e-bridgestone',1506946199,1),(152,5,'NOKTOP HKPL E шина грузовая восстановленная на каркасе MICHELIN','',4,15000,NULL,'{\"height\":\"70\",\"diameter\":\"22.5\",\"width\":\"315\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0417\\u0438\\u043c\\u043d\\u0438\\u0439\"],\"brand\":\"NOKIAN\",\"used\":\"0\",\"1cid\":\"\"}','catalog/hkpl-fb19180efb.jpg','noktop-hkpl-e-michelin',1506946266,1),(153,7,'Normaks 11,75х22,5 10/335/281/0','',12,4500,NULL,'{\"diameter\":\"22.5\",\"width\":\"11.75\",\"fixture\":\"10\",\"pcd\":\"335\",\"dia\":\"281\",\"et\":\"\",\"type\":[\"\\u0428\\u0442\\u0430\\u043c\\u043f\\u043e\\u0432\\u0430\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"\",\"used\":\"0\",\"1cid\":\"\"}','catalog/1175225-0-1e963beaee.jpg','normaks-1175225-103352810',1506937034,1),(154,4,'FI 140 н.с.12 инд.157 спецшина','',0,29700,NULL,'{\"height\":\"\",\"diameter\":\"24\",\"width\":\"16\",\"axle\":[\"\\u0423\\u043d\\u0438\\u0432\\u0435\\u0440\\u0441\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f\"],\"type\":[\"\\u0411\\u0435\\u0437\\u0434\\u043e\\u0440\\u043e\\u0436\\u044c\\u0435\"],\"brand\":\"\\u0410\\u043b\\u0442\\u0430\\u0439\\u0448\\u0438\\u043d\\u0430\",\"used\":\"0\",\"1cid\":\"\"}','catalog/fi-140-9846f39b9f.jpg','fi-140-12-157',1507029898,1),(155,4,'FI 140 н.с.24 инд.171 спецшина','',0,44200,NULL,'{\"height\":\"\",\"diameter\":\"24\",\"width\":\"16\",\"axle\":[\"\\u0423\\u043d\\u0438\\u0432\\u0435\\u0440\\u0441\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f\"],\"type\":[\"\\u0411\\u0435\\u0437\\u0434\\u043e\\u0440\\u043e\\u0436\\u044c\\u0435\"],\"brand\":\"\\u0410\\u043b\\u0442\\u0430\\u0439\\u0448\\u0438\\u043d\\u0430\",\"used\":\"0\",\"1cid\":\"\"}','catalog/fi-140-9846f39b9f.jpg','fi-140-24-171',1507030045,1),(156,4,'NorTec GD-106 н.с.16 инд.153 спецшина','',0,21850,NULL,'{\"height\":\"\",\"diameter\":\"24\",\"width\":\"14\",\"axle\":[\"\\u0423\\u043d\\u0438\\u0432\\u0435\\u0440\\u0441\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f\"],\"type\":[\"\\u0411\\u0435\\u0437\\u0434\\u043e\\u0440\\u043e\\u0436\\u044c\\u0435\"],\"brand\":\"\\u0410\\u043b\\u0442\\u0430\\u0439\\u0448\\u0438\\u043d\\u0430\",\"used\":\"0\",\"1cid\":\"\"}','catalog/nortecgd-106-1aa63cbd50.jpg','nortec-gd-106-16-153',1507030196,1),(158,4,'Nortec TC-106 инд.138/125 б/к спецшина','',0,9500,NULL,'{\"height\":\"80\",\"diameter\":\"18\",\"width\":\"12.5\",\"axle\":[\"\\u0423\\u043d\\u0438\\u0432\\u0435\\u0440\\u0441\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f\"],\"type\":[\"\\u041a\\u0430\\u0440\\u044c\\u0435\\u0440\\u043d\\u044b\\u0439\",\"\\u0411\\u0435\\u0437\\u0434\\u043e\\u0440\\u043e\\u0436\\u044c\\u0435\"],\"brand\":\"\\u0410\\u043b\\u0442\\u0430\\u0439\\u0448\\u0438\\u043d\\u0430\",\"used\":\"0\",\"1cid\":\"\"}','catalog/12580-18-nortec-tc-106-aaa2cb4386.jpg','nortec-tc-106-138125',1507101820,1),(159,4,'NORTEC GD-113 норма слойности 16','',0,17100,NULL,'{\"height\":\"\",\"diameter\":\"20\",\"width\":\"14.00\",\"axle\":[\"\\u0423\\u043d\\u0438\\u0432\\u0435\\u0440\\u0441\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f\"],\"type\":[\"\\u0423\\u0441\\u0438\\u043b\\u0435\\u043d\\u043d\\u044b\\u0439\",\"\\u0411\\u0435\\u0437\\u0434\\u043e\\u0440\\u043e\\u0436\\u044c\\u0435\"],\"brand\":\"\\u0410\\u043b\\u0442\\u0430\\u0439\\u0448\\u0438\\u043d\\u0430\",\"used\":\"0\",\"1cid\":\"\"}','catalog/nortec-gd-113-bd63a3f706.jpg','nortec-gd-113-16',1507102237,1),(160,4,'NORTEC GD-113 норма слойности 10','',0,16300,NULL,'{\"height\":\"\",\"diameter\":\"20\",\"width\":\"14.00\",\"axle\":[\"\\u0423\\u043d\\u0438\\u0432\\u0435\\u0440\\u0441\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f\"],\"type\":[\"\\u0411\\u0435\\u0437\\u0434\\u043e\\u0440\\u043e\\u0436\\u044c\\u0435\"],\"brand\":\"\\u0410\\u043b\\u0442\\u0430\\u0439\\u0448\\u0438\\u043d\\u0430\",\"used\":\"0\",\"1cid\":\"\"}','catalog/nortec-gd-113-bd63a3f706.jpg','nortec-gd-113-10',1507102406,1),(161,4,'NORTEC ER-112','',0,16950,NULL,'{\"height\":\"\",\"diameter\":\"20\",\"width\":\"12\",\"axle\":[\"\\u0423\\u043d\\u0438\\u0432\\u0435\\u0440\\u0441\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f\"],\"type\":[\"\\u0423\\u0441\\u0438\\u043b\\u0435\\u043d\\u043d\\u044b\\u0439\",\"\\u0411\\u0435\\u0437\\u0434\\u043e\\u0440\\u043e\\u0436\\u044c\\u0435\"],\"brand\":\"\\u0410\\u043b\\u0442\\u0430\\u0439\\u0448\\u0438\\u043d\\u0430\",\"used\":\"0\",\"1cid\":\"\"}','catalog/nortec-er-112-5d9246f242.jpg','nortec-er-112',1507102701,1),(162,6,'7 Шипы','',0,2580,NULL,'{\"height\":\"65\",\"diameter\":\"14\",\"width\":\"155\",\"type\":[\"\\u0417\\u0438\\u043c\\u043d\\u0438\\u0439\"],\"brand\":\"NORDMAN\",\"used\":\"0\",\"1cid\":\"\"}','catalog/nordman-7-80074107b7.jpg','7',1507103104,1),(163,6,'5 XL Шипы','',0,4200,NULL,'{\"height\":\"55\",\"diameter\":\"15\",\"width\":\"185\",\"type\":[\"\\u0417\\u0438\\u043c\\u043d\\u0438\\u0439\"],\"brand\":\"NORDMAN\",\"used\":\"0\",\"1cid\":\"\"}','catalog/nordman-5-2111a610c3.jpg','5-xl',1507103982,1),(164,5,'NOKTOP 64 шина грузовая восстановленная','',4,12000,NULL,'{\"height\":\"60\",\"diameter\":\"22.5\",\"width\":\"315\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"NOKIAN\",\"used\":\"0\",\"1cid\":\"\"}','catalog/noktop-64-693f80fbff.jpg','noktop-64-3',1507105631,1),(165,5,'NOKTOP 64 шина грузовая восстановленная на каркасе российского пр-ва','',1,11500,NULL,'{\"height\":\"70\",\"diameter\":\"22.5\",\"width\":\"315\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"NOKIAN\",\"used\":\"0\",\"1cid\":\"\"}','catalog/noktop-64-693f80fbff.jpg','noktop-64-4',1507106122,1),(166,5,'NOKTOP 64 шина грузовая восстановленная на каркасе европейского пр-ва','',1,12700,NULL,'{\"height\":\"70\",\"diameter\":\"22.5\",\"width\":\"315\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"NOKIAN\",\"used\":\"0\",\"1cid\":\"\"}','catalog/noktop-64-693f80fbff.jpg','noktop-64-5',1507106221,1),(167,5,'NOKTOP 64 шина грузовая восстановленная на каркасе GoodYear','',1,13500,NULL,'{\"height\":\"70\",\"diameter\":\"22.5\",\"width\":\"315\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"NOKIAN\",\"used\":\"0\",\"1cid\":\"\"}','catalog/noktop-64-693f80fbff.jpg','noktop-64-goodyear',1507106284,1),(168,5,'NOKTOP 64 шина грузовая восстановленная на каркасе Bridgestone','',1,14000,NULL,'{\"height\":\"70\",\"diameter\":\"22.5\",\"width\":\"315\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"NOKIAN\",\"used\":\"0\",\"1cid\":\"\"}','catalog/noktop-64-693f80fbff.jpg','noktop-64-bridgestone',1507106322,1),(169,5,'NOKTOP 64 шина грузовая восстановленная на каркасе Michelin','',1,15000,NULL,'{\"height\":\"70\",\"diameter\":\"22.5\",\"width\":\"315\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"NOKIAN\",\"used\":\"0\",\"1cid\":\"\"}','catalog/noktop-64-693f80fbff.jpg','noktop-64-michelin',1507106353,1),(170,5,'Marangoni Budget Line BZA65S шина грузовая восстановленная на каркасе европейского пр-ва','',1,13500,NULL,'{\"height\":\"65\",\"diameter\":\"22.5\",\"width\":\"385\",\"axle\":[\"\\u041f\\u0440\\u0438\\u0446\\u0435\\u043f\\u043d\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"\",\"used\":\"0\",\"1cid\":\"\"}','catalog/satoya-st-084-d395467840.jpg','marangoni-budget-line-bza65s',1507109162,1),(171,5,'Marangoni Budget Line BZA65S шина грузовая восстановленная на каркасе GoodYear','',1,13900,NULL,'{\"height\":\"65\",\"diameter\":\"22.5\",\"width\":\"385\",\"axle\":[\"\\u041f\\u0440\\u0438\\u0446\\u0435\\u043f\\u043d\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"\",\"used\":\"0\",\"1cid\":\"\"}','catalog/satoya-st-084-3ebd609396.jpg','marangoni-budget-line-bza65s-goodyear',1507109343,1),(172,5,'Marangoni Budget Line BZA65S шина грузовая восстановленная на каркасе Bridgestone','',1,14200,NULL,'{\"height\":\"65\",\"diameter\":\"22.5\",\"width\":\"385\",\"axle\":[\"\\u041f\\u0440\\u0438\\u0446\\u0435\\u043f\\u043d\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"\",\"used\":\"0\",\"1cid\":\"\"}','catalog/satoya-st-084-4115a54bae.jpg','marangoni-budget-line-bza65s-bridgestone',1507109395,1),(173,5,'Marangoni Budget Line BZA65S шина грузовая восстановленная на каркасе Michelin','',1,15000,NULL,'{\"height\":\"65\",\"diameter\":\"22.5\",\"width\":\"385\",\"axle\":[\"\\u041f\\u0440\\u0438\\u0446\\u0435\\u043f\\u043d\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"\",\"used\":\"0\",\"1cid\":\"\"}','catalog/satoya-st-084-fe46f0dd6e.jpg','marangoni-budget-line-bza65s-michelin',1507109438,1),(174,5,'NOKTOP 72 шина грузовая восстановленная на каркасе Michelin','',1,15000,NULL,'{\"height\":\"65\",\"diameter\":\"22.5\",\"width\":\"385\",\"axle\":[\"\\u041f\\u0440\\u0438\\u0446\\u0435\\u043f\\u043d\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"NOKIAN\",\"used\":\"0\",\"1cid\":\"\"}','catalog/noktop-72-7bcde8c901.jpg','noktop-72-michelin',1507109679,1),(175,5,'NOKTOP 72 шина грузовая восстановленная на каркасе Bridgestone','',1,14200,NULL,'{\"height\":\"65\",\"diameter\":\"22.5\",\"width\":\"385\",\"axle\":[\"\\u041f\\u0440\\u0438\\u0446\\u0435\\u043f\\u043d\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"NOKIAN\",\"used\":\"0\",\"1cid\":\"\"}','catalog/noktop-72-c83bbfe89c.jpg','noktop-72-bridgestone-2',1507109613,1),(176,5,'NOKTOP 72 шина грузовая восстановленная на каркасе GoodYear','',1,13900,NULL,'{\"height\":\"65\",\"diameter\":\"22.5\",\"width\":\"385\",\"axle\":[\"\\u041f\\u0440\\u0438\\u0446\\u0435\\u043f\\u043d\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"NOKIAN\",\"used\":\"0\",\"1cid\":\"\"}','catalog/noktop-72-f5193a17d1.jpg','noktop-72-goodyear',1507109546,1),(177,5,'NOKTOP 72 шина грузовая восстановленная на каркасе европейского пр-ва','',1,13500,NULL,'{\"height\":\"65\",\"diameter\":\"22.5\",\"width\":\"385\",\"axle\":[\"\\u041f\\u0440\\u0438\\u0446\\u0435\\u043f\\u043d\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"NOKIAN\",\"used\":\"0\",\"1cid\":\"\"}','catalog/noktop-72-a73e74026a.jpg','noktop-72-2',1507109508,1),(178,5,'JINYU JT560 норма слойности 20','',1,22900,NULL,'{\"height\":\"45\",\"diameter\":\"19.5\",\"width\":\"445\",\"axle\":[\"\\u041f\\u0440\\u0438\\u0446\\u0435\\u043f\\u043d\\u0430\\u044f\"],\"type\":[\"\\u041b\\u0435\\u0442\\u043d\\u0438\\u0439\"],\"brand\":\"\",\"used\":\"0\",\"1cid\":\"\"}','catalog/jinyu-53088a6a4b.png','jinyu-jt560-20',1507113275,1),(180,5,'Midas mml-2 шина грузовая восстановленная на каркасе российского пр-ва','',3,12000,NULL,'{\"height\":\"80\",\"diameter\":\"22.5\",\"width\":\"295\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"\",\"used\":\"0\",\"1cid\":\"\"}','catalog/10-c8595f07d7.jpg','midas-mml-2',1507111809,1),(181,5,'Midas mml-2 шина грузовая восстановленная на каркасе европейского пр-ва','',3,12500,NULL,'{\"height\":\"80\",\"diameter\":\"22.5\",\"width\":\"295\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"\",\"used\":\"0\",\"1cid\":\"\"}','catalog/10-c8595f07d7.jpg','midas-mml-2-2',1507112167,1),(182,5,'Marangoni UD2L шина грузовая восстановленная на каркасе пр-ва Китай','',4,10200,NULL,'{\"height\":\"75\",\"diameter\":\"22.5\",\"width\":\"295\",\"axle\":[\"\\u0412\\u0435\\u0434\\u0443\\u0449\\u0430\\u044f\"],\"type\":[\"\\u0412\\u0441\\u0435\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0439\"],\"brand\":\"\",\"used\":\"0\",\"1cid\":\"\"}','catalog/10-5d70a44b46.jpg','marangoni-ud2l',1507112268,1);
/*!40000 ALTER TABLE `cms_catalog_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_entity_categories`
--

DROP TABLE IF EXISTS `cms_entity_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_entity_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `image_file` varchar(128) DEFAULT NULL,
  `fields` text NOT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `cache` tinyint(1) NOT NULL DEFAULT '1',
  `tree` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `order_num` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_entity_categories`
--

LOCK TABLES `cms_entity_categories` WRITE;
/*!40000 ALTER TABLE `cms_entity_categories` DISABLE KEYS */;
INSERT INTO `cms_entity_categories` VALUES (1,'Транспортные компании','',NULL,'[{\"name\":\"logo\",\"title\":\"\\u041b\\u043e\\u0433\\u043e\\u0442\\u0438\\u043f\",\"type\":\"file\",\"options\":\"\"},{\"name\":\"url\",\"title\":\"\\u0421\\u0441\\u044b\\u043b\\u043a\\u0430 \\u043d\\u0430 \\u0441\\u0430\\u0439\\u0442\",\"type\":\"string\",\"options\":\"\"}]','transportnye-kompanii',1,1,1,2,0,1,1),(17,'Мы готовы предложить вам','',NULL,'{}','features',1,17,1,2,0,6,1),(16,'Адрес','',NULL,'[{\"name\":\"address-production\",\"title\":\"\\u0410\\u0434\\u0440\\u0435\\u0441 \\u043f\\u0440\\u043e\\u0438\\u0437\\u0432\\u043e\\u0434\\u0441\\u0442\\u0432\\u0430\",\"type\":\"string\",\"options\":\"\"},{\"name\":\"address-office\",\"title\":\"\\u041e\\u0444\\u0438\\u0441\",\"type\":\"string\",\"options\":\"\"},{\"name\":\"map\",\"title\":\"\\u041a\\u0430\\u0440\\u0442\\u0430\",\"type\":\"address\",\"options\":\"\"}]','address',1,16,1,2,0,5,1);
/*!40000 ALTER TABLE `cms_entity_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_entity_items`
--

DROP TABLE IF EXISTS `cms_entity_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_entity_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `data` text NOT NULL,
  `order_num` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_entity_items`
--

LOCK TABLES `cms_entity_items` WRITE;
/*!40000 ALTER TABLE `cms_entity_items` DISABLE KEYS */;
INSERT INTO `cms_entity_items` VALUES (1,1,'Delko','{\"url\":\"http:\\/\\/del-ko.ru\\/\",\"logo\":\"entity\\/delko.png\"}',1,1),(2,1,'DHL','{\"url\":\"http:\\/\\/www.dhl.ru\",\"logo\":\"entity\\/dhl.png\"}',2,1),(3,1,'Деловые линии','{\"url\":\"http:\\/\\/www.dellin.ru\\/\",\"logo\":\"entity\\/dl.png\"}',3,1),(4,1,'DPD','{\"url\":\"http:\\/\\/www.dpd.ru\\/\",\"logo\":\"entity\\/dpd.png\"}',4,1),(6,17,'поставки любых объемов, как разовые, так и регулярные','{}',7,1),(7,17,'собственная лабаратория и три этапа контроля качества','{}',8,1),(8,17,'клей и химическое сырье премиум-качества напрямую от производителя','{}',9,1),(9,17,'доставка в любую точку России','{}',6,1),(14,16,'Адрес','{\"address-production\":\"143960, \\u041c\\u043e\\u0441\\u043a\\u043e\\u0432\\u0441\\u043a\\u0430\\u044f \\u043e\\u0431\\u043b., \\u0420\\u0435\\u0443\\u0442\\u043e\\u0432, \\u043f\\u0440. \\u041c\\u0438\\u0440\\u0430, \\u0434\\u043e\\u043c 69\",\"address-office\":\"105568, \\u041c\\u043e\\u0441\\u043a\\u0432\\u0430, \\u0443\\u043b. \\u0427\\u0435\\u043b\\u044f\\u0431\\u0438\\u043d\\u0441\\u043a\\u0430\\u044f, \\u0434\\u043e\\u043c 19, \\u043a\\u043e\\u0440\\u043f\\u0443\\u0441 4, \\u043e\\u0444\\u0438\\u0441 3\",\"map\":\"\\u0427\\u0435\\u043b\\u044f\\u0431\\u0438\\u043d\\u0441\\u043a\\u0430\\u044f \\u0443\\u043b., 19\\u043a4, \\u041c\\u043e\\u0441\\u043a\\u0432\\u0430, \\u0420\\u043e\\u0441\\u0441\\u0438\\u044f, 105568#ChIJzXKL9yLLSkER0WHSwiK7QtQ#55.780194#37.828643000000056\"}',14,1);
/*!40000 ALTER TABLE `cms_entity_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_faq`
--

DROP TABLE IF EXISTS `cms_faq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_faq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `order_num` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_faq`
--

LOCK TABLES `cms_faq` WRITE;
/*!40000 ALTER TABLE `cms_faq` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_faq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_feedback`
--

DROP TABLE IF EXISTS `cms_feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `title` varchar(128) DEFAULT NULL,
  `text` text NOT NULL,
  `answer_subject` varchar(128) DEFAULT NULL,
  `answer_text` text,
  `time` int(11) DEFAULT '0',
  `ip` varchar(16) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_feedback`
--

LOCK TABLES `cms_feedback` WRITE;
/*!40000 ALTER TABLE `cms_feedback` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_files`
--

DROP TABLE IF EXISTS `cms_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `file` varchar(255) NOT NULL,
  `size` int(11) DEFAULT '0',
  `slug` varchar(128) DEFAULT NULL,
  `downloads` int(11) DEFAULT '0',
  `time` int(11) DEFAULT '0',
  `order_num` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_files`
--

LOCK TABLES `cms_files` WRITE;
/*!40000 ALTER TABLE `cms_files` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_gallery_categories`
--

DROP TABLE IF EXISTS `cms_gallery_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_gallery_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `image_file` varchar(128) DEFAULT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `tree` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `order_num` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_gallery_categories`
--

LOCK TABLES `cms_gallery_categories` WRITE;
/*!40000 ALTER TABLE `cms_gallery_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_gallery_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_guestbook`
--

DROP TABLE IF EXISTS `cms_guestbook`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_guestbook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `title` varchar(128) DEFAULT NULL,
  `text` text NOT NULL,
  `answer` text,
  `email` varchar(128) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `ip` varchar(16) DEFAULT NULL,
  `new` tinyint(1) DEFAULT '0',
  `status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_guestbook`
--

LOCK TABLES `cms_guestbook` WRITE;
/*!40000 ALTER TABLE `cms_guestbook` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_guestbook` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_loginform`
--

DROP TABLE IF EXISTS `cms_loginform`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_loginform` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `ip` varchar(16) DEFAULT NULL,
  `user_agent` varchar(1024) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `success` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=107 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_loginform`
--

LOCK TABLES `cms_loginform` WRITE;
/*!40000 ALTER TABLE `cms_loginform` DISABLE KEYS */;
INSERT INTO `cms_loginform` VALUES (1,'root','******','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0',1478894852,1),(2,'root','******','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0',1479144663,1),(3,'root','******','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0',1479403182,1),(4,'root','******','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1479540618,1),(5,'ilya','******','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1479540722,1),(6,'root','******','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1479541154,1),(7,'root','******','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1480263990,1),(8,'root','******','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1480269382,1),(9,'root','******','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1480440265,1),(10,'root','******','188.168.215.21','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1480629787,1),(11,'admin','123321','127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1480666010,0),(12,'root','******','127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1480666015,1),(13,'root','******','127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36',1480671575,1),(14,'admin','123321','127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1480679611,0),(15,'root','******','188.168.215.21','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1480774379,1),(16,'root','******','188.168.215.21','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1480882908,1),(17,'root','******','188.168.215.21','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',1481043006,1),(18,'admin','******','188.168.215.21','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',1481043058,1),(19,'root','******','188.168.215.21','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',1481043105,1),(20,'root','YSKYzhUL','188.168.215.21','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1481043461,0),(21,'root','******','188.168.215.21','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1481043467,1),(22,'root','YSKYzhUL','188.168.215.21','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1481043507,0),(23,'root','c812447c','188.168.215.21','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1481043513,0),(24,'root','c812447c','188.168.215.21','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1481043799,0),(25,'root','c812447c','188.168.215.21','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1481044664,0),(26,'root','c812447c','188.168.215.21','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1481044708,0),(27,'root','******','188.168.215.21','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1481044714,1),(28,'root','******','188.168.215.21','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1481044733,1),(29,'root','******','212.192.204.45','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36 OPR/41.0.2353.69',1481122633,1),(30,'root','******','212.192.204.45','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36 OPR/41.0.2353.69',1481196341,1),(31,'root','c812447c','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1481214897,0),(32,'root','******','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1481214923,1),(33,'root','******','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1481362248,1),(34,'root','******','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1481474456,1),(35,'root','******','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1484166775,1),(36,'root','******','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1484585177,1),(37,'root','******','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',1484851006,1),(38,'root','******','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0',1485892247,1),(39,'root','******','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0',1487439628,1),(40,'root','******','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0',1487618994,1),(41,'root','******','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0',1489172988,1),(42,'root','******','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0',1489340919,1),(43,'root','******','212.192.204.44','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',1505142140,1),(44,'root','******','178.155.4.210','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/603.3.8 (KHTML, like Gecko) Version/10.1.2 Safari/603.3.8',1505166763,1),(45,'root','******','127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:53.0) Gecko/20100101 Firefox/53.0',1505218870,1),(46,'root','******','212.192.204.44','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:53.0) Gecko/20100101 Firefox/53.0',1505285733,1),(47,'root','******','212.192.204.44','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:53.0) Gecko/20100101 Firefox/53.0',1505468719,1),(48,'root','******','127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:53.0) Gecko/20100101 Firefox/53.0',1505480082,1),(49,'root','YSKYzhUL','127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',1505638151,0),(50,'root','******','127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',1505638186,1),(51,'root','******','195.151.120.105','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.125 YaBrowser/17.7.1.791 Yowser/2.5 Safari/537.36',1505648015,1),(52,'Root','kuntel82','217.118.81.24','Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_3 like Mac OS X) AppleWebKit/603.3.8 (KHTML, like Gecko) Version/10.0 Mobile/14G60 Safari/602.1',1505649575,0),(53,'root','kuntel82','217.118.81.24','Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_3 like Mac OS X) AppleWebKit/603.3.8 (KHTML, like Gecko) Version/10.0 Mobile/14G60 Safari/602.1',1505649583,0),(54,'root','******','217.118.81.24','Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_3 like Mac OS X) AppleWebKit/603.3.8 (KHTML, like Gecko) Version/10.0 Mobile/14G60 Safari/602.1',1505649606,1),(55,'root','******','195.151.120.105','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063',1505745261,1),(56,'root','******','195.151.120.105','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',1505745433,1),(57,'root','******','178.76.227.100','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',1505808912,1),(58,'root','******','212.192.204.44','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',1505822567,1),(59,'root','******','212.192.204.44','Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:55.0) Gecko/20100101 Firefox/55.0',1505822787,1),(60,'root','******','178.76.227.100','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',1505898517,1),(61,'root','******','217.118.81.25','Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_3 like Mac OS X) AppleWebKit/603.3.8 (KHTML, like Gecko) Version/10.0 Mobile/14G60 Safari/602.1',1505900102,1),(62,'root','******','178.76.227.100','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',1505901106,1),(63,'root','******','178.76.227.100','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',1505907684,1),(64,'root','******','127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:53.0) Gecko/20100101 Firefox/53.0',1505915098,1),(65,'root','******','195.151.120.105','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063',1505915622,1),(66,'root','******','212.192.204.44','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:53.0) Gecko/20100101 Firefox/53.0',1505916202,1),(67,'root','******','212.192.204.44','Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:55.0) Gecko/20100101 Firefox/55.0',1506004686,1),(68,'root','******','212.192.204.44','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:53.0) Gecko/20100101 Firefox/53.0',1506006769,1),(69,'root','******','195.151.120.105','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',1506329929,1),(70,'root','******','195.151.120.105','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063',1506329955,1),(71,'root','******','127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:53.0) Gecko/20100101 Firefox/53.0',1506333433,1),(72,'root','******','212.192.204.44','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:53.0) Gecko/20100101 Firefox/53.0',1506334201,1),(73,'root','******','195.151.120.105','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',1506334867,1),(74,'root','******','212.192.204.44','Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:55.0) Gecko/20100101 Firefox/55.0',1506336094,1),(75,'root','******','127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',1506339339,1),(76,'root','******','195.151.120.105','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',1506339501,1),(77,'root','******','195.151.120.105','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',1506411840,1),(78,'root','******','195.151.120.105','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063',1506594072,1),(79,'root','******','127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:53.0) Gecko/20100101 Firefox/53.0',1506597799,1),(80,'root','******','195.151.120.105','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',1506606030,1),(81,'root','******','212.192.204.44','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:53.0) Gecko/20100101 Firefox/53.0',1506690429,1),(82,'root','kumel83','127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',1506835811,0),(83,'root','******','127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',1506835857,1),(84,'root','******','195.151.120.105','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',1506926327,1),(85,'root','******','195.151.120.105','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063',1506935622,1),(86,'root','******','212.192.204.44','Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:56.0) Gecko/20100101 Firefox/56.0',1506942454,1),(87,'root','******','127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',1506945887,1),(88,'root','******','195.151.120.105','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',1507012885,1),(89,'root','******','127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',1507016833,1),(90,'root','******','127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',1507049812,1),(91,'root','******','127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',1507099114,1),(92,'root','******','127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:56.0) Gecko/20100101 Firefox/56.0',1507104901,1),(93,'root','******','127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:56.0) Gecko/20100101 Firefox/56.0',1507107787,1),(94,'root','******','127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',1507110463,1),(95,'root','******','127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',1507111418,1),(96,'root','******','127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',1507181661,1),(97,'root','******','127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',1507181720,1),(98,'root','******','195.151.120.105','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',1507185714,1),(99,'root','******','212.192.204.44','Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:56.0) Gecko/20100101 Firefox/56.0',1507202041,1),(100,'root','******','127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:56.0) Gecko/20100101 Firefox/56.0',1507203669,1),(101,'root','******','127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',1507214405,1),(102,'root','******','95.153.131.157','Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:56.0) Gecko/20100101 Firefox/56.0',1507398782,1),(103,'root','******','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0',1507402889,1),(104,'root','******','188.168.215.10','Mozilla/5.0 (Windows NT 10.0; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0',1507403486,1),(105,'root','******','127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',1507406407,1),(106,'root','******','188.168.215.10','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',1507442722,1);
/*!40000 ALTER TABLE `cms_loginform` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_menu`
--

DROP TABLE IF EXISTS `cms_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(128) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `items` text,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`menu_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_menu`
--

LOCK TABLES `cms_menu` WRITE;
/*!40000 ALTER TABLE `cms_menu` DISABLE KEYS */;
INSERT INTO `cms_menu` VALUES (1,'main','Главное меню','[\n    {\n        \"label\": \"Оплата и доставка\",\n        \"url\": \"/oplata\"\n    },\n    {\n        \"label\": \"Контакты\",\n        \"url\": \"/kontakty\"\n    }\n]',1),(2,'catalog','Меню каталога','[\n    {\n        \"label\": \"Спецшины\",\n        \"url\": \"/catalog/spec-tyres\"\n    },\n    {\n        \"label\": \"Грузовые шины\",\n        \"url\": \"/catalog/truck-tyres\"\n    },\n    {\n        \"label\": \"Шины бывшие в употреблении\",\n        \"url\": \"/catalog/auto-tyres\"\n    },\n    {\n        \"label\": \"Колесные диски\",\n        \"url\": \"/catalog/wheels\"\n    },\n    {\n        \"label\": \"Шиномонтаж\",\n        \"url\": \"/uslugi-gruzovogo-shinomontazha\"\n    }\n]',1),(3,'footer','Футер','[\n    {\n        \"label\": \"Обмен и возврат\",\n        \"url\": \"/obmen-i-vozvrat\"\n    },\n    {\n        \"label\": \"Оплата и доставка\",\n        \"url\": \"/oplata\"\n    },\n    {\n        \"label\": \"Контакты\",\n        \"url\": \"/kontakty\"\n    },\n    {\n        \"label\": \"Пользовательское соглашение\",\n        \"url\": \"/polzovatelskoe-soglashenie\"\n    },\n    {\n        \"label\": \"Политика конфиденциальности\",\n        \"url\": \"/politika-konfidencialnosti\"\n    }\n]',1),(4,'shopcart','Меню корзины','[\n    {\n        \"label\": \"Шиномонтаж\",\n        \"url\": \"http://tyres.local/uslugi-gruzovogo-shinomontazha\"\n    },\n    {\n        \"label\": \"Обмен и возврат\",\n        \"url\": \"http://tyres.local/obmen-i-vozvrat\"\n    },\n    {\n        \"label\": \"Оплата и доставка\",\n        \"url\": \"http://tyres.local/oplata\"\n    },\n    {\n        \"label\": \"Контакты\",\n        \"url\": \"http://tyres.local/kontakty\"\n    },\n    {\n        \"label\": \"Пользовательское соглашение\",\n        \"url\": \"http://tyres.local/polzovatelskoe-soglashenie\"\n    },\n    {\n        \"label\": \"Политика конфиденциальности\",\n        \"url\": \"http://tyres.local/politika-konfidencialnosti\"\n    }\n]',1);
/*!40000 ALTER TABLE `cms_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_migration`
--

DROP TABLE IF EXISTS `cms_migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_migration`
--

LOCK TABLES `cms_migration` WRITE;
/*!40000 ALTER TABLE `cms_migration` DISABLE KEYS */;
INSERT INTO `cms_migration` VALUES ('m000000_000000_base',1478894849),('m000000_000000_install',1478894850),('m000009_100000_update',1478894851),('m000009_200000_update',1478894851),('m000009_200003_module_menu',1478894851);
/*!40000 ALTER TABLE `cms_migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_modules`
--

DROP TABLE IF EXISTS `cms_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `class` varchar(128) NOT NULL,
  `title` varchar(128) NOT NULL,
  `icon` varchar(32) DEFAULT NULL,
  `settings` text,
  `notice` int(11) DEFAULT '0',
  `order_num` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_modules`
--

LOCK TABLES `cms_modules` WRITE;
/*!40000 ALTER TABLE `cms_modules` DISABLE KEYS */;
INSERT INTO `cms_modules` VALUES (1,'entity','yii\\cms\\modules\\entity\\EntityModule','Контакты','asterisk','{\"categoryThumb\":true,\"categorySlugImmutable\":false,\"categoryDescription\":true,\"itemsInFolder\":false}',0,90,0),(2,'article','yii\\cms\\modules\\article\\ArticleModule','Статьи','pencil','{\"categoryThumb\":true,\"categorySlugImmutable\":false,\"categoryDescription\":true,\"articleThumb\":true,\"enablePhotos\":true,\"enableTags\":true,\"enableShort\":true,\"shortMaxLength\":255,\"itemsInFolder\":false,\"itemSlugImmutable\":false}',0,60,1),(3,'carousel','yii\\cms\\modules\\carousel\\CarouselModule','Карусель','picture','{\"enableTitle\":true,\"enableText\":true}',0,40,0),(4,'catalog','yii\\cms\\modules\\catalog\\CatalogModule','Каталог','list-alt','{\"categoryThumb\":true,\"categorySlugImmutable\":false,\"categoryDescription\":true,\"itemsInFolder\":false,\"itemThumb\":true,\"itemPhotos\":true,\"itemDescription\":true,\"itemSlugImmutable\":false}',0,95,1),(5,'faq','yii\\cms\\modules\\faq\\FaqModule','Вопросы и ответы','question-sign','{\"questionHtmlEditor\":true,\"answerHtmlEditor\":true,\"enableTags\":true}',0,45,0),(6,'feedback','yii\\cms\\modules\\feedback\\FeedbackModule','Обратная связь','earphone','{\"mailAdminOnNewFeedback\":true,\"subjectOnNewFeedback\":\"\\u041d\\u043e\\u0432\\u0430\\u044f \\u0437\\u0430\\u044f\\u0432\\u043a\\u0430 \\u0441 \\u0441\\u0430\\u0439\\u0442\\u0430\",\"templateOnNewFeedback\":\"@app\\/mail\\/new_feedback\",\"answerTemplate\":\"@cms\\/modules\\/feedback\\/mail\\/en\\/answer\",\"answerSubject\":\"Answer on your feedback message\",\"answerHeader\":\"Hello,\",\"answerFooter\":\"Best regards.\",\"telegramAdminOnNewFeedback\":true,\"telegramTemplateOnNewFeedback\":\"@app\\/telegram\\/new_feedback\",\"enableTitle\":false,\"enableEmail\":true,\"enablePhone\":true,\"enableText\":true,\"enableCaptcha\":false}',0,100,1),(7,'file','yii\\cms\\modules\\file\\FileModule','Файлы','floppy-disk','{\"slugImmutable\":false}',0,30,0),(8,'gallery','yii\\cms\\modules\\gallery\\GalleryModule','Фотогалерея','camera','{\"categoryThumb\":true,\"itemsInFolder\":false,\"categoryTags\":true,\"categorySlugImmutable\":false,\"categoryDescription\":true}',0,80,0),(9,'guestbook','yii\\cms\\modules\\guestbook\\GuestbookModule','Гостевая книга','book','{\"enableTitle\":false,\"enableEmail\":true,\"preModerate\":false,\"enableCaptcha\":false,\"mailAdminOnNewPost\":true,\"subjectOnNewPost\":\"New message in the guestbook.\",\"templateOnNewPost\":\"@cms\\/modules\\/guestbook\\/mail\\/en\\/new_post\",\"frontendGuestbookRoute\":\"\\/guestbook\",\"subjectNotifyUser\":\"Your post in the guestbook answered\",\"templateNotifyUser\":\"@cms\\/modules\\/guestbook\\/mail\\/en\\/notify_user\"}',0,70,0),(10,'menu','yii\\cms\\modules\\menu\\MenuModule','Меню','menu-hamburger','{\"slugImmutable\":false}',0,51,1),(11,'news','yii\\cms\\modules\\news\\NewsModule','Новости','bullhorn','{\"enableThumb\":true,\"enablePhotos\":true,\"enableShort\":true,\"shortMaxLength\":256,\"enableTags\":true,\"slugImmutable\":false}',0,65,0),(12,'page','yii\\cms\\modules\\page\\PageModule','Страницы','file','{\"slugImmutable\":true,\"defaultFields\":\"[]\"}',0,50,1),(13,'shopcart','yii\\cms\\modules\\shopcart\\ShopcartModule','Заказы','shopping-cart','{\"mailAdminOnNewOrder\":true,\"subjectOnNewOrder\":\"New order\",\"templateOnNewOrder\":\"@cms\\/modules\\/shopcart\\/mail\\/en\\/new_order\",\"subjectNotifyUser\":\"Your order status changed\",\"templateNotifyUser\":\"@cms\\/modules\\/shopcart\\/mail\\/en\\/notify_user\",\"frontendShopcartRoute\":\"\\/shopcart\\/order\",\"enablePhone\":true,\"enableEmail\":true}',0,120,1),(14,'subscribe','yii\\cms\\modules\\subscribe\\SubscribeModule','E-mail рассылка','envelope','[]',0,10,0),(15,'text','yii\\cms\\modules\\text\\TextModule','Текстовые блоки','font','[]',0,20,0);
/*!40000 ALTER TABLE `cms_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_news`
--

DROP TABLE IF EXISTS `cms_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `image_file` varchar(128) DEFAULT NULL,
  `short` varchar(1024) DEFAULT NULL,
  `text` text,
  `slug` varchar(128) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `views` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_news`
--

LOCK TABLES `cms_news` WRITE;
/*!40000 ALTER TABLE `cms_news` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_pages`
--

DROP TABLE IF EXISTS `cms_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `text` text,
  `slug` varchar(128) DEFAULT NULL,
  `show_in_menu` tinyint(1) DEFAULT '0',
  `fields` text,
  `data` text,
  `tree` int(11) DEFAULT '0',
  `lft` int(11) DEFAULT '0',
  `rgt` int(11) DEFAULT '0',
  `depth` int(11) DEFAULT '0',
  `order_num` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_pages`
--

LOCK TABLES `cms_pages` WRITE;
/*!40000 ALTER TABLE `cms_pages` DISABLE KEYS */;
INSERT INTO `cms_pages` VALUES (3,'Главная','','index',0,'[{\"name\":\"address\",\"title\":\"\\u0410\\u0434\\u0440\\u0435\\u0441\",\"type\":\"address\",\"options\":\"\"}]','{\"address\":\"\\u0443\\u043b. \\u0417\\u0430\\u043f\\u0430\\u0434\\u043d\\u0430\\u044f, 33, \\u0410\\u043a\\u0441\\u0430\\u0439, \\u0420\\u043e\\u0441\\u0442\\u043e\\u0432\\u0441\\u043a\\u0430\\u044f \\u043e\\u0431\\u043b., \\u0420\\u043e\\u0441\\u0441\\u0438\\u044f, 346720#ChIJNSY_6imx40ARvFztT1JO32U#47.2663761#39.85366190000002\"}',3,1,2,0,1,1),(5,'Контактная информация','<h2>Адрес</h2>\r\n<ul>\r\n	<li>Ростовская область</li>\r\n	<li>г. Аксай</li>\r\n	<li>ул. Западная, 33    (М4 в сторону Краснодара)</li>\r\n	<li>тел.  +7 (863) 333-20-90</li>\r\n	<li>e-mail: info@kord-master.ru</li>\r\n</ul>\r\n<h2>Схемы проезда</h2>\r\n<p><strong>1. По трассе М4 на Москву:</strong>\r\n</p>\r\n<ul>\r\n	<li>– доехать до выезда из Аксая, развернуться через Мегу (поворот на Краснодар).</li>\r\n	<li>– въехать в Аксай.</li>\r\n	<li>– проехать синий переходной мост.</li>\r\n	<li>– доехать до автомойки самообслуживания и сразу за ней повернуть направо.</li>\r\n</ul>\r\n<p><strong>2. По трассе М4 в сторону Краснодара:</strong>\r\n</p>\r\n<ul>\r\n	<li>– повернуть на Краснодар.</li>\r\n	<li>– въехать в Аксай.</li>\r\n	<li>– проехать синий переходной мост.</li>\r\n	<li>– доехать до автомойки самообслуживания и сразу за ней повернуть направо.<span></span></li>\r\n</ul>\r\n<h2>Реквизиты</h2>\r\n<p><strong>ООО ” ТД “КОРДМАСТЕР”</strong><br>\r\n	ИНН: 6102042184<br>\r\n	КПП: 610201001<br>\r\n	ОГРН: 1136181000625<br>\r\n	ОКПО: 12142646\r\n</p>\r\n<p>Юридический адрес: 346720, Ростовская обл, Аксайский р-н, Аксай г,<br>\r\n	Западная ул, дом № 33, помещение 1-2а\r\n</p>\r\n<p>Телефон: (863) 3332090\r\n</p>\r\n<p>e-mail: info@kord-master.ru\r\n</p></strong>','contact',0,'[{\"name\":\"address\",\"title\":\"\\u0410\\u0434\\u0440\\u0435\\u0441\",\"type\":\"address\",\"options\":\"\"}]','{\"address\":\"\\u0443\\u043b. \\u0417\\u0430\\u043f\\u0430\\u0434\\u043d\\u0430\\u044f, 33, \\u0410\\u043a\\u0441\\u0430\\u0439, \\u0420\\u043e\\u0441\\u0442\\u043e\\u0432\\u0441\\u043a\\u0430\\u044f \\u043e\\u0431\\u043b., \\u0420\\u043e\\u0441\\u0441\\u0438\\u044f, 346720#ChIJNSY_6imx40ARvFztT1JO32U#47.2663761#39.85366190000002\"}',5,1,2,0,2,1),(6,'Оформление заказа и доставка','<h2>Оформление заказов</h2><p>Обработка заказов производится ежедневно с 8.00 до 18.00.</p><p>После оформления заказа менеджер свяжется с Вами в течение 30 минут. \r\nПожалуйста, проверяйте правильность указанного телефона и электронной \r\nпочты.</p><p>Вы можете оформить заказ по телефону 8 (863) 333-20-90, \r\nвоспользовавшись формой заявки на нашем сайте \r\n(http://kord-master.ru/kontakty/) или отправив запрос на электронную \r\nпочту info@kord-master.ru.</p><h2><br></h2><h2>Способы оплаты</h2><p>– предоплатой по счету банковским переводом (счет выставляется менеджером при подтверждении заказа)</p><p>– наличными</p><p>– банковской картой через терминал</p><p>– банковской картой через интернет (оплата производится через ПАО \r\nСБЕРБАНК с использованием банковских карт следующих систем: МИР, \r\nMastercard Worldwide, VISA International. Для оплаты (ввода реквизитов \r\nВашей карты) Вы будете перенаправлены на платежный шлюз ПАО СБЕРБАНК. \r\nСоединение с платежным шлюзом и передача информации осуществляется в \r\nзащищенном режиме с использованием протокола шифрования SSL. В случае \r\nесли Ваш банк поддерживает технологию безопасного проведения \r\nинтернет-платежей Verified By Visa или MasterCard SecureCode для \r\nпроведения платежа также может потребоваться ввод специального пароля.<br>\r\nНастоящий сайт поддерживает 256-битное шифрование. Конфиденциальность \r\nсообщаемой персональной информации обеспечивается ПАО СБЕРБАНК. \r\nВведенная информация не будет предоставлена третьим лицам за исключением\r\n случаев, предусмотренных законодательством РФ. Проведение платежей по \r\nбанковским картам осуществляется в строгом соответствии с требованиями \r\nплатежных систем МИР, Visa Int. и MasterCard Europe Sprl.)</p><p>– оформление кредита (мы сотрудничаем с банками ОТП и Home Credit. \r\nУсловия кредитования рассчитываются персонально для каждого Клиента).</p><h2><br></h2><h2>Доставка</h2><p>– Самовывоз: заказанный Товар Вы можете забрать самостоятельно по адресу: г. Аксай, ул. Западная 33.</p><p>– Доставка в регионы транспортной компанией осуществляется по всей \r\nРоссии.  Стоимость и сроки доставки рассчитываются менеджером при \r\nоформлении заказа по запросу Клиента. Оплата банковским платежом \r\nили банковской картой на сайте. Срок доставки от 3- х дней, в \r\nзависимости от региона получателя.</p><p>Доставка транспортной компанией осуществляется после 100% предоплаты Товара.</p>','payment',0,'[{\"name\":\"address\",\"title\":\"\\u0410\\u0434\\u0440\\u0435\\u0441\",\"type\":\"address\",\"options\":\"\"}]','{\"address\":\"\\u0443\\u043b. \\u0417\\u0430\\u043f\\u0430\\u0434\\u043d\\u0430\\u044f, 33, \\u0410\\u043a\\u0441\\u0430\\u0439, \\u0420\\u043e\\u0441\\u0442\\u043e\\u0432\\u0441\\u043a\\u0430\\u044f \\u043e\\u0431\\u043b., \\u0420\\u043e\\u0441\\u0441\\u0438\\u044f, 346720#ChIJNSY_6imx40ARvFztT1JO32U#47.2663761#39.85366190000002\"}',6,1,2,0,3,1),(7,'Шиномонтаж','<p class=\"table-responsive\">\r\n	<table class=\"table table-bordered table-striped\">\r\n	<tbody>\r\n	<tr>\r\n		<td rowspan=\"4\" width=\"302\"><strong>Наименование услуг</strong>\r\n		</td>\r\n		<td colspan=\"7\" width=\"389\"><strong>Размер</strong>\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td colspan=\"4\"><strong>Одноэлементный диск</strong>\r\n		</td>\r\n		<td colspan=\"3\"><strong>Многоэлементный диск</strong>\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td colspan=\"4\"><strong>Колесо:</strong>\r\n		</td>\r\n		<td colspan=\"3\"><strong>Кольца</strong>\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td><u><strong>16″</strong></u>\r\n		</td>\r\n		<td><u><strong>17,5″</strong></u>\r\n		</td>\r\n		<td><u><strong>19,5″</strong></u>\r\n		</td>\r\n		<td><u><strong>22″</strong></u>\r\n		</td>\r\n		<td><u><strong>16″</strong></u>\r\n		</td>\r\n		<td><u><strong>20″</strong></u>\r\n		</td>\r\n		<td><u><strong>24R</strong></u>\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td width=\"302\">Снять – установить колесо\r\n		</td>\r\n		<td>100\r\n		</td>\r\n		<td>150\r\n		</td>\r\n		<td>150\r\n		</td>\r\n		<td>200\r\n		</td>\r\n		<td>150\r\n		</td>\r\n		<td>200\r\n		</td>\r\n		<td>350\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td width=\"302\">Снять – установить спаренные колёса\r\n		</td>\r\n		<td>150\r\n		</td>\r\n		<td>200\r\n		</td>\r\n		<td>200\r\n		</td>\r\n		<td>300\r\n		</td>\r\n		<td>200\r\n		</td>\r\n		<td>300\r\n		</td>\r\n		<td>450\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td width=\"302\">Снять – установить колесо свыше 385\r\n		</td>\r\n		<td colspan=\"7\">300\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td width=\"302\">Снять – установить спаренные колёса свыше 6 шпилек\r\n		</td>\r\n		<td colspan=\"7\">300\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td width=\"302\">Снять – установить спаренные колёса с футорками\r\n		</td>\r\n		<td>200\r\n		</td>\r\n		<td>250\r\n		</td>\r\n		<td>300\r\n		</td>\r\n		<td>350\r\n		</td>\r\n		<td>250\r\n		</td>\r\n		<td>350\r\n		</td>\r\n		<td>\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td width=\"302\">Применение гидравлики при снятии колеса\r\n		</td>\r\n		<td colspan=\"7\">150\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td width=\"302\">Снять – установить запасное колесо\r\n		</td>\r\n		<td>50\r\n		</td>\r\n		<td>65\r\n		</td>\r\n		<td>80\r\n		</td>\r\n		<td>150\r\n		</td>\r\n		<td>65\r\n		</td>\r\n		<td>150\r\n		</td>\r\n		<td>\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td width=\"302\">Подкачка колеса\r\n		</td>\r\n		<td>20\r\n		</td>\r\n		<td>20\r\n		</td>\r\n		<td>30\r\n		</td>\r\n		<td>40\r\n		</td>\r\n		<td>20\r\n		</td>\r\n		<td>40\r\n		</td>\r\n		<td>\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td width=\"302\">Протяжка гаек\r\n		</td>\r\n		<td>40\r\n		</td>\r\n		<td>40\r\n		</td>\r\n		<td>60\r\n		</td>\r\n		<td>80\r\n		</td>\r\n		<td>40\r\n		</td>\r\n		<td>80\r\n		</td>\r\n		<td>\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td width=\"302\">Балансировка компаунд\r\n		</td>\r\n		<td colspan=\"2\">ТМ 300=600\r\n		</td>\r\n		<td colspan=\"2\">ТМ 400=700\r\n		</td>\r\n		<td colspan=\"2\">ТМ 500=800\r\n		</td>\r\n		<td>\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td width=\"302\">Балансировка грузами\r\n		</td>\r\n		<td>150\r\n		</td>\r\n		<td>150\r\n		</td>\r\n		<td>200\r\n		</td>\r\n		<td>250\r\n		</td>\r\n		<td>\r\n		</td>\r\n		<td>\r\n		</td>\r\n		<td>\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td width=\"302\">Нарезка протектора (руль, прицеп)\r\n		</td>\r\n		<td>350\r\n		</td>\r\n		<td>350\r\n		</td>\r\n		<td>450\r\n		</td>\r\n		<td>500\r\n		</td>\r\n		<td>400\r\n		</td>\r\n		<td>700\r\n		</td>\r\n		<td>800\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td width=\"302\">Нарезка протектора (зад.)\r\n		</td>\r\n		<td colspan=\"3\">500\r\n		</td>\r\n		<td>700\r\n		</td>\r\n		<td>\r\n		</td>\r\n		<td>\r\n		</td>\r\n		<td>\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td width=\"302\">Шиномонтаж колеса\r\n		</td>\r\n		<td>120\r\n		</td>\r\n		<td>150\r\n		</td>\r\n		<td>200\r\n		</td>\r\n		<td>250\r\n		</td>\r\n		<td>500\r\n		</td>\r\n		<td>600\r\n		</td>\r\n		<td>800\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td colspan=\"8\" width=\"691\"><strong>Шина + диск</strong>\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td width=\"302\">Демонтаж\r\n		</td>\r\n		<td>60\r\n		</td>\r\n		<td>60\r\n		</td>\r\n		<td>80\r\n		</td>\r\n		<td>100\r\n		</td>\r\n		<td>200\r\n		</td>\r\n		<td>250\r\n		</td>\r\n		<td>400\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td width=\"302\">Монтаж\r\n		</td>\r\n		<td>60\r\n		</td>\r\n		<td>90\r\n		</td>\r\n		<td>120\r\n		</td>\r\n		<td>150\r\n		</td>\r\n		<td>300\r\n		</td>\r\n		<td>350\r\n		</td>\r\n		<td>400\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td rowspan=\"2\" width=\"302\">Диск\r\n		</td>\r\n		<td colspan=\"2\">чистка=50\r\n		</td>\r\n		<td colspan=\"2\">уст. Уд-ля=150\r\n		</td>\r\n		<td colspan=\"2\">уст. Вентиля=150\r\n		</td>\r\n		<td>\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td colspan=\"3\">прот. Посад. Места=100\r\n		</td>\r\n		<td colspan=\"3\">уголок подкачки СМЕ45=150\r\n		</td>\r\n		<td>\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td width=\"302\">Установка камеры\r\n		</td>\r\n		<td>50\r\n		</td>\r\n		<td>100\r\n		</td>\r\n		<td>150\r\n		</td>\r\n		<td>\r\n		</td>\r\n		<td>\r\n		</td>\r\n		<td>\r\n		</td>\r\n		<td>\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td colspan=\"8\" width=\"691\"><strong>Ремонт шины:</strong>\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td width=\"302\">Диагностика повреждения и определение способа ремонта\r\n		</td>\r\n		<td colspan=\"4\">100\r\n		</td>\r\n		<td>\r\n		</td>\r\n		<td>\r\n		</td>\r\n		<td>\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td rowspan=\"2\" width=\"302\">Установка грибка\r\n		</td>\r\n		<td colspan=\"2\">9 мм=600\r\n		</td>\r\n		<td colspan=\"2\">13 мм=900\r\n		</td>\r\n		<td colspan=\"2\">15 мм=1000\r\n		</td>\r\n		<td>\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td colspan=\"2\">Мини А6=500\r\n		</td>\r\n		<td>\r\n		</td>\r\n		<td>\r\n		</td>\r\n		<td>\r\n		</td>\r\n		<td>\r\n		</td>\r\n		<td>\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td width=\"302\">Установка ножки\r\n		</td>\r\n		<td colspan=\"2\">13 мм=450\r\n		</td>\r\n		<td colspan=\"2\">В8 = 150\r\n		</td>\r\n		<td>В10= 250\r\n		</td>\r\n		<td>В12 = 250\r\n		</td>\r\n		<td>\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td rowspan=\"7\" width=\"302\">Установка пластыря без вулканизации повреждения\r\n		</td>\r\n		<td colspan=\"2\">110 TL = 300\r\n		</td>\r\n		<td colspan=\"2\">UP 3 = 200\r\n		</td>\r\n		<td colspan=\"2\">UP8=300\r\n		</td>\r\n		<td>\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td colspan=\"2\">115 TL = 350\r\n		</td>\r\n		<td colspan=\"2\">UP 6 = 250\r\n		</td>\r\n		<td>\r\n		</td>\r\n		<td>\r\n		</td>\r\n		<td>\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td colspan=\"2\">125 TL = 950\r\n		</td>\r\n		<td>\r\n		</td>\r\n		<td>\r\n		</td>\r\n		<td>\r\n		</td>\r\n		<td>\r\n		</td>\r\n		<td>\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td colspan=\"2\">120 TL = 650\r\n		</td>\r\n		<td colspan=\"2\">122 TL = 750\r\n		</td>\r\n		<td colspan=\"2\">124 TL = 800\r\n		</td>\r\n		<td>\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td colspan=\"2\">140 TL = 1000\r\n		</td>\r\n		<td colspan=\"2\">142 TL = 1200\r\n		</td>\r\n		<td colspan=\"2\">144 TL = 1300\r\n		</td>\r\n		<td>\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td colspan=\"2\">146 TL = 2100\r\n		</td>\r\n		<td colspan=\"2\">135 TL = 1200\r\n		</td>\r\n		<td colspan=\"2\">533 TL = 1100\r\n		</td>\r\n		<td>\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td colspan=\"2\">535 TL = 1300\r\n		</td>\r\n		<td colspan=\"2\">537 TL = 1400\r\n		</td>\r\n		<td>\r\n		</td>\r\n		<td>\r\n		</td>\r\n		<td>\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td width=\"302\">Установка камерной латки, якорь\r\n		</td>\r\n		<td>100\r\n		</td>\r\n		<td>150\r\n		</td>\r\n		<td>250\r\n		</td>\r\n		<td>\r\n		</td>\r\n		<td>\r\n		</td>\r\n		<td>\r\n		</td>\r\n		<td>\r\n		</td>\r\n	</tr>\r\n	<tr>\r\n		<td width=\"302\">Удаление старого ремонтного материала\r\n		</td>\r\n		<td>150\r\n		</td>\r\n		<td></td>\r\n		<td></td>\r\n		<td></td>\r\n		<td></td>\r\n		<td></td>\r\n		<td></td>\r\n	</tr>\r\n	<tr>\r\n		<td width=\"302\">Вулканизация\r\n		</td>\r\n		<td>400\r\n		</td>\r\n		<td>500\r\n		</td>\r\n		<td>700\r\n		</td>\r\n		<td>900\r\n		</td>\r\n		<td></td>\r\n		<td></td>\r\n		<td></td>\r\n	</tr>\r\n	<tr>\r\n		<td width=\"302\">Приемка автошины на хранение\r\n		</td>\r\n		<td>50\r\n		</td>\r\n		<td></td>\r\n		<td></td>\r\n		<td></td>\r\n		<td></td>\r\n		<td></td>\r\n		<td></td>\r\n	</tr>\r\n	<tr>\r\n		<td width=\"302\">Восстановление скола(вулканизация)\r\n		</td>\r\n		<td>250\r\n		</td>\r\n		<td>350\r\n		</td>\r\n		<td>450\r\n		</td>\r\n		<td>550\r\n		</td>\r\n		<td></td>\r\n		<td></td>\r\n		<td></td>\r\n	</tr>\r\n	<tr>\r\n		<td width=\"302\">Утилизация автошины\r\n		</td>\r\n		<td>100\r\n		</td>\r\n		<td>50\r\n		</td>\r\n		<td>100\r\n		</td>\r\n		<td>100\r\n		</td>\r\n		<td></td>\r\n		<td></td>\r\n		<td></td>\r\n	</tr>\r\n	</tbody>\r\n	</table>\r\n</p>','price',0,'[{\"name\":\"description\",\"title\":\"\\u041e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435\",\"type\":\"html\",\"options\":\"\"}]','{\"description\":\"<p>\\u041a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u044b\\u0435 \\u0448\\u0438\\u043d\\u043e\\u043c\\u043e\\u043d\\u0442\\u0430\\u0436 \\u0438 \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442 \\u0448\\u0438\\u043d \\u043f\\u0440\\u043e\\u0434\\u043b\\u0438\\u0442 \\u0441\\u0440\\u043e\\u043a \\u0438\\u0445 \\u0441\\u043b\\u0443\\u0436\\u0431\\u044b. \\u041e\\u0441\\u0443\\u0449\\u0435\\u0441\\u0442\\u0432\\u043b\\u044f\\u0435\\u043c \\u043c\\u043e\\u043d\\u0442\\u0430\\u0436 \\u0448\\u0438\\u043d, \\u0431\\u0430\\u043b\\u0430\\u043d\\u0441\\u0438\\u0440\\u043e\\u0432\\u043a\\u0443 \\u043a\\u043e\\u043b\\u0451\\u0441, \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442 \\u0448\\u0438\\u043d \\u043b\\u044e\\u0431\\u043e\\u0439 \\u0441\\u043b\\u043e\\u0436\\u043d\\u043e\\u0441\\u0442\\u0438.<\\/p>\"}',7,1,2,0,4,1),(8,'Пользовательское соглашение','<p><strong>Пользовательское соглашение</strong></p><p>Настоящее Соглашение определяет условия использования Пользователями \r\nматериалов и сервисов сайта <a href=\"http://www.kord-master.ru\">www.kord-master.ru</a>       (далее — \r\n«Сайт»). Настоящий документ представляет собой предложение ООО «ТД \r\n“Кордмастер» (далее — «Администрация»), заключить договор на изложенных \r\nниже условиях Соглашения.</p><p><strong>1.Общие условия</strong></p><p>1.1. Использование материалов и сервисов Сайта регулируется нормами действующего законодательства Российской Федерации.</p><p>1.2. Настоящее Соглашение является публичной офертой. Получая доступ к\r\n материалам Сайта Пользователь считается присоединившимся к настоящему \r\nСоглашению.</p><p>1.3. Соглашение (в том числе любая из его частей) может быть изменено\r\n Администрацией без какого-либо специального уведомления. Новая редакция\r\n Соглашения вступает в силу с момента ее размещения на Сайте \r\nАдминистрации либо доведения до сведения Пользователя в иной удобной \r\nформе, если иное не предусмотрено новой редакцией Соглашения. При \r\nнесогласии Пользователя с внесенными изменениями он обязан отказаться от\r\n доступа к Сайту, прекратить использование материалов и сервисов Сайта.</p><p><strong>2. Обязательства Пользователя</strong></p><p>2.1. Пользователь соглашается не предпринимать действий, которые \r\nмогут рассматриваться как нарушающие российское законодательство или \r\nнормы международного права, в том числе в сфере интеллектуальной \r\nсобственности, авторских  и/или\r\n смежных правах, а также любых действий, которые приводят или могут \r\nпривести к нарушению нормальной работы Сайта и сервисов Сайта.</p><p>2.2. Использование материалов Сайта без согласия правообладателей не \r\nдопускается (статья 1270 Г.К РФ). Для правомерного использования \r\nматериалов Сайта необходимо заключение лицензионных договоров (получение\r\n лицензий) от Правообладателей.</p><p>2.3. При цитировании  материалов Сайта, включая охраняемые авторские \r\nпроизведения, ссылка на Сайт обязательна (подпункт 1 пункта 1 статьи \r\n1274 Г.К РФ).</p><p>2.4. Комментарии и иные записи Пользователя на Сайте не должны \r\nвступать в противоречие с требованиями законодательства Российской \r\nФедерации и общепринятых норм морали и нравственности.</p><p>2.5. Пользователь предупрежден о том, что Администрация Сайта не \r\nнесет ответственности за посещение и использование им внешних ресурсов, \r\nссылки на которые могут содержаться на сайте.</p><p>2.6. Пользователь согласен с тем, что Администрация Сайта не несет \r\nответственности и не имеет прямых или косвенных обязательств перед \r\nПользователем в связи с любыми возможными или возникшими потерями или \r\nубытками, связанными с любым содержанием Сайта, регистрацией авторских \r\nправ и сведениями о такой регистрации, товарами или услугами, доступными\r\n на или полученными через внешние сайты или ресурсы либо иные контакты \r\nПользователя, в которые он вступил, используя размещенную на Сайте \r\nинформацию или ссылки на внешние ресурсы.</p><p>2.7. Пользователь принимает положение о том, что все материалы и \r\nсервисы Сайта или любая их часть могут сопровождаться рекламой. \r\nПользователь согласен с тем, что Администрация Сайта не несет какой-либо\r\n ответственности и не имеет каких-либо обязательств в связи с такой \r\nрекламой.</p><p>2.8. Технические, организационные и коммерческие условия \r\nиспользования Сервиса, в том числе его функциональных возможностей \r\nдоводятся до сведения Пользователей путем отдельного размещения на Сайте\r\n или путем нотификации Пользователей</p><p><strong>3. Прочие условия</strong></p><p>3.1. Все возможные споры, вытекающие из настоящего Соглашения или \r\nсвязанные с ним, подлежат разрешению в соответствии с действующим \r\nзаконодательством Российской Федерации.</p><p>3.2. Ничто в Соглашении не может пониматься как установление между \r\nПользователем и Администрации Сайта агентских отношений, отношений \r\nтоварищества, отношений по совместной деятельности, отношений личного \r\nнайма, либо каких-то иных отношений, прямо не предусмотренных \r\nСоглашением.</p><p>3.3. Признание судом какого-либо положения Соглашения \r\nнедействительным или не подлежащим принудительному исполнению не влечет \r\nнедействительности иных положений Соглашения.</p><p>3.4. Бездействие со стороны Администрации Сайта в случае нарушения \r\nкем-либо из Пользователей положений Соглашения не лишает Администрацию \r\nСайта права предпринять позднее соответствующие действия в защиту своих \r\nинтересов и защиту авторских прав на охраняемые в соответствии с \r\nзаконодательством материалы Сайта.</p><p><strong>Пользователь подтверждает, что ознакомлен со всеми пунктами настоящего Соглашения и безусловно принимает их.</strong></p>','conditions',0,'{}','{}',8,1,2,0,5,1),(9,'Политика конфиденциальности','<p>Настоящая Политика конфиденциальности персональных данных (далее – \r\nПолитика конфиденциальности) действует в отношении всей информации, \r\nкоторую Сайт «ООО ТД «Кордмастер»,  расположенный на доменном имени \r\nwww.kord-master.ru, может получить о Пользователе во время использования\r\n Cайта.</p><p><strong>1. ОПРЕДЕЛЕНИЕ ТЕРМИНОВ</strong></p><p>1.1.    В настоящей Политике конфиденциальности используются следующие термины:</p><p>1.1.1. «Администрация сайта (далее – Администрация сайта) » – \r\nуполномоченные сотрудники на управления сайтом, действующие от имени \r\n«ООО ТД «Кордмастер»и,  которые организуют и (или) осуществляет \r\nобработку персональных данных, а также определяет цели обработки \r\nперсональных данных, состав персональных данных, подлежащих обработке, \r\nдействия (операции), совершаемые с персональными данными.</p><p>1.1.2. «Персональные данные» – любая информация, относящаяся к прямо \r\nили косвенно определенному или определяемому физическому лицу (субъекту \r\nперсональных данных).</p><p>1.1.3. «Обработка персональных данных» – любое действие (операция) \r\nили совокупность действий (операций), совершаемых с использованием \r\nсредств автоматизации или без использования таких средств с \r\nперсональными данными, включая сбор, запись, систематизацию, накопление,\r\n хранение, уточнение (обновление, изменение), извлечение, использование,\r\n передачу (распространение, предоставление, доступ), обезличивание, \r\nблокирование, удаление, уничтожение персональных данных.</p><p>1.1.4. «Конфиденциальность персональных данных» – обязательное для \r\nсоблюдения Оператором или иным получившим доступ к персональным данным \r\nлицом требование не допускать их распространения без согласия субъекта \r\nперсональных данных или наличия иного законного основания.</p><p>1.1.5. «Пользователь сайта (далее ‑ Пользователь)» – лицо, имеющее \r\nдоступ к Сайту, посредством сети Интернет и использующее Сайт.</p><p>1.1.6. «Cookies» — небольшой фрагмент данных, отправленный \r\nвеб-сервером и хранимый на компьютере пользователя, который веб-клиент \r\nили веб-браузер каждый раз пересылает веб-серверу в HTTP-запросе при \r\nпопытке открыть страницу соответствующего сайта.</p><p>1.1.7. «IP-адрес» — уникальный сетевой адрес узла в компьютерной сети, построенной по протоколу IP.</p><p><strong>2. ОБЩИЕ ПОЛОЖЕНИЯ</strong></p><p>2.1.    Использование Пользователем сайта означает согласие с \r\nнастоящей Политикой конфиденциальности и условиями обработки \r\nперсональных данных Пользователя.</p><p>2.2.    В случае несогласия с условиями Политики конфиденциальности Пользователь должен прекратить использование сайта.</p><p>2.3.    Настоящая Политика конфиденциальности применяется только к \r\nсайту «ООО ТД «Кордмастер». Сайт не контролирует и не несет \r\nответственность за сайты третьих лиц, на которые Пользователь может \r\nперейти по ссылкам, доступным на Сайте.</p><p>2.4.    Администрация сайта не проверяет достоверность персональных данных, предоставляемых Пользователем Сайта.</p><p><strong>3. ПРЕДМЕТ ПОЛИТИКИ КОНФИДЕНЦИАЛЬНОСТИ</strong></p><p><strong> </strong></p><p>3.1.    Настоящая Политика конфиденциальности устанавливает \r\nобязательства Администрации сайта по неразглашению и обеспечению режима \r\nзащиты конфиденциальности персональных данных, которые Пользователь \r\nпредоставляет по запросу Администрации сайта при регистрации на сайте \r\nили при оформлении заказа для приобретения Товара.</p><p>3.2. Персональные данные, разрешённые к обработке в рамках настоящей \r\nПолитики конфиденциальности, предоставляются Пользователем путём \r\nзаполнения формы заявки на сайте «ООО ТД «Кордмастер» в разделе  \r\n«Контакты» и включают в себя следующую информацию:</p><p>3.2.1. фамилию, имя, отчество Пользователя;</p><p>3.2.2. контактный телефон Пользователя;</p><p>3.2.3. адрес электронной почты (e-mail);</p><p>3.3. Сайт защищает Данные, которые автоматически передаются в \r\nпроцессе просмотра рекламных блоков и при посещении страниц, на которых \r\nустановлен статистический скрипт системы (“пиксель”):</p><ul><li>IP адрес;</li><li>информация из cookies;</li><li>информация о браузере (или иной программе, которая осуществляет доступ к показу рекламы);</li><li>время доступа;</li><li>адрес страницы, на которой расположен рекламный блок;</li><li>реферер (адрес предыдущей страницы).</li></ul><p>3.3.1. Отключение cookies может повлечь невозможность доступа к частям сайта, требующим авторизации.</p><p>3.3.2. Сайт осуществляет сбор статистики об IP-адресах своих \r\nпосетителей. Данная информация используется с целью выявления и решения \r\nтехнических проблем, для контроля законности проводимых финансовых \r\nплатежей.</p><p>3.4. Любая иная персональная информация неоговоренная выше (история \r\nпокупок, используемые браузеры и операционные системы и т.д.) подлежит \r\nнадежному хранению и нераспространению, за исключением случаев, \r\nпредусмотренных в п.п. 5.2. и 5.3. настоящей Политики \r\nконфиденциальности.</p><p><strong>4. ЦЕЛИ СБОРА ПЕРСОНАЛЬНОЙ ИНФОРМАЦИИ ПОЛЬЗОВАТЕЛЯ</strong></p><p>4.1. Персональные данные Пользователя Администрация сайта может использовать в целях:</p><p>4.1.1. Идентификации Пользователя, зарегистрированного на сайте, для \r\nоформления заказа и (или) заключения Договора купли-продажи товара \r\nдистанционным способом с «ООО ТД «Кордмастер».</p><p>4.1.2. Предоставления Пользователю доступа к персонализированным ресурсам Сайта.</p><p>4.1.3. Установления с Пользователем обратной связи, включая \r\nнаправление уведомлений, запросов, касающихся использования Сайта, \r\nоказания услуг, обработка запросов и заявок от Пользователя.</p><p>4.1.4. Определения места нахождения Пользователя для обеспечения безопасности, предотвращения мошенничества.</p><p>4.1.5. Подтверждения достоверности и полноты персональных данных, предоставленных Пользователем.</p><p>4.1.6. Создания учетной записи для совершения покупок, если Пользователь дал согласие на создание учетной записи.</p><p>4.1.7. Уведомления Пользователя Сайта о состоянии Заказа.</p><p>4.1.8. Обработки и получения платежей, подтверждения налога или \r\nналоговых льгот, оспаривания платежа, определения права на получение \r\nкредитной линии Пользователем.</p><p>4.1.9. Предоставления Пользователю эффективной клиентской и \r\nтехнической поддержки при возникновении проблем связанных с \r\nиспользованием Сайта.</p><p>4.1.10. Предоставления Пользователю с его согласия, обновлений \r\nпродукции, специальных предложений, информации о ценах, новостной \r\nрассылки и иных сведений от имени Сайта или от имени партнеров Сайта.</p><p>4.1.11. Осуществления рекламной деятельности с согласия Пользователя.</p><p>4.1.12. Предоставления доступа Пользователю на сайты или сервисы \r\nпартнеров Сайта с целью получения продуктов, обновлений и услуг.</p><p><strong>5. СПОСОБЫ И СРОКИ ОБРАБОТКИ ПЕРСОНАЛЬНОЙ</strong> <strong>ИНФОРМАЦИИ</strong></p><p>5.1. Обработка персональных данных Пользователя осуществляется без \r\nограничения срока, любым законным способом, в том числе в информационных\r\n системах персональных данных с использованием средств автоматизации или\r\n без использования таких средств.</p><p>5.2. Пользователь соглашается с тем, что Администрация сайта вправе \r\nпередавать персональные данные третьим лицам, в частности, курьерским \r\nслужбам, организациями почтовой связи, операторам электросвязи, \r\nисключительно в целях выполнения заказа Пользователя, оформленного на \r\nСайте «ООО ТД «Кордмастер», включая доставку Товара.</p><p>5.3. Персональные данные Пользователя могут быть переданы \r\nуполномоченным органам государственной власти Российской Федерации \r\nтолько по основаниям и в порядке, установленным законодательством \r\nРоссийской Федерации.</p><p>5.4. При утрате или разглашении персональных данных Администрация \r\nсайта информирует Пользователя об утрате или разглашении персональных \r\nданных.</p><p>5.5. Администрация сайта принимает необходимые организационные и \r\nтехнические меры для защиты персональной информации Пользователя от \r\nнеправомерного или случайного доступа, уничтожения, изменения, \r\nблокирования, копирования, распространения, а также от иных \r\nнеправомерных действий третьих лиц.</p><p>5.6. Администрация сайта совместно с Пользователем принимает все \r\nнеобходимые меры по предотвращению убытков или иных отрицательных \r\nпоследствий, вызванных утратой или разглашением персональных данных \r\nПользователя.</p><p><strong>6. ОБЯЗАТЕЛЬСТВА СТОРОН</strong></p><p><strong>6.1. Пользователь обязан:</strong></p><p>6.1.1. Предоставить информацию о персональных данных, необходимую для пользования Сайтом.</p><p>6.1.2. Обновить, дополнить предоставленную информацию о персональных данных в случае изменения данной информации.</p><p><strong>6.2. Администрация сайта обязана:</strong></p><p>6.2.1. Использовать полученную информацию исключительно для целей, указанных в п. 4 настоящей Политики конфиденциальности.</p><p>6.2.2. Обеспечить хранение конфиденциальной информации в тайне, не \r\nразглашать без предварительного письменного разрешения Пользователя, а \r\nтакже не осуществлять продажу, обмен, опубликование, либо разглашение \r\nиными возможными способами переданных персональных данных Пользователя, \r\nза исключением п.п. 5.2. и 5.3. настоящей Политики Конфиденциальности.</p><p>6.2.3. Принимать меры предосторожности для защиты конфиденциальности \r\nперсональных данных Пользователя согласно порядку, обычно используемого \r\nдля защиты такого рода информации в существующем деловом обороте.</p><p>6.2.4. Осуществить блокирование персональных данных, относящихся к \r\nсоответствующему Пользователю, с момента обращения или запроса \r\nПользователя или его законного представителя либо уполномоченного органа\r\n по защите прав субъектов персональных данных на период проверки, в \r\nслучае выявления недостоверных персональных данных или неправомерных \r\nдействий.</p><p><strong>7. ОТВЕТСТВЕННОСТЬ СТОРОН</strong></p><p>7.1. Администрация сайта, не исполнившая свои обязательства, несёт \r\nответственность за убытки, понесённые Пользователем в связи с \r\nнеправомерным использованием персональных данных, в соответствии с \r\nзаконодательством Российской Федерации, за исключением случаев, \r\nпредусмотренных п.п. 5.2., 5.3. и 7.2. настоящей Политики \r\nКонфиденциальности.</p><p>7.2. В случае утраты или разглашения Конфиденциальной информации \r\nАдминистрация сайта не несёт ответственность, если данная \r\nконфиденциальная информация:</p><p>7.2.1. Стала публичным достоянием до её утраты или разглашения.</p><p>7.2.2. Была получена от третьей стороны до момента её получения Администрацией сайта.</p><p>7.2.3. Была разглашена с согласия Пользователя.</p><p><strong>8. РАЗРЕШЕНИЕ СПОРОВ</strong></p><p>8.1. До обращения в суд с иском по спорам, возникающим из отношений \r\nмежду Пользователем сайта  и Администрацией сайта, обязательным является\r\n предъявление претензии (письменного предложения о добровольном \r\nурегулировании спора).</p><p>8.2 .Получатель претензии в течение 30 календарных дней со дня \r\nполучения претензии, письменно уведомляет заявителя претензии о \r\nрезультатах рассмотрения претензии.</p><p>8.3. При недостижении соглашения спор будет передан на рассмотрение в\r\n судебный орган в соответствии с действующим законодательством \r\nРоссийской Федерации.</p><p>8.4. К настоящей Политике конфиденциальности и отношениям между \r\nПользователем и Администрацией сайта применяется действующее \r\nзаконодательство Российской Федерации.</p><p><strong>9. ДОПОЛНИТЕЛЬНЫЕ УСЛОВИЯ</strong></p><p>9.1. Администрация сайта вправе вносить изменения в настоящую Политику конфиденциальности без согласия Пользователя.</p><p>9.2. Новая Политика конфиденциальности вступает в силу с момента ее \r\nразмещения на Сайте, если иное не предусмотрено новой редакцией Политики\r\n конфиденциальности.</p><p>9.3. Все предложения или вопросы по настоящей Политике \r\nконфиденциальности следует сообщать в письменном виде на почту \r\ninfo@kord-master.ru.</p><p>9.4. Действующая Политика конфиденциальности размещена на странице по адресу http://kord-master.ru/politika-konfidencialnosti.</p>','privacy',0,'{}','{}',9,1,2,0,6,1),(10,'Обмен и возврат','<p><strong>ОБМЕН И ВОЗВРАТ ТОВАРА НАДЛЕЖАЩЕГО КАЧЕСТВА</strong></p><p>– Покупатель вправе обменять товар на аналогичный либо получить \r\nвозврат денежных средств за товар  в течение 14 дней после передачи \r\nтовара.<br>\r\n– Обмен и возврат товара надлежащего качества происходит на складе \r\nпродавца по адресу: г. Аксай, ул. Западная 33, ежедневно с 9 до 18 кроме\r\n субботы и воскресенья.<br>\r\n– При возврате или обмене товара покупатель при себе должен иметь \r\nтоварную накладную, кассовый чек или любой иной документ, подтверждающий\r\n факт приобретения товара.<br>\r\n– Товар должен сохранять свой товарный вид и потребительские свойства.Не\r\n принимаются к возврату шины использованные или бортированные. Не \r\nпринимаются к возврату диски бортированные или с испорченным \r\nлакокрасочным покрытием.<br>\r\n– Покупатель по электронной почте или по телефону должен уведомить продавца о намерении поменять или вернуть товар.<br>\r\n– Возврат денежных средств осуществляется в течение 10 дней с момента \r\nпредъявления покупателем соответствующего требования в офисе продавца по\r\n адресу: г. Аксай, ул. Западная 33, ежедневно с 9 до 18 кроме субботы и \r\nвоскресенья.</p><p>Важно! Если покупателю осуществлялась платная доставка, то при \r\nвозврате продукции деньги, уплаченные за доставку, не возвращаются!</p><p>При отказе Покупателя от Товара надлежащего качества Продавец должен \r\nвозвратить ему денежную сумму, уплаченную Покупателем по договору, за \r\nисключением расходов Продавца на доставку от Покупателя возвращенного \r\nТовара.<br>\r\nВ зависимости от формы оплаты возвращаемого Товара и правовой \r\nпринадлежности Покупателя (физическое или юридическое лицо), существуют \r\nнебольшие различия в процедуре возврата денежных средств.</p><p><strong>ОБМЕН И ВОЗВРАТ ТОВАРА ПРИ НАСТУПЛЕНИИ ГАРАНТИЙНОГО СЛУЧАЯ</strong></p><p>Если брак товара был выявлен в процессе пользования товаром напишите \r\nнам на почту info@kord-master.ru или свяжитесь с вашим менеджером. Мы \r\nоперативно проинструктируем Вас по дальнейшим действиям.</p><p><strong>ВОЗВРАТ ПРИ ПОКУПКЕ ЗА НАЛИЧНЫЙ РАСЧЁТ</strong></p><p>Возврат денежных средств осуществляется наличными в кассе Продавца не\r\n позднее, чем за 10 (десять) календарных дней с даты принятия \r\nПродавцом положительного решения о возврате денежных средств за товар . \r\nВозврат денежных средств возможен при наличии кассового, товарного \r\nчеков, паспорта Покупателя.</p><p><strong>ВОЗВРАТ ПРИ ПОКУПКЕ ЗА БЕЗНАЛИЧНЫЙ РАСЧЕТ</strong></p><p>Если возвращаемый Товар был оплачен банковской картой, возврат \r\nденежных средств происходит сразу после получения заявления от магазина в\r\n процессинговый центр и оформления заявки. Зачисление денежных средств \r\nосуществляется в те сроки, которые установлены банком, выпустившим \r\nкарту.</p>','return',0,'{}','{}',10,1,2,0,7,1),(11,'Результаты поиска','','search',0,'{}','{}',11,1,2,0,8,1),(12,'Корзина','','shopcart',0,'{}','{}',12,1,2,0,9,1),(13,'Вход в личный кабинет','','customer-login',0,'{}','{}',13,1,2,0,10,1),(14,'Регистрация','','customer-signup',0,'{}','{}',14,1,2,0,11,1),(15,'Личный кабинет','<p><span id=\"selection-marker-2\" class=\"redactor-selection-marker\"><span id=\"selection-marker-1\" class=\"redactor-selection-marker\"></span><span id=\"selection-marker-1\" class=\"redactor-selection-marker\"></span><span id=\"selection-marker-2\" class=\"redactor-selection-marker\"></span></span></p>','account',0,'{}','{}',15,1,2,0,12,1),(16,'Оформление заказа','','checkout',0,'{}','{}',16,1,2,0,13,1);
/*!40000 ALTER TABLE `cms_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_photos`
--

DROP TABLE IF EXISTS `cms_photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class` varchar(128) NOT NULL,
  `item_id` int(11) NOT NULL,
  `image_file` varchar(128) NOT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `order_num` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `model_item` (`class`,`item_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_photos`
--

LOCK TABLES `cms_photos` WRITE;
/*!40000 ALTER TABLE `cms_photos` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_seotext`
--

DROP TABLE IF EXISTS `cms_seotext`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_seotext` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class` varchar(128) NOT NULL,
  `item_id` int(11) NOT NULL,
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `model_item` (`class`,`item_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_seotext`
--

LOCK TABLES `cms_seotext` WRITE;
/*!40000 ALTER TABLE `cms_seotext` DISABLE KEYS */;
INSERT INTO `cms_seotext` VALUES (12,'yii\\cms\\modules\\article\\models\\Category',1,'Блог','Блог','склеивания поролонов','Полезные материалы по выбору клея для производства мебели, склеивания поролонов между собой и другими материалами.'),(13,'yii\\cms\\modules\\page\\models\\Page',3,'','КордМастер - грузовые шины в Ростове-на-Дону. Продажа грузовых шин. Восстановление, наварка грузовых шин.','','');
/*!40000 ALTER TABLE `cms_seotext` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_settings`
--

DROP TABLE IF EXISTS `cms_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `title` varchar(128) NOT NULL,
  `value` varchar(1024) DEFAULT NULL,
  `visibility` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_settings`
--

LOCK TABLES `cms_settings` WRITE;
/*!40000 ALTER TABLE `cms_settings` DISABLE KEYS */;
INSERT INTO `cms_settings` VALUES (1,'cms_version','cmsCMS version','0.91',0),(2,'image_max_width','Максимальная ширина загружаемых изображений, которые автоматически не сжимаются','1900',2),(3,'redactor_plugins','Список плагинов редактора Redactor через запятую','imagemanager, filemanager, table, fullscreen',1),(4,'ga_service_email','E-mail сервис аккаунта Google Analytics','',1),(5,'ga_profile_id','Номер профиля Google Analytics','',1),(6,'ga_p12_file','Путь к файлу ключей p12 сервис аккаунта Google Analytics','',1),(7,'gm_api_key','Google Maps API ключ','AIzaSyD3TNxUzBswDgaODRYXWsHdWgfwe-i3Vm0',1),(8,'recaptcha_key','ReCaptcha key','',1),(9,'password_salt','Password salt','KG-6Nsr6MWMpH9dWoTARYkVgWEIdXDJK',0),(10,'root_auth_key','Root authorization key','Mn-w2PPdz_CMncHOWw840OWWqrlwuRoK',0),(11,'root_password','Пароль разработчика','189861b645fa28da19d6b819beac4a46ac9f455d',1),(12,'auth_time','Время авторизации','86400',1),(13,'robot_email','E-mail рассыльщика','b059ae@gmail.com',1),(14,'admin_email','E-mail администратора','b059ae@gmail.com',2),(15,'telegram_chat_id','Telegram chat ID','',2),(16,'telegram_bot_token','Telegram bot token','',1),(17,'recaptcha_secret','ReCaptcha secret','',1),(18,'toolbar_position','Позиция панели на сайте (\"top\" or \"bottom\" or \"hide\")','hide',1),(19,'phone','Контактный телефон на сайте','+7 (863) 333-20-90',2),(20,'email','Контактный Email на сайте','info@kord-master.ru',2),(21,'address','Адрес на сайте','Ростовская область; г. Аксай; ул. Западная, 33',2),(22,'map_url_1','Ссылка на маршрут по М4 на Москву','https://yandex.ru/maps/?z=14&ll=39.84833757554779,47.27881400949132&l=map&origin=jsapi_2_1_55&from=api-maps&um=constructor:LLtZZYxMeh7GxrbCcvS7YyJRv8-y5jKC',2),(23,'map_url_2','Маршрут по М4 в сторону Краснодара','https://yandex.ru/maps/?z=14&ll=39.850480578437406,47.28105536217925&l=map&origin=jsapi_2_1_55&from=api-maps&um=constructor:g-IMAFwnxcu3fbwOev8YFjK3qYZREQwi',2),(24,'truck-tyres_max_price','Максимальная цена для грузовых шин','40000',2),(25,'spec-tyres_max_price','Максимальная цена для спецшин','400000',2),(26,'auto-tyres_max_price','Максимальная цена для легковых шин','20000',2),(27,'wheels_max_price','Максимальная цена на диски','20000',2);
/*!40000 ALTER TABLE `cms_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_shopcart_goods`
--

DROP TABLE IF EXISTS `cms_shopcart_goods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_shopcart_goods` (
  `good_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `options` varchar(255) NOT NULL,
  `price` float DEFAULT '0',
  `discount` int(11) DEFAULT '0',
  PRIMARY KEY (`good_id`)
) ENGINE=MyISAM AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_shopcart_goods`
--

LOCK TABLES `cms_shopcart_goods` WRITE;
/*!40000 ALTER TABLE `cms_shopcart_goods` DISABLE KEYS */;
INSERT INTO `cms_shopcart_goods` VALUES (4,2,126,2,'',19800,0),(5,2,124,2,'',17100,0),(34,8,78,1,'',9900,0),(33,8,90,3,'',22000,0),(32,8,131,1,'',14000,0),(30,6,50,1,'',10500,0),(29,4,97,1,'',23200,0),(23,5,130,1,'',5800,0),(28,4,79,1,'',14600,0),(27,4,70,1,'',9900,0),(35,8,31,1,'',12800,0),(36,8,130,15,'',5800,0),(37,9,104,1,'',13300,0),(38,10,41,1,'',9000,0),(39,11,89,6,'',19500,0),(40,12,121,1,'',21000,0),(41,13,123,3,'',16300,0),(42,14,130,1,'',5800,0),(43,13,121,1,'',21000,0),(44,15,175,5,'',14200,0),(45,16,50,1,'',10500,0),(46,16,117,1,'',17100,0),(49,19,123,1,'',16300,0),(50,20,123,4,'',16300,0),(51,21,178,1,'',22900,0),(52,22,135,2,'',4000,0),(53,22,153,4,'',4500,0),(54,23,153,2,'',4500,0),(55,23,137,4,'',4500,0);
/*!40000 ALTER TABLE `cms_shopcart_goods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_shopcart_orders`
--

DROP TABLE IF EXISTS `cms_shopcart_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_shopcart_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `comment` varchar(1024) DEFAULT NULL,
  `remark` varchar(1024) DEFAULT NULL,
  `access_token` varchar(32) DEFAULT NULL,
  `ip` varchar(16) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `new` tinyint(1) DEFAULT '0',
  `status` tinyint(1) DEFAULT '0',
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_shopcart_orders`
--

LOCK TABLES `cms_shopcart_orders` WRITE;
/*!40000 ALTER TABLE `cms_shopcart_orders` DISABLE KEYS */;
INSERT INTO `cms_shopcart_orders` VALUES (1,'','','',NULL,'',NULL,'_w-nk-WVgdMZdJQuNgOet2EjhME7ksMH','127.0.0.1',1506587018,0,0,NULL),(2,'','','',NULL,'',NULL,'F1kCbt5gHqnwUpobitkSmSMBoa2dJtGl','127.0.0.1',1506625179,0,0,NULL),(3,'','','',NULL,'',NULL,'ha17FC7LEuXqRM_WWjlTZCrCGbEjDHt2','127.0.0.1',1506673508,0,0,NULL),(4,'','','',NULL,'',NULL,'rhE4qNJ2-0HW58F9ZR9hkjjI1Ba67hVi','212.192.204.44',1506691361,0,0,NULL),(5,'','','',NULL,'',NULL,'-vrewxyVvWehT5RlQdcXqKuuxgB4H-95','195.151.120.105',1506694993,0,0,NULL),(6,'','','',NULL,'',NULL,'mAAewt_gmFFt9W9elybusgxWJiV1o_e_','178.155.4.164',1506755960,0,0,NULL),(7,'','','',NULL,'',NULL,'OlZHtrUIdOgwL7ptuJK3smucDNG6SwAL','95.153.131.201',1506832283,0,0,NULL),(8,'','','',NULL,'',NULL,'KdLeyV0SgvLDNuUNn0-5MJRurp7yIfwt','127.0.0.1',1506847892,0,0,NULL),(9,'','','',NULL,'',NULL,'FQ9l5Gy9YiDTU095YD8ug7CjwNU2t08B','195.151.120.105',1506935764,0,0,NULL),(10,'','','',NULL,'',NULL,'PL7WCDnMwOB5zSTxdomwGfJSRUQx-RcC','212.192.204.44',1506937198,0,0,NULL),(11,'','','',NULL,'',NULL,'rVr9fWX8hdK6u16GIWB-X9OJuCmIhqWs','195.151.159.222',1506955947,0,0,NULL),(12,'','','',NULL,'',NULL,'XuIDNmeiO70V1EWDDUQc6MMI4C7WbErS','127.0.0.1',1507111909,0,0,NULL),(13,'','','',NULL,'',NULL,'JwUKC9t6WLTNdejiIDcbJ_ZHBnswZ8Rk','127.0.0.1',1507112578,0,0,NULL),(14,'','','',NULL,'',NULL,'YBlVowx9yQ0h3Xt92wcrkQ_n40K9n98n','212.192.204.44',1507114748,0,0,NULL),(15,'','','',NULL,'',NULL,'R4yB99LJqqtUVTOJLHs1NwEKQGfkgN5S','195.151.159.222',1507124430,0,0,NULL),(16,'','','',NULL,'',NULL,'8sW6JAJB_lUyfJ4yDySOLqUfxILUHz6G','127.0.0.1',1507125906,0,0,NULL),(17,'','','',NULL,'',NULL,'MaSFvOHJpWvczg13bTj-3NF0m2dd3VS0','195.151.120.105',1507126974,0,0,NULL),(18,'','','',NULL,'',NULL,'lH0oQL0x_1-JeU6bqi1Q3C-d3Sq4WxTI','195.151.120.105',1507186010,0,0,NULL),(19,'','','',NULL,'',NULL,'cA_S06t3dqPS-lyPHPJlrxQ33-23jrQU','127.0.0.1',1507211229,0,0,NULL),(20,'','','',NULL,'',NULL,'pqCEN7UN4mgdFyZxwfFmob_iH8haWlO_','217.118.81.201',1507213654,0,0,NULL),(21,'','','',NULL,'',NULL,'qXaGrFZMj7QMagBEO5XrZYdsm_QnH-pO','188.162.167.118',1507233805,0,0,NULL),(22,'','','',NULL,'',NULL,'t5LUs7lOKYN2phwrW7o0-AxuKvuWvheT','127.0.0.1',1507273513,0,0,NULL),(23,'Rocker','Ворошиловский проспект, X','','stariy.roker@yandex.ru','Способ доставки: Транспортная компания',NULL,'i116jLXnGCefxPObWrmbRUDH6VlzYmvc','127.0.0.1',1507284940,0,1,2);
/*!40000 ALTER TABLE `cms_shopcart_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_subscribe_history`
--

DROP TABLE IF EXISTS `cms_subscribe_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_subscribe_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(128) NOT NULL,
  `body` text,
  `sent` int(11) DEFAULT '0',
  `time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_subscribe_history`
--

LOCK TABLES `cms_subscribe_history` WRITE;
/*!40000 ALTER TABLE `cms_subscribe_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_subscribe_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_subscribe_subscribers`
--

DROP TABLE IF EXISTS `cms_subscribe_subscribers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_subscribe_subscribers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL,
  `ip` varchar(16) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_subscribe_subscribers`
--

LOCK TABLES `cms_subscribe_subscribers` WRITE;
/*!40000 ALTER TABLE `cms_subscribe_subscribers` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_subscribe_subscribers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_tags`
--

DROP TABLE IF EXISTS `cms_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `frequency` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_tags`
--

LOCK TABLES `cms_tags` WRITE;
/*!40000 ALTER TABLE `cms_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_tags_assign`
--

DROP TABLE IF EXISTS `cms_tags_assign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_tags_assign` (
  `class` varchar(128) NOT NULL,
  `item_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  KEY `class` (`class`),
  KEY `item_tag` (`item_id`,`tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_tags_assign`
--

LOCK TABLES `cms_tags_assign` WRITE;
/*!40000 ALTER TABLE `cms_tags_assign` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_tags_assign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_texts`
--

DROP TABLE IF EXISTS `cms_texts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_texts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text NOT NULL,
  `slug` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_texts`
--

LOCK TABLES `cms_texts` WRITE;
/*!40000 ALTER TABLE `cms_texts` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_texts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('m000000_000000_base',1504005185),('m140209_132017_init',1504005195),('m140403_174025_create_account_table',1504005196),('m140504_113157_update_tables',1504005205),('m140504_130429_create_token_table',1504005206),('m140830_171933_fix_ip_field',1504005207),('m140830_172703_change_account_table_name',1504005207),('m141222_110026_update_ip_field',1504005208),('m141222_135246_alter_username_length',1504005209),('m150614_103145_update_social_account_table',1504005211),('m150623_212711_fix_username_notnull',1504005211),('m151218_234654_add_timezone_to_profile',1504005212),('m160929_103127_add_last_login_at_to_user_table',1504005212);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci,
  `timezone` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_user_profile` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile`
--

LOCK TABLES `profile` WRITE;
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
INSERT INTO `profile` VALUES (1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'tyres',NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social_account`
--

DROP TABLE IF EXISTS `social_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_unique` (`provider`,`client_id`),
  UNIQUE KEY `account_unique_code` (`code`),
  KEY `fk_user_account` (`user_id`),
  CONSTRAINT `fk_user_account` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social_account`
--

LOCK TABLES `social_account` WRITE;
/*!40000 ALTER TABLE `social_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `social_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `token`
--

DROP TABLE IF EXISTS `token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `token` (
  `user_id` int(11) NOT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) NOT NULL,
  `type` smallint(6) NOT NULL,
  UNIQUE KEY `token_unique` (`user_id`,`code`,`type`),
  CONSTRAINT `fk_user_token` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `token`
--

LOCK TABLES `token` WRITE;
/*!40000 ALTER TABLE `token` DISABLE KEYS */;
/*!40000 ALTER TABLE `token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `last_login_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_unique_username` (`username`),
  UNIQUE KEY `user_unique_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'zubizub','zubizubwork@gmail.com','$2y$10$ts.h9LwEYiNn7B/QLlnVCecmuetehKiaKPSPoIxfFZ7oDtrV3yzei','H6HHCIF7SEfiQVMchtu8EJoe35PTzucX',1504005435,NULL,NULL,NULL,1504005410,1504005410,0,1504096394),(2,'tyres','tyres@tyres.local','$2y$10$mYPlL0qYKl322Q1GQnenFuCB/abs7zvtmiq4r8fHu2AaChCtzl8Aa','awhNfSobkZcySOK_nBp1u0ILHRicDV99',1504005604,NULL,NULL,NULL,1504005596,1504005596,0,1504167023);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-08  9:11:26
