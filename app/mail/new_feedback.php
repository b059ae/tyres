<?php
/** @var string $subject */
/** @var string $link */

/** @var \yii\cms\modules\feedback\models\Feedback $feedback */

use yii\helpers\Html;

$this->title = $subject;
?>
<p>Пользователь оставил заявку на сайте.</p>
<p><b><?= $feedback->name ?></b></p>
<p><b><?= $feedback->phone ?></b></p>
<p>Просмотреть её вы можете <?= Html::a('здесь', $link) ?>.</p>
<hr>
<p>Это автоматическое сообщение и на него не нужно отвечать.</p>