<?php
namespace app\components;

use yii\base\BaseObject;
use OTasha\SberbankInternetAcquiringApiClient\ClientFactory;
use app\models\Payment;
use yii\cms\modules\shopcart\api\Shopcart;
use yii\cms\modules\shopcart\models\Order;

class SberbankPaymentGate extends BaseObject
{
    CONST FULL_AUTHORIZATION_ORDER_AMOUNT = 2;

    public $userName;
    public $password;
    public $testMode;
    private $apiContext;

    function init()
    {
        parent::init();

        $this->apiContext = ClientFactory::create(ClientFactory::REST, $this->userName, $this->password, $this->testMode);
    }

    public function getContext()
    {
        return $this->apiContext;
    }

    public function registerOrder($merchantOrderId, $cost, $orderBundle, $returnUrl, $userId = null)
    {
        $amount = $cost * 100; //Сбербанк принимает сумму платежа в минимальных единицах валюты (копейки, центы и т. п.)!!!

        if (empty($merchantOrderId)) {
            return ['formUrl' => '', 'message' => 'Не указан ID заказа для оплаты'];
        }

        $payment = Payment::find()->where(['merchantOrderId' => $merchantOrderId])->one();
        
        if ($payment === null) {
            $payment = new Payment();
            $payment = $this->doRegisterOrder($payment, $merchantOrderId, $amount, $orderBundle, $returnUrl, $userId);
            $payment->save();

        } else {
            if ($payment->paymentStatus !== self::FULL_AUTHORIZATION_ORDER_AMOUNT) {
                if (empty($payment->formUrl)) {
                    $payment = $this->doRegisterOrder($payment, $merchantOrderId, $amount, $orderBundle, $returnUrl, $userId);
                    $payment->save();
                }
                elseif(!empty($payment->orderBundle) && !$payment->equalsOrderBundle($orderBundle)){ // Если операция зарегистрирована, но покупатель поменял параметры заказа
                    $payment = $this->oneMoreRegister($payment, $amount, $orderBundle, $returnUrl, $userId);
                    $payment->save();
                }
            } else {
                return ['formUrl' => '', 'message' => 'Заказ уже оплачен'];
            }
        }

        $formUrl = $payment->formUrl;
        $message = $payment->paymentErrorMessage;
        return ['formUrl' => $formUrl, 'message' => $message];
    }

    public function doRegisterOrder($payment, $merchantOrderId, $amount, $orderBundle, $returnUrl, $userId)
    {
        $response = $this->getContext()->registerOrder($merchantOrderId, $amount, $orderBundle, $returnUrl);

        $payment->dateCreate = \Yii::$app->formatter->asDatetime('now', "php:Y-m-d H:i:s");
        $payment->merchantOrderId = $merchantOrderId;
        $payment->userId = $userId;
        $payment->amount = $amount;
        $payment->orderBundle = json_encode($orderBundle);

        if (isset($response['orderId'])) $payment->gatewayOrderId = $response['orderId'];
        if (isset($response['formUrl'])) $payment->formUrl = $response['formUrl'];

        if (isset($response['errorCode'])) $payment->paymentErrorCode = intval($response['errorCode']);
        if (isset($response['errorMessage'])) $payment->paymentErrorMessage = $response['errorMessage'];

        return $payment;
    }

    public function doOrderStatusExtended($gatewayOrderId)
    {
        $response = $this->getContext()->getOrderStatusExtended($gatewayOrderId);

        $message = (isset($response['errorMessage']) && !empty($response['errorMessage'])) ? $response['errorMessage'] : 'Сбой в работе платежного шлюза при получении статуса оплаты заказа, попробуйте позже';

        if ($message == 'Успех' && isset($response['actionCodeDescription']) && !empty($response['actionCodeDescription'])){
            $message = $response['actionCodeDescription'];
        }

        if (isset($response['orderNumber'])) {
            //$order_id = intval($response['orderNumber']);

            $payment = Payment::find()->where(['gatewayOrderId' => $gatewayOrderId/*, 'merchantOrderId' => $order_id*/])->one();
            $order_id = $payment->merchantOrderId;
            
            if ($payment === null) {
                return ['message' => 'Заказ не найден в системе магазина'];
            }

            $payment->paymentStatus = $response['orderStatus'];
            $payment->paymentErrorCode = $response['errorCode'];
            $payment->paymentErrorMessage = $message;

            $payment->save();

            //заказ успешно оплачен
            if (!empty($order_id) && $payment->paymentStatus == self::FULL_AUTHORIZATION_ORDER_AMOUNT) {
                $order = Shopcart::order($order_id);
                \app\models\shopcart\ShopcartPayment::sendReport($order->model);
                $message = '<b>Оплата проведена успешно!</b><br>Уведомление отправлено на указанный Вами e-mail.';
                if (!is_null($order)) {
                    $order->model->status = Order::STATUS_COMPLETED;
                    $order->model->save();
                }
            }
        }

        return ['message' => $message];
    }
    
    /**
     * Создание на тот же заказ магазина новой банковской операции
     * @param Payment $payment
     * @return Payment
     */
    public function oneMoreRegister($payment, $amount, $orderBundle, $returnUrl, $userId){
        $payment->attempt += 1;
        
        $response = $this->getContext()->registerOrder($payment->getFullMerchantOrderId(), $amount, $orderBundle, $returnUrl);
        
        $payment->userId = $userId;
        $payment->amount = $amount;
        $payment->orderBundle = json_encode($orderBundle);

        if (isset($response['orderId'])) $payment->gatewayOrderId = $response['orderId'];
        if (isset($response['formUrl'])) $payment->formUrl = $response['formUrl'];

        if (isset($response['errorCode'])) $payment->paymentErrorCode = intval($response['errorCode']);
        if (isset($response['errorMessage'])) $payment->paymentErrorMessage = $response['errorMessage'];

        return $payment;
    }
}