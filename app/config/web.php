<?php

$params = require(__DIR__ . '/params.php');

$basePath = dirname(__DIR__);
$webroot = dirname($basePath);

Yii::setAlias('@uploads', $webroot . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'uploads');
//Yii::setAlias('@uploads', 'uploads');

$config = [
    'id' => 'app',
    'basePath' => $basePath,
    'bootstrap' => ['log'],
    'name' => 'КОРДМАСТЕР',
    'language' => 'ru',
    'runtimePath' => $webroot . '/runtime',
    'vendorPath' => $webroot . '/vendor',
    'modules' => [
        'YandexMarketYml' => [
            'class' => 'corpsepk\yml\YandexMarketYml',
            'enableGzip' => true,
            'cacheExpire' => 1,
            'categoryModel' => 'app\models\catalog\Category',
            'shopOptions' => [
                'name' => 'Корд-мастер',
                'company' => 'ООО ТД Корд-мастер',
                'url' => 'https://kord-master.ru',
                'currencies' => [
                    [
                        'id' => 'RUR',
                        'rate' => 1
                    ]
                ],
            ],
            'offerModels' => [
                ['class' => 'app\models\catalog\Item'],
            ],
        ],
    ],
    'components' => [
        'html2pdf' => [
            'class' => 'yii2tech\html2pdf\Manager',
            'viewPath' => '@app/pdf',
            'converter' => [
                'class' => 'yii2tech\html2pdf\converters\Dompdf',
                'defaultOptions' => [
                    'pageSize' => 'A4',
                ],
            ],
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'salpfm3kfmk34mf3m4fklm',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'amocrm' => [
            'class' => 'yii\amocrm\Client',
            'subdomain' => 'kordmaster1',
            'login' => 'lapatin@kord-master.ru',
            'hash' => '792ec7ceb5ba1fa2a2f5291c76aeb36f',
            'fields' => [
                'status_id' => 10525225,
                'responsible_user_id_1' => 2012731,
                'responsible_user_id_2' => 2012731,
                'pipeline_id' => 1100938,
                'custom_fields' => [
                    'phone' => 291237,
                    'email' => 291239,
                    'address' => 291245,
                    'datetime' => 328977,
                    'inn' => 328979,
                    'company' => 328981,
                    'fulltitle' => 367511,
                ]
            ],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'htmlLayout' => false,
            'textLayout' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.yandex.ru',
                'port' => '465',
                'encryption' => 'ssl',
            ]
        ],
        
        'formatter' => [
            'dateFormat'     => 'php:d-m-Y',
            'datetimeFormat' => 'php:d-m-Y H:i',
            'timeFormat'     => 'php:H:i:s',
	],
        
        'urlManager' => require(__DIR__ . '/urlManager.php'),
        'view' => [
            'class' => '\rmrevin\yii\minify\View',
            'enableMinify' => !YII_DEBUG,
            'concatCss' => true, // concatenate css
            'minifyCss' => true, // minificate css
            'concatJs' => true, // concatenate js
            'minifyJs' => true, // minificate js
            'minifyOutput' => true, // minificate result html page
            'webPath' => '@web', // path alias to web base
            'basePath' => '@webroot', // path alias to web base
            'minifyPath' => '@webroot/minify', // path alias to save minify result
            'jsPosition' => [\yii\web\View::POS_END], // positions of js files to be minified
            'forceCharset' => 'UTF-8', // charset forcibly assign, otherwise will use all of the files found charset
            'expandImports' => true, // whether to change @import on content
            'compressOptions' => ['extra' => true], // options for compress
            /*'excludeFiles' => [
                'jquery.js', // exclude this file from minification
                'app-[^.].js', // you may use regexp
            ],*/
            'excludeBundles' => [
                //  \dev\helloworld\AssetBundle::class, // exclude this bundle from minification
            ],
        ],
        'assetManager' => [
            //'bundles' => require(__DIR__ . '/assets-local.php'),
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => '@app/media',
                    'js' => [
                        'js/jquery-1.min.js',
                    ]
                ],
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'sberbankPaymentGate' => require(__DIR__ . '/sb.php'),
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';
    $config['components']['assetManager']['forceCopy'] = true;

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';

    $config['components']['db']['enableSchemaCache'] = false;
}

return array_merge_recursive($config, require(dirname(__FILE__) . '/../../vendor/iddqd-roman/cms/config/cms.php'));
