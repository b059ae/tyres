<?php
return [
    'rules' => [
        ['pattern' => 'yandex-market', 'route' => 'YandexMarketYml/default/index', 'suffix' => '.yml'],
        'callback' => 'site/callback',
        
        'services/<slug:[\w-]+>' => 'services/view',
        'services/<slug:[\w-]+>/price' => 'services/download-price',
        
        'catalog/all-tyres' => 'catalog/all-tyres',
        'catalog/<category:[\w-]+>/<slug:[\w-]+>' => 'catalog/view',
        'catalog/<slug:[\w-]+>/page/<page:\d+>' => 'catalog/cat',
        'catalog/<slug:[\w-]+>' => 'catalog/cat',
        'catalog' => 'catalog/index',

        'shopcart/add' => 'shopcart/add',
        'shopcart/remove' => 'shopcart/remove',
        'shopcart' => 'shopcart/index',
        'shopcart/update' => 'shopcart/index',
        'shopcart/payment' => 'shopcart/index',
        'shopcart/invoice' => 'shopcart/index',
        'shopcart/reserve' => 'shopcart/index',
        
        'vacancies' => 'vacancies/index',
        'vacancies/<slug:[\w-]+>' => 'vacancies/view',

        'success' => 'site/message',
        'fail' => 'site/message',
        'download' => 'site/download',
        
        'oplata' => 'site/payment',
        'kontakty' => 'site/contact',
        'uslugi-gruzovogo-shinomontazha' => 'site/price',
        'polzovatelskoe-soglashenie' => 'site/conditions',
        'politika-konfidencialnosti' => 'site/privacy',
        'obmen-i-vozvrat' => 'site/return',

        '<controller:\w+>/view/<slug:[\w-]+>' => '<controller>/view',
        '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',

        // Оплата через платежный шлюз Сбербанка
        'sberbank-payment' => 'site/sberbank-payment',
        'sberbank-payment-done' => 'site/sberbank-payment-done',
    ],
];
