<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\cms\modules\article\api\Article;


class ServicesController extends Controller{
    
    public function actions(){
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionView($slug){
        $cat = Article::cat('services');
        if(!$cat){
            throw new NotFoundHttpException('Категория не найдена.');
        }
        $article = Article::get($slug);
        if(!$article){
            throw new NotFoundHttpException('Статья не найдена.');
        }
        $model = new \app\models\ServicesForm();
        $model->service = $article->title;
        if($model->load(Yii::$app->request->post()) && $model->action()){
            return $this->redirect(['/success']);
        }
        $price_model = new \app\models\ServicesPriceForm();
        $price_model->service = $article->title;
        if($price_model->load(Yii::$app->request->post()) && $price_model->action($article)){
            return $this->redirect(['/success']);
        }
        return $this->render('view', [
            'article' => $article,
            'cat' => $cat,
            'model' => $model,
            'price_model' => $price_model,
        ]);
    }
    
    public function actionIndex(){
        $cat = Article::cat('services');
        if(!$cat){
            throw new NotFoundHttpException('Категория не найдена.');
        }
        return $this->render('index', [
            'cat' => $cat,
        ]);
    }
    
    public function actionDownloadPrice($slug){
        $article = Article::get($slug);
        if(!$article){
            throw new NotFoundHttpException('Статья не найдена.');
        }
        $file = Yii::getAlias('@webroot') . '/uploads/' . $article->model->file;
        if(!file_exists($file) || !is_file($file)){
            throw new NotFoundHttpException('Файл не найден.');
        }
        return Yii::$app->response->sendFile($file);
    }
}