<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\cms\modules\catalog\api\Catalog;
use yii\cms\modules\shopcart\api\Shopcart;
use yii\web\NotFoundHttpException;


class ShopcartController extends Controller{
    
    
    public function actionIndex($action = null){
        $model_update = new \app\models\shopcart\ShopcartUpdate();
        $model_payment = new \app\models\shopcart\ShopcartPayment();
        $model_invoice = new \app\models\shopcart\ShopcartInvoice();
        $model_reserve = new \app\models\shopcart\ShopcartReserve();
        
        switch($action){
            case 'update' : $active_model = $model_update;
                break;
            case 'payment' : $active_model = $model_payment;
                break;
            case 'invoice' : $active_model = $model_invoice;
                break;
            case 'reserve' : $active_model = $model_reserve;
                break;
            default : $active_model = null;
                break;
        }
        
        if($active_model){
            if($active_model->load(Yii::$app->request->post())){
                $redirect = $active_model->action();
                if($redirect){
                    return $redirect;
                }
            }
        }
        
        return $this->render('index', [
            'goods' => Shopcart::goods(),
            'model_update' => $model_update,
            'model_payment' => $model_payment,
            'model_invoice' => $model_invoice,
            'model_reserve' => $model_reserve,
        ]);
    }
    
    
    public function actionAdd(){
        $post = Yii::$app->request->post();
        
        $item = Catalog::get($post['id']);
        if(!$item){
            throw new NotFoundHttpException('Item not found');
        }
        
        Shopcart::add($item->id, $post['count']);
        $add_message = 'Товар добавлен в корзину';
        
        if(Yii::$app->request->isAjax){
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'add_message' => $add_message,
                'goods_count' => count(Shopcart::goods()),
            ];
        }
        
        Yii::$app->getSession()->setFlash('add_message', $add_message);
        return $this->redirect(['/shopcart']);
    }
    
    
    public function actionRemove($id){
        Shopcart::remove($id);
        return $this->redirect(Yii::$app->request->referrer);
    }
    
    
    public function actionOrder($id, $token){
        $order = Shopcart::order($id);
        if(!$order || $order->access_token != $token){
            throw new NotFoundHttpException('Order not found');
        }
        return $this->render('order', ['order' => $order]);
    }
}
