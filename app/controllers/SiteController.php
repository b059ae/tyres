<?php

namespace app\controllers;

use Yii;
use app\helpers\CatalogHelper;
use app\models\CatalogFilterForm;
use yii\cms\models\Setting;
use yii\cms\modules\catalog\api\Catalog;
use yii\cms\modules\page\api\Page;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\cms\modules\shopcart\api\Shopcart;
use yii\helpers\Url;


class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Главная страница
     *
     * @return string
     */
    public function actionIndex(){
        $page = Page::get('index');
        $meeting_model = new \app\models\MeetingForm();
        if($meeting_model->load(Yii::$app->request->post()) && $meeting_model->action()){
            return $this->redirect(['site/message']);
        }
        $feedback_model = new \app\models\FeedbackForm();
        if($feedback_model->load(Yii::$app->request->post()) && $feedback_model->action()){
            return $this->redirect(['site/message']);
        }

        return $this->render('index', [
            'meeting_model' => $meeting_model,
            'feedback_model' => $feedback_model,
            'page' => $page,
            'catalogs' => CatalogHelper::cats(),
            'popular' => Catalog::items([
                'pagination' => ['pageSize' => 12],
                'filters' => ['popular' => 'Да']
            ]),
            'filterForm' => new CatalogFilterForm(),
            'address' => Setting::get('address'),
            'workingHours' => Setting::get('workingHours'),
            'services_cat' => \yii\cms\modules\article\api\Article::cat('services'),
        ]);
    }

    /**
     * Оформление заказа и доставка
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionPayment()
    {
        $page = Page::get('payment');
        if (!$page) {
            throw new NotFoundHttpException('Item not found.');
        }
        return $this->render('payment', [
            'page' => $page,
        ]);
    }

    /**
     * Контактная информация
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionContact()
    {
        $page = Page::get('contact');
        if (!$page) {
            throw new NotFoundHttpException('Item not found.');
        }
        return $this->render('contact', [
            'page' => $page,
        ]);
    }

    /**
     * Цены на шиномонтаж
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionPrice()
    {
        $page = Page::get('price');
        if (!$page) {
            throw new NotFoundHttpException('Item not found.');
        }
        return $this->render('price', [
            'page' => $page,
            'map_url_1' => Setting::get('map_url_1'),
            'map_url_2' => Setting::get('map_url_2'),
            'email' => Setting::get('email'),
            'phone' => Setting::get('phone'),
            'address' => Setting::get('address'),
            'workingHours' => Setting::get('workingHours'),
        ]);
    }

    /**
     * Обмен и возврат
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionReturn()
    {
        $page = Page::get('return');
        if (!$page) {
            throw new NotFoundHttpException('Item not found.');
        }
        return $this->render('return', [
            'page' => $page,
        ]);
    }

    /**
     * Пользовательское соглашение
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionConditions()
    {
        $page = Page::get('conditions');
        if (!$page) {
            throw new NotFoundHttpException('Item not found.');
        }
        return $this->render('conditions', [
            'page' => $page,
        ]);
    }

    /**
     * Политика конфиденциальности
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionPrivacy()
    {
        $page = Page::get('privacy');
        if (!$page) {
            throw new NotFoundHttpException('Item not found.');
        }
        return $this->render('privacy', [
            'page' => $page,
        ]);
    }

    /**
     * Оплата заказа через эквайринг сбербанка
     *
     */
    public function actionSberbankPayment()
    {
        $order = (new Shopcart())->order;
        
        if(empty($order->id)){
            Yii::$app->session->setFlash('title', 'Ошибка: заказ не оформлен.');
            throw new NotFoundHttpException('Ваша корзина пуста. Выберите товары и оформите заказ в корзине.');
        }
        
        $cost = $order->cost;
        $returnUrl = Url::toRoute(['site/sberbank-payment-done'], 'https');
        $bundle = $this->bundleToFormat($order);
        $dataView = Yii::$app->sberbankPaymentGate->registerOrder($order->id, $cost, $bundle, $returnUrl);
        $dataView['order_id'] = $order->id;

        return $this->render('sberbank-payment', $dataView);
    }
    
    private function bundleToFormat($order){
        $result = [
            'cartItems' => ['items' => []]
        ];
        if(!empty($order->email)){
            $result['customerDetails'] = ['email' => $order->email];
        }
        $i = 0;
        foreach($order->goods as $good){
            $i++;
            $result['cartItems']['items'][] = [
                'positionId' => $i,
                'name' => CatalogHelper::getItemTitle($good->item),
                'quantity' => [
                    'value' => $good->count,
                    'measure' => 'шт',
                ],
                'itemAmount' => $good->item->price * $good->count * 100,
                'itemCode' => $good->item->id,
                /*'tax' => [
                    'taxType' => '0',
                ],*/
                'itemPrice' => $good->item->price * 100,
            ];
        }
        return $result;
    }
    
    

    /**
     * Страница, на которую редиректит эквайринг сбербанка после оплаты
     *
     * @param string $orderId ID заказа в системе платежного шлюза
     * @return mixed
     */
    public function actionSberbankPaymentDone($orderId)
    {
        $dataView = ['message' => 'Не передан ID заказа'];
        if (!empty($orderId)) {
            $dataView = Yii::$app->sberbankPaymentGate->doOrderStatusExtended($orderId);
        }

        return $this->renderPartial('_sberbank-payment-done', $dataView);
    }
    
    /**
     * Страница сообщения об удаче или ошибке
     * @return string|yii\base\Response
     */
    public function actionMessage(){
        $title = Yii::$app->session->getFlash('title');
        $message = Yii::$app->session->getFlash('message');
        $download = Yii::$app->session->getFlash('download');
        
        if(empty($message)){
            return $this->redirect(['/']);
        }
        
        return $this->render('message', [
            'title' => $title,
            'message' => $message,
            'download' => $download,
        ]);
    }
    
    /**
     * Страница скачивания файла
     * @return yii\base\Response
     * @throws NotFoundHttpException
     */
    public function actionDownload(){
        $file_to_download = Yii::$app->session->getFlash('download');
        if(!$file_to_download){
            throw new NotFoundHttpException('Страница не найдена.');
        }
        return Yii::$app->response->sendFile($file_to_download);
    }
    
    public function actionCallback(){
        $model = new \app\models\CallbackForm();
        if($model->load(Yii::$app->request->post()) && $model->action()){
            return $this->redirect(['/success']);
        }
        return $this->goBack();
    }
}