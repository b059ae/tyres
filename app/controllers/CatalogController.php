<?php

namespace app\controllers;

use app\models\CatalogFilterForm;
use Yii;
use yii\cms\modules\catalog\api\Catalog;
use yii\cms\modules\page\api\Page;
use yii\web\NotFoundHttpException;
use app\helpers\CatalogHelper;

class CatalogController extends \yii\web\Controller
{
    /**
     * Страница Каталог
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        /*$page = Page::get('catalog');
        if (!$page) {
            throw new NotFoundHttpException('Item not found.');
        }*/

        // Список категорий
        $cats = Catalog::cats();
        return $this->render('index', [
            //'page' => $page,
            'cats' => $cats,
        ]);
    }

    /**
     * Просмотр списка товаров в категории
     * @param string $slug Название категории
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionCat($slug){
        $model = new CatalogFilterForm();
        $cat = Catalog::cat($slug);

        if(!$cat) {
            throw new NotFoundHttpException('Категория не найдена.');
        }
        $ids = null;
        if ($model->load(Yii::$app->request->get()) && $model->validate()) {
            $ids = $model->getItemIds($cat->id);
        }
        
        return $this->render('cat', [
            'model' => $model,
            'cat' => $cat,
            'items' => $cat->getItems([
                'where' => ['id' => $ids],
                'pagination' => ['pageSize'=>16, 'pageSizeParam' => false, 'forcePageParam' => false],
                'orderBy' => 'available DESC',
            ]),
            'filter_params' => \yii\helpers\BaseJson::encode(array_filter($model->toArray())),
        ]);
    }
    
    
    public function actionAllTyres(){
        $model = new CatalogFilterForm();
        $cat_to_exclude = Catalog::cat('wheels');
        $ids = null;
        if ($model->load(Yii::$app->request->get()) && $model->validate()) {
            $ids = $model->getItemIds(null, $cat_to_exclude->id);
        }
        return $this->render('all-tyres', [
            'model' => $model,
            'items' => Catalog::items([
                'where' => ['id' => $ids],
                'pagination' => ['pageSize'=>16, 'pageSizeParam' => false, 'forcePageParam' => false],
                'orderBy' => 'available DESC',
            ]),
            'filter_params' => \yii\helpers\BaseJson::encode(array_filter($model->toArray())),
            'pagination' => Catalog::pagination(),
            'cat_to_exclude_id' => $cat_to_exclude->id,
        ]);
    }
    
    

    public function actionSearch($q){
        $page = Page::get('search');
        $q = filter_var($q, FILTER_SANITIZE_STRING);
        
        $ids = CatalogHelper::searchItemIDs($q);
        if (count($ids) == 0) {
            $where = ['id' => '0'];
        }
        else{
            $where = ['in', 'id', $ids];
        }
        
        return $this->render('search', [
            'text' => $q,
            'items' => Catalog::items([
                'pagination' => ['pageSize'=>16, 'pageSizeParam' => false, 'forcePageParam' => false],
                'where' => $where,
            ]),
            'page' => $page,
        ]);
    }

    
    public function actionView($slug){
        $item = Catalog::get($slug);
        if (!$item) {
            throw new NotFoundHttpException('Товар не найден.');
        }
        $popular = CatalogHelper::getRecommended($item);
        if(!count($popular)){
            $popular = Catalog::items([
                'pagination' => ['pageSize' => 12],
                'filters' => ['popular' => 'Да']
            ]);
        }
        return $this->render('view', [
            'item' => $item,
            'popular' => $popular,
        ]);
    }
}
