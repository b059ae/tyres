<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\cms\modules\article\api\Article;


class VacanciesController extends Controller{
    
    public function actions(){
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionView($slug){
        $cat = Article::cat('vacancies');
        if(!$cat){
            throw new NotFoundHttpException('Категория не найдена.');
        }
        $article = Article::get($slug);
        if(!$article){
            throw new NotFoundHttpException('Вакансия не найдена.');
        }
        return $this->render('view', [
            'article' => $article,
            'cat' => $cat,
        ]);
    }
    
    public function actionIndex(){
        $cat = Article::cat('vacancies');
        if(!$cat){
            throw new NotFoundHttpException('Категория не найдена.');
        }
        return $this->render('index', [
            'cat' => $cat,
        ]);
    }
}